﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceTestLibrary
{
    public class MemoryKiller
    {
        private byte[] _bytes;
        public MemoryKiller()
        {
            //_bytes = new List<object>();
        }

        public void EatOneMBMemory(int amountOfMB)
        {
            //eat one MB
            var amountOfBytes = amountOfMB*1024*1024;
            _bytes = new byte[amountOfBytes];
            for (var i = 0; i < amountOfBytes; i++)
            {
                _bytes[i] = 255;
            }

        }

        public void FreeMemory()
        {
        }
    }
}
