﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PerformanceTestLibrary
{
    public class CPUKiller
    {
        public static void SpawnThreads(int amount)
        {
            var threads = new List<Thread>();
            for (var i = 0; i < amount; i++)
            {
                var t = new Thread(InfiniteLoop);
                t.Start();
                threads.Add(t);
            }
            Thread.Sleep(10000);
            foreach (var t in threads)
            {
                t.Abort();
            }
        }

        public static void InfiniteLoop()
        {
            while (true)
            {
                
            }
        }

        public static void ParallelCPUKill(object cpuUsage)
        {
            Parallel.For(0, 1, i =>
            {
                var watch = new Stopwatch();
                watch.Start();
                while (true)
                {
                    if (watch.ElapsedMilliseconds > (int) cpuUsage)
                    {
                        Thread.Sleep(100 - (int) cpuUsage);
                        watch.Reset();
                        watch.Start();
                    }
                }
            });
        }

        public static void StartParallelCPUKill(int cpuPercent, int millisecondsToRun)
        {
            var cpuUsage = cpuPercent;
            var time = millisecondsToRun;
            var threads = new List<Thread>();
            for (var i = 0; i < Environment.ProcessorCount; i++)
            {
                var t = new Thread(ParallelCPUKill);
                t.Start(cpuUsage);
                threads.Add(t);
            }
            Thread.Sleep(time);
            foreach (var t in threads)
            {
                t.Abort();
            }
        }


    }
}
