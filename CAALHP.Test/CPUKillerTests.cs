﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using PerformanceTestLibrary;
using Xbehave;

namespace CAALHP.Test
{
    public class CPUKillerTests
    {
        [Scenario]
        public void TryKillingCPU(int cpuUsage, int milliseconds)
        {

            "Given cpuUsage is 90%"
                .Given(() => cpuUsage = 90);

            "Given milliseconds is 10000"
                .Given(() => milliseconds = 10000);

            //"When creating a new instance of InstallDriverPluginInstrategy"
            //    .When(() => strategy = new InstallDriverPluginStrategy(host));

            "Then cpukiller should consume 90% cpu time for 10 seconds"
                .Then(() => CPUKiller.StartParallelCPUKill(cpuUsage, milliseconds));
        }

        [Scenario]
        public void SpawningThreadsShouldKillCPU(int amountOfThreads)
        {

            "Given amountOfThreads is 10"
                .Given(() => amountOfThreads = 10);

            //"When creating a new instance of InstallDriverPluginInstrategy"
            //    .When(() => strategy = new InstallDriverPluginStrategy(host));

            "Then cpukiller should consume lots of cpu"
                .Then(() => CPUKiller.SpawnThreads(amountOfThreads));
        }
    }
}
