﻿using System;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using FluentAssertions;
using Xbehave;

namespace CAALHP.Test
{
    public class EventHelperTests
    {
        [Scenario]
        public void TestGetTypeFromFullyQualifiedNameSpace(Type t, string key)
        {
            "Given key is json:CAALHP.Events.SimpleMeasurementEvent"
                .Given(() => key = "json:CAALHP.Events.SimpleMeasurementEvent");
            "When calling GetTypeFromFullyQualifiedNameSpace"
                .When(() => t = EventHelper.GetTypeFromFullyQualifiedNameSpace(key));
                //.When(() => t = Type.GetType("CAALHP.Events.SimpleMeasurementEvent, CAALHP.Events"));
            /*"And waiting for a long time"
                .And(() => Thread.Sleep(60000));*/
            "Then t should be a SimpleMeasurementEvent"
                .Then(() => t.Should().Be(typeof(SimpleMeasurementEvent)));
        } 
    }
}
