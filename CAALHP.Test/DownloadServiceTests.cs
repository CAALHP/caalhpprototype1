﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using CAALHP.Contracts;
using FluentAssertions;
using Plugins.DownloadService;
using Xbehave;

namespace CAALHP.Test
{
    public class DownloadServiceTests
    {
        [Scenario]
        public void CallingGetFileShouldDownloadFile(IDownloadManager downloadManager, IHostCAALHPContract host, string appName, string downloadDir, IAsyncResult res)
        {
            "Given host"
                .Given(() => host = new Moq.Mock<IHostCAALHPContract>().Object);
            
            "And downloadManager"
                .Given(() => downloadManager = new DownloadManager());

            "And appName is plugins.healthdataviewer.zip"
                .Given(() => appName = "plugins.healthdataviewer.zip");
            "And downloadDir is setup"
                .Given(()=>downloadDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings.Get("downloadDirectory")));

            //"And downloadManager is monitored for events"
            //    .Given(() => downloadManager.MonitorEvents());

            "When downloading app async"
                .When(() =>
                {
                    var x = downloadManager.DownloadAppAsync(appName, CancellationToken.None);
                    while (!x.IsCompleted) { Thread.Sleep(50);}
                });

            /*"While downloading..."
                .When(() =>
                {
                    while (!res.IsCompleted)
                    {
                    }
                });*/

            "Then download should be completed"
                .Then(() => new DirectoryInfo(downloadDir).GetFiles().Count().Should().BeGreaterThan(0));
        }
    }
}
