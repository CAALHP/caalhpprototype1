﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using PerformanceTestLibrary;
using Xbehave;
using Xunit;

namespace CAALHP.Test
{
    public class MemoryKillerTests
    {
        [Scenario]
        public void SpawningThreadsShouldKillCPU(int amountOfMB, MemoryKiller memKiller)
        {

            "Given amountOfThreads is 10"
                .Given(() => amountOfMB = 1000);
            "and a memKiller"
                .And(() => memKiller = new MemoryKiller());

            "When eating some memory 1000 times"
                .When(() =>memKiller.EatOneMBMemory(amountOfMB));

            "Then memkiller should consume lots of memory"
                .Then(() => memKiller.Should().NotBeNull());
        }
    }
}
