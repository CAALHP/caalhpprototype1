﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CAALHP.Test
{
    /// <summary>
    /// REMEMBER TO MAKE CLASS PUBLIC
    /// otherwise it won't be visible to the testrunner
    /// </summary>
    public class MyTests
    {
        [Fact]
        public void FactMethodName()
        {
            Assert.Equal(4, 2+2);
        }
    }
}
