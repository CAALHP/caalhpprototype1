﻿using System.Collections.Generic;
using System.ServiceModel;
using CareStoreServiceContracts;
using FluentAssertions;
using Plugins.DownloadService;
using Xbehave;

namespace CAALHP.Test
{
    public class MarketplaceTests
    {
        [Scenario]
        public void CallingGetAppsShouldReturnSomeNames(ICareStoreMarketplaceContract market, IList<string> appNames)
        {
            //binding
            var binding = new BasicHttpsBinding();
            //endpoint
            var endpoint = new EndpointAddress("https://carestoremarketplace.cloudapp.net/CareStoreMarketplaceService.svc");
            //channelfactory
            var channelFactory = new ChannelFactory<ICareStoreMarketplaceContract>(binding, endpoint);

            System.Net.ServicePointManager.ServerCertificateValidationCallback +=
                (sender, certificate, chain, sslPolicyErrors) => true;
            var testmarket = channelFactory.CreateChannel();
            var list = testmarket.GetAppLinks();
            //IList<Uri> uris;
            "Given market is an instance of ICareStoreMarketplaceContract"
                .Given(() => market = channelFactory.CreateChannel());
            
            "When getting applinks from market"
                .When(() => appNames = market.GetAppLinks());

            "Then appnames should contain more than 0 objects"
                .Then(() => appNames.Count.Should().BeGreaterThan(0));
        }

        [Scenario]
        public void CallingGetFileNamesShouldReturnSomeNames(string host, int port, string username, string password, string remoteDirectory, IList<string> resultList)
        {
            "Given host is mumeVM2012.cloudapp.net"
                .Given(() => host = "mumeVM2012.cloudapp.net");
            "Given port is 22000"
                .Given(() => port = 22000);
            "And username is caalhp"
                .Given(() => username = "caalhp");
            "And password is CareStore2013"
                .Given(() => password = "CareStore2013");
            "And remotedirectory = ./apps/"
                .Given(() => remoteDirectory = "./apps/");

            "When getting filenames from SFTPHelper"
                .When(() => resultList = SFTPHelper.GetFileNames(host,port,username,password,remoteDirectory));

            "Then memkiller should consume lots of memory"
                .Then(() => resultList.Count.Should().BeGreaterThan(0));
        }

        
    }
}
