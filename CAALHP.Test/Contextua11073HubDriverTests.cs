﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CAALHP.Contracts;
using FluentAssertions;
using Moq;
using Plugins.Contextua11073HubDriver;
using Xbehave;

namespace CAALHP.Test
{
    public class Contextua11073HubDriverTests
    {
        [Scenario]
        public void CreateDriver(IDeviceDriverCAALHPContract deviceDriver, IDeviceDriverHostCAALHPContract host)
        {
            "Given driver is contextua hub"
                .Given(() => deviceDriver = new Contextua11073HubDeviceDriverImplementation());
            "And host is mocked"
                .And(() => host = new Mock<IDeviceDriverHostCAALHPContract>().Object);
            "When initializing driver"
                .When(() => deviceDriver.Initialize(host, 0));
            /*"And waiting for a long time"
                .And(() => Thread.Sleep(60000));*/
            "Then GetMeasurement should be greater than 0"
                .Then(() => deviceDriver.GetName().Should().NotBeNullOrEmpty());
        }
        
    }
}
