﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CAALHP.Contracts;
using CAALHP.Test.BedWeightMonitorServiceReference;
using FluentAssertions;
using Moq;
using Plugins.Contextua11073HubDriver;
using Xbehave;

namespace CAALHP.Test
{
    public class CareBedTests
    {
        [Scenario]
        public void GetWeight(IBedWeightSensor client, double weight)
        {
            "Given client is BedWeightSensorClient"
                .Given(() => client = new BedWeightSensorClient());
            "When calling client.weight"
                .When(() => weight = client.Weight());
            /*"And waiting for a long time"
                .And(() => Thread.Sleep(60000));*/
            "Then GetMeasurement should be greater than 0"
                .Then(() => weight.Should().BeGreaterOrEqualTo(0));
        } 
    }
}
