﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CareStoreServiceContracts;

namespace CareStoreMarketplaceServiceWebRole
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class CareStoreMarketplaceContractService : ICareStoreMarketplaceContract
    {
        /// <summary>
        /// Looks up link to driver file for profile
        /// </summary>
        /// <param name="profile">the name of the profile</param>
        /// <returns>link to driver</returns>
        /// <exception cref="ArgumentException">thrown if an unknown profile is provided</exception>
        public Uri GetDriverLink(string profile)
        {
            var link = new UriBuilder {Host = "carestoremarketplace.cloudapp.net"};
            switch (profile.ToLower())
            {
                case "pi":
                    link.Path = "/Files/Drivers/PiDriver.zip";
                    break;
                case "random":
                    link.Path = "/Files/Drivers/RandomDriver.zip";
                    break;
                default:
                    return null;
            }
            return link.Uri;
        }

        public IList<string> GetAppLinks()
        {
            var host = ConfigurationManager.AppSettings.Get("host");
            var port = int.Parse(ConfigurationManager.AppSettings.Get("port"));
            var username = ConfigurationManager.AppSettings.Get("username");
            var password = ConfigurationManager.AppSettings.Get("password");
            var remoteDirectory = ConfigurationManager.AppSettings.Get("remoteDirectory") + "/apps/";

            return SFTPFileLister.GetFileNames(host, port, username, password, remoteDirectory);
        }

    }
}
