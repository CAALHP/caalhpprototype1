﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using N4CLibrary;
using Net4Care.Common.Standard;
using Net4Care.Observation;

namespace N4CConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            //we need 2 arguments
            if (args.Length != 2) return;
            var serverAddress = args[0];
            var cpr = args[1];
            var glucoseHelper = new N4CHelper(serverAddress, typeof(GlucoseObservation));
            var saturationHelper = new N4CHelper(serverAddress, typeof(SaturationObservation));
            var bloodHelper = new N4CHelper(serverAddress, typeof(HomeBloodPressureObservation));

            //how about context?
            //var context = new ContextCode("ND",,"NoiseDetected");
            
            var bloodpressure = new HomeBloodPressureObservation(120, 80, 60, true, 900);
            
            Console.WriteLine("Defined new home blood pressure:");
            Console.WriteLine(bloodpressure);
            Console.WriteLine("Uploading Bloodpressure measurement...");
            
            bloodHelper.UploadBloodPressureObservation(cpr, bloodpressure);
            
            var saturation = new SaturationObservation(98);

            Console.WriteLine("Defined new saturation:");
            Console.WriteLine(saturation.ObservationAsHumanReadableText);
            Console.WriteLine("Uploading saturation measurement...");
            
            saturationHelper.UploadSaturationObservation(cpr, saturation);
            
            Console.WriteLine("Uploading done.");
            Console.WriteLine();
            Console.WriteLine("Fetching Bloodpressure measurements from the last 10 minutes");
            
            var homebps = bloodHelper.GetFilteredObservations<HomeBloodPressureObservation>(cpr, 10);
            foreach (var hbp in homebps)
            {
                Console.WriteLine(hbp.ObservationSpecifics.ObservationAsHumanReadableText);
                Console.WriteLine();
            }
            Console.WriteLine("Fetching measurements from the last 10080 minutes");
            var stos = saturationHelper.GetFilteredObservations<SaturationObservation>(cpr, 10080);
            foreach (var sto in stos)
            {
                Console.WriteLine(sto.ObservationSpecifics.ObservationAsHumanReadableText);
                Console.WriteLine();
            }
            Console.WriteLine("Press <ENTER> to exit");
            Console.ReadLine();
            //helper.UploadBloodPressureObservation();
        }
    }
}
