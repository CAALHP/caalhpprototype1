﻿using CAALHP.Library.Hosts;
using Ice;

namespace CAALHP.SOA.ICE.HostAdapters
{
    public interface IDeviceDriverHostIce : IDeviceDriverHost
    {
        void RegisterDeviceDriver(string name, string category, int processId, Current current__);
    }
}