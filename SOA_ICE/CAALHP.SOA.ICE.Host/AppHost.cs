﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Threading.Tasks;
using CAALHP.Library.Brokers;
using CAALHP.Library.Config;
using CAALHP.Library.Hosts;
using CAALHP.Library.Managers;
using CAALHP.Library.Observer;
using CAALHP.Library.Visitor;
using CAALHP.SOA.ICE.Host.LifeCycleManagers;
using CAALHP.SOAICE.Contracts;
using CAALHP.SOA.ICE.HostAdapters;
using Ice;
using Exception = System.Exception;

namespace CAALHP.SOA.ICE.Host
{
    public class AppHost : Host, IAppHostIce
    {
        private readonly IAppHostContractDisp_ _iceAdapter;
        private readonly ICAALHPBroker _broker;
        private string _appRoot;
        private Ice.Communicator _ic;
        private ObjectAdapter _adapter;
        public AppLifeCycleManager LifeCycleManager { get; private set; }
        public AppHost(EventManager eventManager, ICAALHPBroker broker)
            : base(eventManager)
        {
            _broker = broker;
            _appRoot = Path.Combine(IceRoot, "Apps");
            if (!Directory.Exists(_appRoot))
            {
                Directory.CreateDirectory(_appRoot);
            }
            //Create lifecyclemanager
            LifeCycleManager = new AppLifeCycleManager(_appRoot);
            _iceAdapter = new AppHostAdapter(this);

            StartIce();
        }

        private void StartIce()
        {
            //_status = 0;
            _ic = null;
            try
            {
                _ic = Ice.Util.initialize();
                _adapter = _ic.createObjectAdapterWithEndpoints("AppHostAdapter", "default -p 10003");
                var obj = new AppHostAdapter(this);
                _adapter.add(obj, Ice.Util.stringToIdentity("AppHost"));
                _adapter.activate();
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e);
                //_status = 1;
            }

        }

        public override IList<PluginConfig> GetListOfInstalledApps()
        {
            return _broker.GetListOfInstalledApps();
        }

        public override void ShowApp(string appName)
        {
            _broker.ShowApp(appName);
        }

        /*private bool IsAppInstalled(string name)
        {
            return GetListOfInstalledApps().Any(x => x.Config.PluginName.Equals(name));
        }

        private int GetIndexOfInstalledApp(string name)
        {
            var installedApps = GetListOfInstalledApps();
            //var index = 0;
            for (var i = 0; i < installedApps.Count; i++)
            {
                if (installedApps[i].Name == name)
                {
                    return i;
                }
            }
            return -1;
        }

        private int GetIndexOfRunningApp(string name)
        {
            foreach (var appProcess in LifeCycleManager.AppDictionary.Where(appProcess => appProcess.Value.App.GetName().Equals(name)))
            {
                return appProcess.Key;
            }
            return -1;
        }*/

        public override void CloseApp(string appName)
        {
            var list = GetListOfRunningApps();
            if (list.All(x => !x.Directory.EndsWith(appName))) return;
            var app = list.First(x => x.Directory.EndsWith(appName));
            if (app == null) return;
            var index = list.IndexOf(app);
            CloseApp(index);
        }

        public override IList<PluginConfig> GetListOfInstalledDeviceDrivers()
        {
            return _broker.GetListOfInstalledDeviceDrivers();
        }

        public override void UnSubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            if (!LifeCycleManager.AppDictionary.ContainsKey(processId)) return;
            var plugin = LifeCycleManager.AppDictionary[processId];
            EventManager.EventSubject.Detach(plugin, fullyQualifiedNameSpace);
        }

        public override void SubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            if (!LifeCycleManager.AppDictionary.ContainsKey(processId)) return;
            var plugin = LifeCycleManager.AppDictionary[processId];
            EventManager.EventSubject.Attach(plugin, fullyQualifiedNameSpace);
        }

        public override void ShutDown()
        {
            LifeCycleManager.StopPlugins();
        }

        public override void Accept(IHostVisitor visitor)
        {
            visitor.Visit(this);
        }

        public Dictionary<int, ProcessInfo> GetRunningAppsProcessInfo()
        {
            throw new NotImplementedException();
        }

        public IList<PluginConfig> GetListOfRunningApps()
        {
            return _broker.GetListOfRunningApps();
        }

        public IList<PluginConfig> GetLocalRunningApps()
        {
            var apps = new List<PluginConfig>();
            foreach (var appProcess in LifeCycleManager.AppDictionary)
            {
                if (appProcess.Value == null) continue;
                if (appProcess.Value.App == null) continue;
                apps.Add(appProcess.Value.Config);// (new PluginInfo(){LocationDir = appProcess.Value.Config.Directory, Name = appProcess.Value.App.GetName()});
            }
            return apps;
        }

        public void ShowLocalApp(string appName)
        {
            var apps = GetLocalInstalledApps();
            foreach (var app in apps.Where(app => app.Config.PluginName.Equals(appName)))
            {
                LifeCycleManager.ActivatePlugin(app);
            }
        }

        public void StartApp(int app)
        {
            throw new NotImplementedException();
        }

        public void StartApp(int appIndex, PermissionSet permissions)
        {
            throw new NotImplementedException();
        }

        public void CloseApp(int app)
        {
            LifeCycleManager.AppDictionary[app].App.begin_ShutDown();
            LifeCycleManager.AppDictionary.Remove(app);
        }

        public void SwitchToApp(int app)
        {
            LifeCycleManager.AppDictionary[app].App.Show();
        }

        public IList<PluginConfig> GetLocalInstalledApps()
        {
            return LifeCycleManager.GetInstalledPlugins();
        }

        public void RegisterApp(string name, string category, int processId, Current current__)
        {
            lock (this)
            {
                //System.Console.Out.WriteLine("adding client `" + _communicator.identityToString(ident) + "'");
                var task = new Task(() =>
                {

                    try
                    {
                        Ice.ObjectPrx @base = current__.con.createProxy(new Identity(name, category));
                        var client = IAppContractPrxHelper.uncheckedCast(@base);
                        if (LifeCycleManager.AppDictionary.ContainsKey(processId))
                            LifeCycleManager.AppDictionary[processId].App = client;
                        Console.WriteLine("added a new app: " + client.GetName());
                        //Console.WriteLine("GetName() = " + client.GetName());
                        //client.Initialize(null, processId);
                        //client.Notify("Test", "Fedt");
                        //client.Stop();
                        //LifeCycleManager.ServiceDictionary[processId].Service.Initialize(HostView, processId);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error during registration of CareStoreApp callback " + e);
                    }
                });
                task.Start();
            }
        }

        public void ActivateDrivers()
        {
            _broker.ActivateDeviceDrivers();
        }

    }
}
