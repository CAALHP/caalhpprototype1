﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CAALHP.SOA.ICE.ClientAdapters;

namespace IceServiceConsoleClient
{
    class Program
    {
        /// <summary>
        /// Test service program
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //Set the test app name
            //var testServiceName = "TestService" + DateTime.Now.ToShortTimeString();
            var endpoint = "localhost";

            //For each adapter - attach an observer facade for handling the callback 
            //var observer = new CareStoreSoaIceApdater.CareStoreSOAEventObserverFacade(); //TODO: consider refactoring this. Maybe it can be merged into the adpater instead?

            //The CareStoreSoaIceAdpater handles all communication 
            var adapter = new ServiceAdapter(endpoint,);

            //Task taska = new Task(() => adapter.Connect("localhost", "TestApp", adapter));
            //taska.Start();
            adapter.Connect(endpoint);

            var attachedSuccessfully = adapter.WaitForAttachment();

            if (attachedSuccessfully)
            {

                //Attach an eventhandler to CareStoreSOAEvents ...
                //adapter.CareStoreSOAEvent += observer_CareStoreSOAEvent;

                Console.WriteLine("Connected ... ");
                Console.WriteLine("Calling Show ... ");

                //adapter.Show("TestApp");

                //adapter..Register("any event");

                while (true)
                {
                    Thread.Sleep(1000);
                    adapter.HostProxy.GetHost().ReportEvent("test","now");
                    //adapter.SendEvent("", "any event", "test at time: " + DateTime.Now.ToLongTimeString());
                }

            }
            else
                Console.WriteLine("Did not attach!");

            Console.ReadLine();
        }
    }
}
