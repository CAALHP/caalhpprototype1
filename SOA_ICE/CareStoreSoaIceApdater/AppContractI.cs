﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CareStoreSOAICEContracts;

namespace CareStoreSOAICE
{
    class AppContractI : AppContractDisp_
    {
        private CareStoreClientAdapter observer;
        private string appname;

        public AppContractI()
        {
        }

        public AppContractI(string appname, CareStoreClientAdapter obs)
        {
            this.appname = appname;
            this.observer = obs;
        }
        

        public override void Show(Ice.Current current__)
        {
            observer.Show();
        }

        public override void Hide(Ice.Current current__)
        {
            observer.Hide();
        }

        public override string GetName(Ice.Current current__)
        {
            return appname;
        }

        public override void ShutDown(Ice.Current current__)
        {
            observer.ShutDown();
        }

        public override bool isAlive(Ice.Current current__)
        {
            return true;
        }

        public override void Notify(string fullyQualifiedNameSpace, string key, string value, Ice.Current current__)
        {
            observer.Notify(fullyQualifiedNameSpace, key, value);
        }
    }
}
