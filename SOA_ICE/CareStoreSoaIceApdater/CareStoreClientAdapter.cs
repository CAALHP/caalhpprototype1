﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CAALHP.Contracts;
using System.Threading;

namespace CareStoreSOAICE
{
    /// <summary>
    /// This is the adapter for a CareStore App or Driver
    /// </summary>
    public class CareStoreClientAdapter : IAppCAALHPContract
    {

        public event CareStoreSOAEventHandler CareStoreSOAEvent;

        /// <summary>
        /// The identity of the callback
        /// </summary>
        public Ice.Identity ident = null;

        //Create a CareStoreNamesService proxy holdedr
        private CareStoreNameServicePrx csns = null;


        public void Show()
        {
            var args = new CareStoreSOAEvent();
            args.eventData = EventType.Show;
            OnCareStoreSOAEvent(args);
        }

        // Invoke the event
        internal virtual void OnCareStoreSOAEvent(CareStoreSOAEvent e)
        {
            try
            {
                if (CareStoreSOAEvent != null)
                    CareStoreSOAEvent(this, e);
            }
            catch (System.Exception ex)
            {
                Console.WriteLine("Error:" + ex);
            }
        }

        internal void Notify(string fullyqualifiedname, string key, string value)
        {
            var args = new CareStoreSOAEvent();
            args.eventData = EventType.Event;
            args.key = key;
            args.value = value;
            OnCareStoreSOAEvent(args);
        }


        /// <summary>
        /// This is an internal class used as a callback service
        /// </summary>
        class App : Ice.Application
        {
            /// <summary>
            /// This method will execute the callback server code needed for the CareStoreNameService 
            /// to be able to perform callbacks to the individual app
            /// </summary>
            /// <param name="args"></param>
            /// <returns></returns>
            public override int run(string[] args)
            {
                // Terminate cleanly on receipt of a signal
                shutdownOnInterrupt();

                // Create an object adapter (stored in the _adapter
                // static members)
                Ice.ObjectAdapter adapter = communicator().createObjectAdapterWithEndpoints(
                    "CareStoreApdater", "default -p 10000");

                //Add an unamed CareStoreApp with a generated UUID
                adapter.addWithUUID(new AppContractI());

                // All objects are created, allow client requests now
                adapter.activate();

                // Wait until we are done - and then clean up
                communicator().waitForShutdown();

                if (interrupted())
                    Console.Error.WriteLine(appName() + ": terminating");

                return 0;
            }
        }


        public void Register(string key)
        {
            if (csns != null && ident != null)
            {
                csns.SubscribeToEvents(key, ident);
            }

        }

        public void SendEvent(string fullyqualified, string key, string value)
        {
            if (csns != null && ident != null)
            {
                csns.ReportEvent(fullyqualified, key, value);
            }


        }

        /// <summary>
        /// Implementation of the Show command - will allow to show any alternative app by calling the show command
        /// </summary>
        /// <param name="appName">the app to show by name</param>
        public void Show(string appName)
        {
            if (csns != null)
                csns.Show(appName);
            else
                Console.WriteLine("NameServer is not available");
        }

        /// <summary>
        /// The connect application provides CareStore Apps and Drivers the ability to connect to the CAALHP CareStoreNameService.
        /// Thus, it will register with the CAALPH through the CareStoreNameService, and thus allow CAALHP to communicate cross-process
        /// </summary>
        /// <param name="endpoint">the endpoint to call</param>
        /// <param name="appname">the appname or drivername of the registering application</param>
        /// <param name="observer">the connected observer object</param>
        public void Connect(string endpoint, string appname)
        {
            //int status = 0;

            Ice.Communicator ic = null;
            try
            {

                new Task(() =>
                {
                    //Task task = new Task( () => {
                    // Create a communicator
                    ic = Ice.Util.initialize();

                    if (endpoint == null || endpoint.Equals(""))
                        endpoint = "127.0.0.1";

                    // Create a proxy 
                    Ice.ObjectPrx obj = ic.stringToProxy("CareStoreNameService:default -h " + endpoint + " -p 10001");
                    //Ice.ObjectPrx obj = ic.stringToProxy("CareStoreNameService:default -p 10001");

                    // Down-cast the proxy to a CareStoreNameService proxy
                    csns = CareStoreNameServicePrxHelper.checkedCast(obj);

                    //Create a CareStoreApp implementation - given an appname and an observer
                    var csapp = new AppContractI(appname, this);

                    //Create a nameless adapter for holding the 
                    Ice.ObjectAdapter adapter = ic.createObjectAdapter("");

                    //Create an identiy for us to track the callback obeject
                    ident = new Ice.Identity();
                    ident.name = Guid.NewGuid().ToString();
                    ident.category = "";

                    //bind the identiy with the implementation and activate it
                    adapter.add(csapp, ident);
                    adapter.activate();

                    //activate the adapter for the callback
                    csns.ice_getConnection().setAdapter(adapter);
                    //csns.addClient(ident);
                    //communicator().waitForShutdown();

                    //register the callback with the NameService object
                    csns.RegisterCareStoreApp(ident);

                    ic.waitForShutdown();
                    //});
                }).Start();

                //int counter = 0;

                //while (!ConnectionEstablished() || counter < 50)
                //{
                //    counter++;

                //    Console.WriteLine("Waiting for attachment to Naming service");
                //    Thread.Sleep(100);
                //}

                //Thread.Sleep(100);

            }
            catch (Ice.Exception e)
            {
                Console.Error.WriteLine(e);
                //status = 1;
            }
            if (ic != null)
            {
                // Clean up
                try
                {
                    ic.destroy();
                }
                catch (System.Exception e)
                {
                    Console.Error.WriteLine(e);
                    //status = 1;
                }
            }
            //Environment.Exit(status);
        }


        public bool ConnectionEstablished()
        {
            if (csns != null && ident != null)
                return true;
            else
                return false;
        }

        internal void Hide()
        {
            throw new NotImplementedException();
        }

        internal void ShutDown()
        {
            var args = new CareStoreSOAEvent();
            args.eventData = EventType.Close;
            OnCareStoreSOAEvent(args);
        }

        public void Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            throw new NotImplementedException();
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            throw new NotImplementedException();
        }

        public string GetName()
        {
            return Name;
        }

        public string Name { get; set; }

        public bool WaitForAttachment()
        {
            int counter = 0;

            while (!ConnectionEstablished() && counter < 50)
            {
                counter++;

                Console.WriteLine("Waiting for attachment to Naming service");
                Thread.Sleep(100);
            }

            return ConnectionEstablished();
        }
    }
}
