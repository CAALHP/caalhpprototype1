﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CareStoreSOAICE
{
    public enum EventType { Show, Hide, Close, Event };

    public class CareStoreSOAEvent : EventArgs
    {
        public EventType eventData { get; set; }
        public string fullyqualifiedname { get; set; }
        public string key { get; set; }
        public string value { get; set; }
    }

    // A delegate type for hooking up change notifications.
    public delegate void CareStoreSOAEventHandler(object sender, CareStoreSOAEvent e);

    public class CareStoreSOAEventObserverFacade
    {
        public event CareStoreSOAEventHandler CareStoreSOAEvent; 

        public void Show()
        {
            var args = new CareStoreSOAEvent();
            args.eventData = EventType.Show;
            OnCareStoreSOAEvent(args);
        }

        // Invoke the event
        internal virtual void OnCareStoreSOAEvent(CareStoreSOAEvent e)
        {
            try
            {
                if (CareStoreSOAEvent != null)
                    CareStoreSOAEvent(this, e);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error:" + ex);
            }
        }

        internal void Notify(string key, string value)
        {
            var args = new CareStoreSOAEvent();
            args.eventData = EventType.Event;
            args.key = key;
            args.value = value;
            OnCareStoreSOAEvent(args);

        }
    }
}
