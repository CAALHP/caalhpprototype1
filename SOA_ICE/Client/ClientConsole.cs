﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using CareStoreSOAICE;


namespace Client
{
    /// <summary>
    /// This is a demo application for testing the SOA ICE client
    /// </summary>
    class ClientConsole
    {
        
        /// <summary>
        /// This is a test application for testing how the adapter works.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {

            //Set the test app name
            var testAppName = "TestApp" + DateTime.Now.ToShortTimeString();
            var endpoint = "localhost";

            //For each adapter - attach an observer facade for handling the callback 
            //var observer = new CareStoreSoaIceApdater.CareStoreSOAEventObserverFacade(); //TODO: consider refactoring this. Maybe it can be merged into the adpater instead?
            
            //The CareStoreSoaIceAdpater handles all communication 
            var adapter = new CareStoreClientAdapter();
            
            //Task taska = new Task(() => adapter.Connect("localhost", "TestApp", adapter));
            //taska.Start();
            adapter.Connect(endpoint, testAppName);

   
            var attachedSuccessfully = adapter.WaitForAttachment();

            if (attachedSuccessfully)
            {

                //Attach an eventhandler to CareStoreSOAEvents ...
                adapter.CareStoreSOAEvent += observer_CareStoreSOAEvent;

                Console.WriteLine("Connected ... ");
                Console.WriteLine("Calling Show ... ");

                adapter.Show("TestApp");

                adapter.Register("any event");

                while (true)
                {
                    Thread.Sleep(1000);
                    adapter.SendEvent("", "any event", "test at time: " + DateTime.Now.ToLongTimeString());
                }

            }
            else
                Console.WriteLine("Did not attach!");

            Console.ReadLine();
        }

        /// <summary>
        /// Implementation of the callback event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void observer_CareStoreSOAEvent(object sender, CareStoreSOAEvent e)
        {
            Console.WriteLine("CareStoreSOAEvent called with the data: " + e.ToString());

            if (e.eventData == EventType.Event) 
            {
                Console.WriteLine("Event recieved with key: " + e.key + " and value: " + e.value);
            }
            if (e.eventData == EventType.Close)
            {
                Console.WriteLine("Close command recieved");
            }

        }
    }
}
