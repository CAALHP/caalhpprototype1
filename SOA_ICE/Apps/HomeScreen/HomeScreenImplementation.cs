﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Threading;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;
using GalaSoft.MvvmLight.Messaging;
using HomeScreen.ViewModel;
using System.Security.Permissions;

namespace HomeScreen
{
    public class HomeScreenImplementation : IAppCAALHPContract
    {
        private IAppHostCAALHPContract _host;
        private int _processId;
        private readonly MainViewModel _main;
        private const string AppName = "HomeScreen";

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        public HomeScreenImplementation(MainViewModel main)
        {
            _main = main;
            Messenger.Default.Register<NotificationMessage>(this, SendStartAppMessage);
        }

        private void SendStartAppMessage(NotificationMessage message)
        {
            if (!message.Target.Equals(GetType())) return;
            var showAppEvent = new ShowAppEvent
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                AppName = message.Notification
            };
            var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, showAppEvent);
            _host.Host.ReportEvent(serializedEvent);
            _host.ShowApp(message.Notification);
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }

        private void HandleEvent(ShowAppEvent e)
        {
            if (e.AppName.Equals(AppName))
            {
                //Show homescreen
                Show();
            }
        }

        private void HandleEvent(InstallAppCompletedEvent e)
        {
            //Show homescreen
            Show();
        }

        public string GetName()
        {
            return AppName;
        }

        public bool IsAlive()
        {
            DoEvents();

            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            //Subscribe to events so we can react to "Home" messages
            //_host.Host.SubscribeToEvents(_processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(ShowAppEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(InstallAppCompletedEvent)), _processId);
            Show();
            _main.Start();
        }

        private void UpdateAppList(object sender, EventArgs e)
        {
            DispatchToApp(() =>
            {
                _main.AppList = GetListOfInstalledApps();
                _main.UpdateApps();
            });
        }

        public void Show()
        {

            //System.Media.SystemSounds.Beep.Play();

            DoEvents(); //Allow Message Pump to Run

            DispatchToApp(() =>
            {
                _main.Show();
            });

            DoEvents(); //Allow Message Pump to Run


            //ref: http://reedcopsey.com/2011/11/28/launching-a-wpf-window-in-a-separate-thread-part-1/
            //Threading is used so we do not block the main thread.


            DispatchToApp(() =>
            {
                var proc = Process.GetProcessById(_processId);
                var handle = proc.MainWindowHandle;
                ShowWindow(handle, 3);
                _main.AppList = GetListOfInstalledApps();
                _main.Show();
            });

        }

        public IList<IPluginInfo> GetListOfInstalledApps()
        {
            return _host.GetListOfInstalledApps();
        }

        private void DispatchToApp(Action action)
        {
            
            Application.Current.Dispatcher.Invoke(action, DispatcherPriority.Send);
        
        }

        /// <summary>
        /// This is the WPF version of the WinForm Application.DoEvents that will allow to execute the message pump of an idle application
        /// </summary>
        [SecurityPermissionAttribute(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
        public void DoEvents()
        {
            DispatcherFrame frame = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
                new DispatcherOperationCallback(ExitFrame), frame);
            Dispatcher.PushFrame(frame);
        }

        /// <summary>
        /// Helper method for DoEvents()
        /// </summary>
        /// <param name="f"></param>
        /// <returns></returns>
        public object ExitFrame(object f)
        {
            ((DispatcherFrame)f).Continue = false;

            return null;
        }
    }
}