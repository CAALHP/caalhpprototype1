﻿using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace HomeScreen.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class AppTileViewModel : ViewModelBase
    {
        private string _mainTitle;
        private string _imagePath;

        /// <summary>
        /// Initializes a new instance of the AppTileViewModel class.
        /// </summary>
        public AppTileViewModel()
        {
            ClickCommand = new RelayCommand(ClickCommand_Execute);
        }

        public string MainTitle
        {
            get { return _mainTitle; }
            set
            {
                _mainTitle = value; RaisePropertyChanged("MainTitle");
            }
        }

        public string ImagePath
        {
            get { return _imagePath; }
            set
            {
                _imagePath = value; RaisePropertyChanged("ImagePath");
            }
        }
        #region Command
        
        public ICommand ClickCommand { get; private set; }

        private void ClickCommand_Execute()
        {
            Messenger.Default.Send(new NotificationMessage(this,typeof(HomeScreenImplementation),MainTitle));
        }

        #endregion
        
    }
}