﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using CAALHP.Contracts;
using CAALHP.SOA.ICE.ClientAdapters;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using HomeScreen.Views;

namespace HomeScreen.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        //private IAppCAALHPContract _host;
        public IList<IPluginInfo> AppList { get; set; }

        //private IList<string> _imagePaths;
        private ObservableCollection<AppTileViewModel> _appTiles;
        private const string AppImage = "images\\app.png";
        private const string AppImageName = "app.png";

        private AppAdapter _adapter;
        private HomeScreenImplementation _imp;
        private DispatcherTimer _timer;

        /*public IList<string> ImagePaths
        {
            get
            {
                return _imagePaths;
            }
            set
            {
                _imagePaths = value;
                RaisePropertyChanged("ImagePaths");
            }
        }*/

        public ObservableCollection<AppTileViewModel> AppTiles
        {
            get
            {
                return _appTiles;
            }
            set
            {
                _appTiles = value;
                RaisePropertyChanged(() => AppTiles);
            }
        }


        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()//IAppCAALHPContract host)>
        {
            //Debugger.Launch();
            Task.Run(() =>
            {

                const string endpoint = "localhost";
                try
                {
                    _imp = new HomeScreenImplementation(this);
                    _adapter = new AppAdapter(endpoint, _imp);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
                //_host = host;
                AppList = new List<IPluginInfo>();
                _appTiles = new ObservableCollection<AppTileViewModel>();
                //_imagePaths = new List<string>();
                UpdateApps();
            });
        }

        public void UpdateApps()
        {
            //UpdateImagePaths();
            UpdateAppTiles();
        }

        /*private void UpdateImagePaths()
        {
            foreach (var path in AppList.Select(app => Path.Combine(app.LocationDir, Appimage)).Where(File.Exists))
            {
                if (!_imagePaths.Contains(path))
                {
                    _imagePaths.Add(path);
                }
            }
            RaisePropertyChanged("ImagePaths");
        }*/

        private void UpdateAppTiles()
        {
            //add new tiles
            foreach (var app in AppList)
            {
                var imagePath = Path.Combine(app.LocationDir, AppImage);
                if (!File.Exists(imagePath)) continue;
                var current = AssemblyDirectory;
                if (current == null) continue;
                //create directory for file
                var targetDir = Path.Combine(current, "./Images/", app.Name);
                if (!Directory.Exists(targetDir))
                {
                    Directory.CreateDirectory(targetDir);
                }
                //copy file so we don't lock it from the plugin
                var targetImagePath = Path.Combine(targetDir, AppImageName);
                if (!File.Exists(targetImagePath))
                    File.Copy(imagePath, targetImagePath);
                //then refer to the copy
                if (AppTiles.Any(x => x.MainTitle == app.Name)) continue;
                var tile = new AppTileViewModel { MainTitle = app.Name, ImagePath = targetImagePath };
                AppTiles.Add(tile);
            }
            //remove obsolete tiles
            var removeThisList = AppTiles.Where(x => !AppList.Any(y => y.Name.Equals(x.MainTitle)));
            var removeThese = removeThisList.ToArray();
            for (var i = removeThese.Length - 1; i >= 0; i--)
            {
                AppTiles.Remove(removeThese[i]);
            }
            RaisePropertyChanged(() => AppTiles); //("AppTiles");
        }

        static private string AssemblyDirectory
        {
            get
            {
                var codeBase = Assembly.GetExecutingAssembly().CodeBase;
                var uri = new UriBuilder(codeBase);
                var path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        public void Show()
        {
            UpdateApps();
            Messenger.Default.Send(new NotificationMessage(this, typeof(MainView), "Show"));
        }

        public void Start()
        {
            _timer = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, UpdateAppList, Application.Current.Dispatcher);
            _timer.Start();
        }

        private void UpdateAppList(object sender, EventArgs e)
        {
            AppList = _imp.GetListOfInstalledApps();
            UpdateApps();
        }
    }
}