﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TouchlessLib;
using UserManager.EventAggregators;
using UserManager.EventMessages;

namespace UserManager.Model
{
    public class WebcamManager
    {

        private Camera cam;

        public TouchlessMgr _touchlessMgr { get; set; }

        public WebcamManager()
        {
            _touchlessMgr = new TouchlessMgr();
        }

        public void StartCapture()
        {
            new Thread(doSendImageInThread).Start();

        }

        public void StopCapture()
        {
            cam.OnImageCaptured -= cam_OnImageCaptured;
            //cam.Dispose();
            //_touchlessMgr.CurrentCamera.Dispose();
            cam = null;
            _touchlessMgr.CurrentCamera = null;

        }

        private void doSendImageInThread()
        {
            if (cam == null)
            {

                foreach (var cam2 in _touchlessMgr.Cameras)
                {
                    if (cam2.ToString().StartsWith("Logi"))
                    {
                        cam2.CaptureHeight = 600;
                        cam2.CaptureWidth = 600;
                        _touchlessMgr.CurrentCamera = cam2;

                        cam = cam2;
                        Thread.Sleep(1000);
                    }
                }
            }

            if (cam != null)
                cam.OnImageCaptured += cam_OnImageCaptured; //Do NOT subscribe for continuous

            //_imageBytes = imageToByteArray(cam.GetCurrentImage());

            //if (_imageBytes != null)
            //{
            //    //Upload image to the server (insulin monitor) at behandlerskærm
            //    _proxy.UploadImage(_imageBytes);
            //    Thread.Sleep(3000);
            //}

        }

        private void cam_OnImageCaptured(object sender, CameraEventArgs e)
        {
            //throw new NotImplementedException();

            //cam.OnImageCaptured -= cam_OnImageCaptured; //Do NOT subscribe for continuous

            System.Drawing.Image im = e.Image;

            ViewModelEventAggregator.EventAggregator.Publish(new NewWebcamImageMessage{Image = im, Sender = GetType()} );

        }
        

        //public byte[] ImageToByteArray(System.Drawing.Image imageIn)
        //{
        //    MemoryStream ms = new MemoryStream();
        //    imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
        //    return ms.ToArray();

        //}
    }
}
