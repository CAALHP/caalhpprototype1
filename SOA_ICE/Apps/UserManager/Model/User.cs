﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using CAALHP.Events.Types;
using Caliburn.Micro;

namespace UserManager.Model
{
    [Export(typeof(User))]
    public class User : PropertyChangedBase
    {
        private string _name;
        private string _email;
        private string _pin;
        private CareStoreUserLevel _userLevel;
        private string _dateOfBirth;
        private string _mobileNumber;
        private string _height;
        private bool _fingerPrintExsists;
        private bool _facialProfileExsists;
        private bool _hasProfilePhoto;
        private BitmapImage _profileImage;
        private byte[] _fingerTemplate;
        private byte[] _facialTemplate;


        public User()
        {
            Id = Guid.NewGuid().ToString();
            CreationTime = CreationTime = DateTime.Now.ToUniversalTime().ToString(CultureInfo.InvariantCulture);
        }

        public User(string id, string creationTime)
        {
            Id = id;
            CreationTime = creationTime;
        }


        public string Id { get; private set; }

        public string CreationTime
        {
            get;
            private set;
        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (value == _name) return;
                _name = value;
                NotifyOfPropertyChange(() => Name);
            }
        }

        public string DateOfBirth
        {
            get { return _dateOfBirth; }
            set
            {
                if (value == _dateOfBirth) return;
                _dateOfBirth = value;
                NotifyOfPropertyChange(() => DateOfBirth);
            }
        }

        public string Height
        {
            get { return _height; }
            set
            {
                if (value == _height) return;
                _height = value;
                NotifyOfPropertyChange(() => Height);
            }
        }

        public string Email
        {
            get { return _email; }
            set
            {
                if (value == _email) return;
                _email = value;
                NotifyOfPropertyChange(() => Email);
            }
        }

        public string MobileNumber
        {
            get { return _mobileNumber; }
            set
            {
                if (value == _mobileNumber) return;
                _mobileNumber = value;
                NotifyOfPropertyChange(() => MobileNumber);
            }
        }

        public string PhotoLink
        {
            get
            {
                return Id + ".jpg";
            }
        }

        public BitmapImage ProfilePhoto
        {
            get
            {
                if (_profileImage == null)
                    _profileImage = CreateProfilePictureFromFile();

                return _profileImage;
            }
            set
            {
                if (Equals(value, _profileImage)) return;
                _profileImage = value;
                NotifyOfPropertyChange(() => ProfilePhoto);
            }
        }

        private BitmapImage CreateProfilePictureFromFile()
        {
            try
            {
                var photoPath = Utils.ImagePath + PhotoLink;
                var img = new BitmapImage();
                img.BeginInit();
                img.CacheOption = BitmapCacheOption.OnLoad;
                img.UriSource = new Uri(photoPath, UriKind.Absolute);
                img.EndInit();
                img.Freeze();
                return img;
            }
            catch (Exception e)
            {
                var photoPath = Utils.ImagePath + "Default.png";
                var img = new BitmapImage();
                img.BeginInit();
                img.CacheOption = BitmapCacheOption.OnLoad;
                img.UriSource = new Uri(photoPath, UriKind.Absolute);
                img.EndInit();
                img.Freeze();
                return img;
            }
        }

        public string Pin
        {
            get { return _pin; }
            set
            {
                if (value == _pin) return;
                _pin = value;
                NotifyOfPropertyChange(() => Pin);
            }
        }

        public bool FingerTemplateExsists
        {
            get { return _fingerPrintExsists; }
            set
            {
                if (value.Equals(_fingerPrintExsists)) return;
                _fingerPrintExsists = value;
                NotifyOfPropertyChange(() => FingerTemplateExsists);
            }
        }

        public bool FacialTemplateExsists
        {
            get { return FingerTemplate != null; }
            set
            {
                if (value.Equals(_facialProfileExsists)) return;
                _facialProfileExsists = value;
                NotifyOfPropertyChange(() => FacialTemplateExsists);
            }
        }

        public byte[] FingerTemplate
        {
            get { return _fingerTemplate; }
            set
            {
                _fingerTemplate = value;
                if (value == null) return;
                FingerTemplateExsists = true;
            }
        }

        public byte[] FacialTemplate
        {
            get { return _facialTemplate; }
            set
            {
                _facialTemplate = value;
                if (value == null) return;
                FacialTemplateExsists = true;
            }
        }

        public CareStoreUserLevel UserLevel
        {
            get { return _userLevel; }
            set
            {
                if (value == _userLevel) return;
                _userLevel = value;
                NotifyOfPropertyChange(() => UserLevel);
            }
        }
    }
}
