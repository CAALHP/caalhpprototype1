﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAALHP.Events.Types;


namespace UserManager.EventMessages
{
    public class UserListResponseMessage: BaseMessage
    {
        public string PhotoRoot { get; set; }
        public List<UserProfile> Users { get; set; }
    }
}
