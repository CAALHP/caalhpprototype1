﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManager.Model;

namespace UserManager.EventMessages
{
    public class UserToEditCompleteMessage: BaseMessage
    {
        public User User { get; set; }
        public string PhotoRoot { get; set; }
        public bool NewPhoto { get; set; }
    }
}
