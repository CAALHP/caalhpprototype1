﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManager.EventMessages
{
    public class FingerEnrollResponsMessage: BaseMessage
    {
        public string UserId { get; set; }
        public byte[] Template { get; set; }
    }
}
