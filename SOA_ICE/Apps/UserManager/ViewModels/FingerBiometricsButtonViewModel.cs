﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using UserManager.EventAggregators;
using UserManager.EventMessages;
using UserManager.Model;

namespace UserManager.ViewModels
{
    public class FingerBiometricsButtonViewModel : BiometricsButtonViewModel, IHandle<BaseMessage>
    {
        
        public FingerBiometricsButtonViewModel(User u)
            : base(u)
        {
            ViewModelEventAggregator.EventAggregator.Subscribe(this);

            TypeName = "Finger";

            UserBiometricsExsists = u.FingerTemplateExsists;

            if (UserBiometricsExsists)
            {
                Icon = @"\Images\OkIcon.png";
                DeviceStatus = "";
            }
            else
            {
                Icon = @"\Images\ErrorIcon.png";
                DeviceStatus = "Tryk her for at oprette";
            }

        }

        public byte[] FingerTemplate { get; private set; }

        public override void Enroll()
        {
            EnrollingRunning = true;
            
            ViewModelEventAggregator.EventAggregator.Publish(new RequestFingerEnrollMessage() { UserId = User.Id, Sender = GetType() });
        }

        public override void Dispose()
        {
            //clean up here if something needs to be.
        }

        public override void StoreTemplate(User userToEdit)
        {
            userToEdit.FingerTemplate = FingerTemplate;
        }

        public void Handle(BaseMessage message)
        {
            var obj = message as dynamic;
            HandleMessage(obj);
        }

        public void HandleMessage(BaseMessage message)
        {
            //this function is empty by design.
        }


        public void HandleMessage(UpdateBiometricsMessage msg)
        {
            IsDeviceEnabled = Utils.FingerDriverEnabled;
        }

        public void HandleMessage(FingerEnrollResponsMessage msg)
        {
            if (string.Equals(msg.UserId, User.Id))
            {
                FingerTemplate = msg.Template;
                UserBiometricsExsists = true;
            }

            EnrollingRunning = false;

        }

        public void HandleMessage(FingerEnrollErrorMessage msg)
        {
            EnrollingRunning = false;
        }
    }
}
