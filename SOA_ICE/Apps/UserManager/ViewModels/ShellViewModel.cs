using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using CAALHP.SOA.ICE.ClientAdapters;
using Caliburn.Micro;
using UserManager.EventAggregators;
using UserManager.EventMessages;

using UserManager.Model;

namespace UserManager.ViewModels
{
    [Export(typeof(IShell))]
    public class ShellViewModel : Screen, IShell, IHandle<BaseMessage>
    {
        private UserManagerImplementation _caalhpConctact;
        private AppAdapter _adapter;
        const string endpoint = "localhost";

        private bool _isWindowEnabled;
        private UserListViewModel _testView;
        private string _header;
        private EditUserViewModel _editUserView;
        private System.Timers.Timer _statusTimer = new System.Timers.Timer(30000);
        private string _statusText;

        public ShellViewModel()
        {

            Application.Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;

            Task.Run(() =>
                {
                    try
                    {
                        _caalhpConctact = new UserManagerImplementation();
                        _adapter = new AppAdapter(endpoint, _caalhpConctact);

                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message);

                    }
                });

            ViewModelEventAggregator.EventAggregator.Subscribe(this);
            
            UserListControl = new UserListViewModel();

            Header = "CareStore User Manager";

            IsWindowEnabled = true;

            _statusTimer.Elapsed += StatusTimerElapsed;
        }

        void StatusTimerElapsed(object sender, ElapsedEventArgs e)
        {
            StatusText = null;
        }

        void Current_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            MessageBox.Show(e.Exception.Message);
        }

        public string Header
        {
            get { return _header; }
            set
            {
                if (value == _header) return;
                _header = value;
                NotifyOfPropertyChange(() => Header);
            }
        }

        public string StatusText
        {
            get { return _statusText; }
            set
            {
                if (value == _statusText) return;
                _statusText = value;
                NotifyOfPropertyChange(() => StatusText);

                if (string.IsNullOrEmpty(value)) return;
                _statusTimer.Start();

            }
        }

        public UserListViewModel UserListControl
        {
            get { return _testView; }
            set
            {
                if (Equals(value, _testView)) return;
                _testView = value;
                NotifyOfPropertyChange(() => UserListControl);
            }
        }

        public EditUserViewModel EditUserControl
        {
            get { return _editUserView; }
            set
            {
                if (Equals(value, _editUserView)) return;
                _editUserView = value;
                NotifyOfPropertyChange(() => EditUserControl);
                NotifyOfPropertyChange(() => ShowCreateUserButton);
            }
        }

        public bool ShowCreateUserButton
        {
            get { return EditUserControl == null; }

        }

        public void CreateNewUser()
        {
            EditUserControl = new EditUserViewModel();
        }


        public bool IsWindowEnabled
        {
            get { return _isWindowEnabled; }
            set
            {
                if (value.Equals(_isWindowEnabled)) return;
                _isWindowEnabled = value;
                NotifyOfPropertyChange(() => IsWindowEnabled);
            }
        }

        private void Show()
        {
            IsWindowEnabled = true;

            //   MessageBox.Show("Show is called");
            //Application.Current.MainWindow.WindowState = WindowState.Minimized;
            //Application.Current.MainWindow.WindowState = WindowState.Maximized;

            //_windowManager.ShowWindow(this);

            //  this.ActivateItem(this);

        }

        public void Handle(BaseMessage message)
        {
            var obj = message as dynamic;
            HandleMessage(obj);
        }

        private void HandleMessage(BaseMessage o)
        {
            //this function is empty by design
        }

        private void HandleMessage(ShowMessage o)
        {
            Show();
        }

        private void HandleMessage(UserToEditMessage msg)
        {
            var userToEdit = msg.UserToEdit;
            if (EditUserControl == null)
                EditUserControl = new EditUserViewModel(userToEdit);
            else
                EditUserControl.ChangedUserToEdit(userToEdit);
        }

        private void HandleMessage(UserToEditCompleteMessage o)
        {
            RemovedEditUserControl();
        }

        private void HandleMessage(CancelUserEditMessage o)
        {
            RemovedEditUserControl();
        }

        private void HandleMessage(NewUserMessage o)
        {
            RemovedEditUserControl();
        }

        private void HandleMessage(StatusMessage o)
        {
            StatusText = o.Message;
        }

        private void RemovedEditUserControl()
        {
            EditUserControl = null;
        }

        public void WindowClose(object sender, CancelEventArgs e)
        {
            e.Cancel = true;

            IsWindowEnabled = false;

            //var tmp = sender as Window;
            //tmp.Hide();

            //Thread.Sleep(5000);
            //Show();
            //tmp.Show();

        }
    }
}