﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Caliburn.Micro;
using UserManager.EventAggregators;
using UserManager.EventMessages;
using UserManager.Model;
using UserManager.Properties;

namespace UserManager.ViewModels
{
    public class FacialBiometricsButtonViewModel : BiometricsButtonViewModel, IHandle<BaseMessage>
    {
        private WebcamManager _webcamManager = new WebcamManager();

        public FacialBiometricsButtonViewModel(User u)
            : base(u)
        {
            TypeName = "Ansigt";

            UserBiometricsExsists = u.FacialTemplateExsists;

            UpdateLayout();
            

            ViewModelEventAggregator.EventAggregator.Subscribe(this);

            _webcamManager.StartCapture();
        }

        private void UpdateLayout()
        {
            if (UserBiometricsExsists)
            {
                Icon = @"\Images\OkIcon.png";
                DeviceStatus = "";
            }
            else
            {
                Icon = @"\Images\ErrorIcon.png";
                DeviceStatus = "Tryk her for at oprette";
            }
        }

        public byte[] FacialTemplate { get; private set; }
        
        public override void Dispose()
        {
            _webcamManager.StopCapture(); 
        }

        public override void StoreTemplate(User userToEdit)
        {
            userToEdit.FacialTemplate = FacialTemplate;
        }

        public override void Enroll()
        {
            EnrollingRunning = true;
            _webcamManager.StopCapture();

            Thread.Sleep(1000);
            ViewModelEventAggregator.EventAggregator.Publish(new RequestFacialEnrollMessage() { UserId = User.Id, Sender = GetType() });
           
        }

        public void Handle(BaseMessage message)
        {
            var obj = message as dynamic;
            HandleMessage(obj);
        }

        public void HandleMessage(BaseMessage message)
        {
            //this function is empty by design.
        }

        public void HandleMessage(EnrollResponsMessage msg)
        {
            if (string.Equals(msg.UserId, User.Id))
            {
                FacialTemplate = msg.Template;
                UserBiometricsExsists = true;
            }

            _webcamManager.StartCapture();
            UpdateLayout();

            EnrollingRunning = false;
        }

        public void HandleMessage(EnrollErrorResponsMessage msg)
        {

            _webcamManager.StartCapture();
            EnrollingRunning = false;
        }

        public void HandleMessage(UpdateBiometricsMessage msg)
        {
            IsDeviceEnabled = Utils.FacialDriverEnabled;
        }
    }
}
