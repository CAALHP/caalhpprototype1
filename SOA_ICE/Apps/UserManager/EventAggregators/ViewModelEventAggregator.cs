﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;

namespace UserManager.EventAggregators
{
    public class ViewModelEventAggregator
    {
        static EventAggregator eventAggregator;

        public static EventAggregator EventAggregator
        {
            get { return eventAggregator ?? (eventAggregator = new EventAggregator()); }
        }

    }
}
