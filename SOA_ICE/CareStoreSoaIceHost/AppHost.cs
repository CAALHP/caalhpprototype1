using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CAALHP.Contracts;
using CAALHP.Library.Hosts;
using CAALHP.Library.Managers;
using CareStoreSOAICE.Host;

namespace CareStoreSoaIceHost
{
    public class AppHost : Host, IAppHost
    {
        private readonly Dictionary<int, AppProcess> _activeAppsDictionary = new Dictionary<int, AppProcess>();
        private int _activeAppsDictionaryIndex;

        //private readonly IAppHostView _hostView;
        //protected IHostObjectView HostObjectView { get; private set; }

        public AppHost(EventManager eventManager)
            : base(eventManager)
        {
            //HostObjectView = new HostObjectView(this);
            //_hostView = new AppHostView(this);
        }

        public Dictionary<int, ProcessInfo> GetRunningAppsProcessInfo()
        {
            var collection = new Dictionary<int, ProcessInfo>();
            foreach (var record in _activeAppsDictionary)
            {
                var counter = record.Value.PerfCounter;
                var processInfo = new ProcessInfo();
                processInfo.ProcessId = record.Value.ProcessId;
                //processInfo.Name = record.Value.App.ToString();
                counter.CategoryName = "Process";

                counter.CounterName = "% Processor Time";
                processInfo.PercentProcessorTime = counter.NextValue();

                counter.CounterName = "Private Bytes";
                processInfo.PrivateBytes = (Int64)counter.NextValue();
                collection.Add(record.Key, processInfo);
            }
            return collection;
        }

        public IList<IPluginInfo> GetListOfRunningApps()
        {
            return (from active in _activeAppsDictionary
                    let token = AddInController.GetAddInController(active.Value.App).Token
                    select new PluginInfo()
                    {
                        Name = token.Name,
                        LocationDir = Path.Combine(AddInRoot, "AddIns", token.AssemblyName.Name),
                        Index = active.Key
                    }).Cast<IPluginInfo>().ToList();
            //return _activeAppsDictionary.Select(app => app.Key + " : " + AddInController.GetAddInController(app.Value.App).Token.Name).ToList();
        }

        public void StartApp(int appIndex)
        {
            //var ps = new PermissionSet(PermissionState.None);
            //ps.AddPermission(new SecurityPermission(SecurityPermissionFlag.Execution));
            //new ParameterizedThreadStart(StartApp(appIndex,ps)));
            //StartApp(appIndex, ps);

        }

        //public void StartApp(int appIndex, PermissionSet permissions)
        //{
        //    UpdateAddInStore();
        //    var tokens = AddInStore.FindAddIns(typeof(IAppView), AddInRoot);
        //    //var app = tokens[appIndex].Activate<IApp>(AddInSecurityLevel.Internet);
        //    //activate in a new process
        //    //var app = tokens[appIndex].Activate<IApp>(new AddInProcess(),AddInSecurityLevel.Internet);
        //    //activate in a new process and set permissions with permissionset
        //    if (tokens.Count > appIndex)
        //    {
        //        var app = tokens[appIndex].Activate<IAppView>(new AddInProcess(), AddInSecurityLevel.FullTrust);
        //        //var app = tokens[appIndex].Activate<IAppView>(new AddInProcess(), permissions);
        //        //var processId = AddInController.GetAddInController(app).AddInEnvironment.Process.ProcessId;
        //        //_driverDictionary.Add(_driverDictionaryIndex++, new DriverProcess(driver, processId, GetPerformanceCounterByProcessId(processId)));
        //        var process = new AppProcess(app);
        //        app.Initialize(_hostView, process.ProcessId);
        //        _activeAppsDictionary.Add(_activeAppsDictionaryIndex++, process);
        //        Console.WriteLine("activated app: " + tokens[appIndex].Name);
        //    }
        //}

        public void CloseApp(int appIndex)
        {
            //if (_activeAppsDictionary.ContainsKey(appIndex))
            //{

            //    try
            //    {
            //        EventManager.EventSubject.Detach(_activeAppsDictionary[appIndex].ProcessId);
            //        var controller = AddInController.GetAddInController(_activeAppsDictionary[appIndex].App);
            //        if (controller.AddInEnvironment != null)
            //            if (controller.AddInEnvironment.Process != null)
            //                controller.AddInEnvironment.Process.Shutdown();
            //        if (controller.AppDomain != null)
            //            AppDomain.Unload(controller.AppDomain);
            //        controller.Shutdown();
            //    }
            //    finally
            //    {
            //        _activeAppsDictionary.Remove(appIndex);
            //    }
            //}
        }

        public void SwitchToApp(int appIndex)
        {
            //_activeAppsDictionary[appIndex].App.Show();
            //Console.WriteLine(_activeAppsDictionary[appIndex].App.GetName());
        }

        public async override Task NotifyEventToPlugin(KeyValuePair<string, string> value, int processId)
        {
            //for (int i = _activeAppsDictionary.Count - 1; i >= 0; i--)
            //{
            //    var record = _activeAppsDictionary[i];
            //    if (record.ProcessId == processId)
            //    {
            //        await Task.Run(() => record.App.Notify(value));
            //    }
            //}
        }

        public override IList<IPluginInfo> GetListOfInstalledApps()
        {
            //UpdateAddInStore();
            //var tokens = AddInStore.FindAddIns(typeof(IAppView), AddInRoot);
            //return tokens.Select(token => new PluginInfo
            //{
            //    Name = token.Name,
            //    LocationDir = Path.Combine(AddInRoot, "AddIns", token.AssemblyName.Name)
            //}).Cast<IPluginInfo>().ToList();
            return null;
        }

        public override void ShowApp(string appName)
        {
            //Is app installed?
            var installed = IsAppInstalled(appName);
            if (installed)
            {
                var index = GetIndexOfInstalledApp(appName);

                ////Is app already started?
                //var runningApps = GetListOfRunningApps();
                var running = GetListOfRunningApps().Any(x => x.Name == appName);
                if (!running)
                {
                    //start app
                    StartApp(index);
                }
                //Show app:
                var runningIndex = GetIndexOfRunningApp(appName);
                if (runningIndex != -1)
                    SwitchToApp(runningIndex);

            }
        }

        public override void CloseApp(string appName)
        {
            var list = GetListOfRunningApps();
            if (list.All(x => !x.LocationDir.EndsWith(appName))) return;
            var app = list.First(x => x.LocationDir.EndsWith(appName));
            if (app == null) return;
            var index = list.IndexOf(app);
            CloseApp(index);
        }

        public override IList<IPluginInfo> GetListOfInstalledDeviceDrivers()
        {
            throw new NotImplementedException();
        }

        private bool IsAppInstalled(string name)
        {
            return GetListOfInstalledApps().Any(x => x.Name == name);
        }

        private int GetIndexOfInstalledApp(string name)
        {
            var installedApps = GetListOfInstalledApps();
            //var index = 0;
            for (var i = 0; i < installedApps.Count; i++)
            {
                if (installedApps[i].Name == name)
                {
                    return i;
                }
            }
            return -1;
        }

        private int GetIndexOfRunningApp(string name)
        {
            //foreach (var appProcess in from appProcess in _activeAppsDictionary let token = AddInController.GetAddInController(appProcess.Value.App).Token where token.Name.Equals(name) select appProcess)
            //{
            //    return appProcess.Key;
            //}
            return -1;
        }


        public void StartApp(int appIndex, System.Security.PermissionSet permissions)
        {
            throw new NotImplementedException();
        }

        /*public IList<IPluginInfo> GetListOfRunningApps()
        {
            throw new NotImplementedException();
        }*/
    }
}