﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using CAALHP.Contracts;
using CAALHP.Library.Hosts;
using CAALHP.Library.Managers;
using CAALHP.Library.Observer;

namespace CareStoreSoaIceHost
{
    public abstract class Host : IHost
    {
        protected string AddInRoot { get; private set; }
        //protected IHostBaseView HostObjectView { get; private set; }
        protected EventManager EventManager { get; private set; }

        protected Host(EventManager eventManager)
        {
            EventManager = eventManager;
            // Assume that the current directory is the application folder,  
            // and that it contains the pipeline folder structure.
            AddInRoot = Environment.CurrentDirectory + "\\Ice";
            if (!Directory.Exists(AddInRoot))
            {
                Directory.CreateDirectory(AddInRoot);
            }
            //HostObjectView = new HostView(this);
        }

        public void SubscribeToEvents(int processId)
        {
            Console.WriteLine("Subscribing process:{0} to events via {1}", processId, this);
            EventManager.EventSubject.Attach(new EventObserver(EventManager.EventSubject, processId, this, ""), "");

        }

        public void SubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            Console.WriteLine("Subscribing process:{0} to {2} events via {1}", processId, this, fullyQualifiedNameSpace);
            EventManager.EventSubject.Attach(new EventObserver(EventManager.EventSubject, processId, this, fullyQualifiedNameSpace), fullyQualifiedNameSpace);
        }

        public void UnSubscribeToEvents(int processId)
        {
            EventManager.EventSubject.Detach(processId);
        }

        public void UnSubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            EventManager.EventSubject.Detach(processId, fullyQualifiedNameSpace);
        }

        public void ReportEvent(KeyValuePair<string, string> value)
        {
            //EventManager.EventSubject.SubjectState = value;
            //Console.WriteLine(value);
            ReportEvent("default", value);
        }

        public void ReportEvent(string fqns, KeyValuePair<string, string> value)
        {
            EventManager.EventSubject.SubjectState = value;
            //Console.WriteLine(value);
            EventManager.EventSubject.Notify(fqns);
        }

        public abstract Task NotifyEventToPlugin(KeyValuePair<string, string> value, int processId);
        public abstract IList<IPluginInfo> GetListOfInstalledApps();
        public abstract void ShowApp(string appName);
        public abstract void CloseApp(string appName);
        public abstract IList<IPluginInfo> GetListOfInstalledDeviceDrivers();
    }
}
