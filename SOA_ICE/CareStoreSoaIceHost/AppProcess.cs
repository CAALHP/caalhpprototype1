﻿using System.Diagnostics;
using CAALHP.Utils.Helpers;

namespace CareStoreSoaIceHost
{
    public class AppProcess
    {
        //public IAppView App { get; private set; }
        public int ProcessId { get; private set; }
        public PerformanceCounter PerfCounter { get; private set; }
        public AppProcess(int processId)//IAppView app)
        {
            //App = app;
            //ProcessId = AddInController.GetAddInController(app).AddInEnvironment.Process.ProcessId;
            ProcessId = processId;
            PerfCounter = PerformanceCounterHelper.GetPerformanceCounterByProcessId(ProcessId);
        }
    }
}