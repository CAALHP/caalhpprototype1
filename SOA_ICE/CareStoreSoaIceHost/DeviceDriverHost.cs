using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using CAALHP.Contracts;
using CAALHP.Library.Hosts;
using CAALHP.Library.Managers;

namespace CareStoreSOAICE.Host
{
    public class DeviceDriverHost : CareStoreSoaIceHost.Host, IDeviceDriverHost
    {
        //public IHost Host { get; set; }

        //private readonly Dictionary<int, DriverProcess> _driverDictionary = new Dictionary<int, DriverProcess>();
        //private int _driverDictionaryIndex;

        //protected IDeviceDriverHostView HostView { get; private set; }

        public DeviceDriverHost(EventManager eventManager)
            : base(eventManager)
        {
            //  HostView = new DeviceDriverHostView(this);
            ActivateDrivers();
        }

        public void ActivateDrivers()
        {
            //UpdateAddInStore();
            //var tokens = AddInStore.FindAddIns(typeof(IDeviceDriverView), AddInRoot);
            //foreach (var token in tokens)
            //{
            //    //async activation of each driver:
            //    ActivateDriver(token);
            //}

            Console.WriteLine("Starting Client Applications...");

            //string pathToDrivers = @"C:\Users\sw\Documents\Dev\CareStore\caalhpprototype1\SOA_ICE\Client\bin\Debug";
            string pathToDrivers = Directory.GetCurrentDirectory() + @"..\SOA_ICE\Client\bin\Debug";

            //Directory.
            //Directory.
            var a = new DirectoryInfo(pathToDrivers);

            /*foreach (var file in a.GetFiles("*.exe"))
            {


                var service = new System.Diagnostics.Process();
                try
                {
                    //service.Process.StartInfo.Arguments = 
                    service.StartInfo.WorkingDirectory = pathToDrivers;//@"C:\Users\sw\Documents\Dev\CareStore\caalhpprototype1\SOA_ICE\Client\bin\Debug";
                    service.StartInfo.FileName = file.FullName;//@"C:\Users\sw\Documents\Dev\CareStore\caalhpprototype1\SOA_ICE\Client\bin\Debug\Client.exe";
                    service.StartInfo.UseShellExecute = true; //use false if you want it in the existing console ...
                    service.Start();

                    Thread.Sleep(200);

                }
                catch (System.Exception e)
                {
                    Console.WriteLine(e);
                }
            }*/
        }

        //private async void ActivateDriver(AddInToken token)
        //{
        //    if (!TokenIsInDictionary(token))
        //    {
        //        await ActivateAsync(token);
        //        //Console.WriteLine("activated driver: " + token.Name);
        //    }
        //}

        //private async Task ActivateAsync(AddInToken token)
        //{
        //    await Task.Run(() =>
        //    {
        //        //var driver = token.Activate<IDriverView>(new AddInProcess(), AddInSecurityLevel.Internet);
        //        var driver = token.Activate<IDeviceDriverView>(new AddInProcess(), AddInSecurityLevel.FullTrust);
        //        //var driver = token.Activate<IDriverView>(AddInSecurityLevel.Internet);
        //        var driverProcess = new DriverProcess(driver);
        //        driver.Initialize(HostView, driverProcess.ProcessId);
        //        _driverDictionary.Add(_driverDictionaryIndex++, driverProcess);
        //    });
        //}

        //private bool TokenIsInDictionary(AddInToken token)
        //{
        //    if (token == null) throw new ArgumentNullException("token");
        //    return _driverDictionary.Values.Any(driver => AddInController.GetAddInController(driver.Driver).Token.Equals(token));
        //}

        public void driver_NewMeasurementEvent(object sender, EventArgs e)
        {
            Console.WriteLine("A new measurement was made");
        }

        //public Dictionary<int, ProcessInfo> GetRunningDriversProcessInfo()
        //{
        //    var collection = new Dictionary<int, ProcessInfo>();
        //    foreach (var record in _driverDictionary)
        //    {
        //        var counter = record.Value.PerfCounter;
        //        var processInfo = new ProcessInfo();
        //        processInfo.ProcessId = record.Value.ProcessId;
        //        processInfo.Name = record.Value.Driver.ToString();
        //        counter.CategoryName = "Process";

        //        counter.CounterName = "% Processor Time";
        //        processInfo.PercentProcessorTime = counter.NextValue();

        //        counter.CounterName = "Private Bytes";
        //        processInfo.PrivateBytes = (Int64)counter.NextValue();
        //        collection.Add(record.Key, processInfo);
        //    }
        //    return collection;
        //}

        /// <summary>
        /// Bending the truth on this one.
        /// </summary>
        /// <returns>List of drivers</returns>
        //public List<string> GetListOfActiveDevices()
        //{
        //    return _driverDictionary.Select(record => record.Key + " : " + AddInController.GetAddInController(record.Value.Driver).Token.Name).ToList();
        //}

        //public void UpdateDriverList()
        //{
        //    ActivateDrivers();
        //}

        //public double GetMeasurementFromDevice(int index)
        //{
        //    return _driverDictionary.ContainsKey(index) ? _driverDictionary[index].Driver.GetMeasurement() : double.NaN;
        //}

        //public override IList<IPluginInfo> GetListOfInstalledDeviceDrivers()
        //{
        //    UpdateAddInStore();
        //    var tokens = AddInStore.FindAddIns(typeof(IDeviceDriverView), AddInRoot);
        //    return tokens.Select(token => new PluginInfo
        //    {
        //        Name = token.Name,
        //        LocationDir = Path.Combine(AddInRoot, "AddIns", token.AssemblyName.Name)
        //    }).Cast<IPluginInfo>().ToList();
        //}

        //public async override Task NotifyEventToPlugin(KeyValuePair<string, string> value, int processId)
        //{
        //    for (var i = _driverDictionary.Count - 1; i >= 0; i--)
        //    {
        //        var record = _driverDictionary[i];
        //        if (record.ProcessId == processId)
        //        {
        //            await Task.Run(() => record.Driver.Notify(value));
        //            //var t = new Thread(() => record.Service.Notify(value));
        //            //t.Start();
        //        }
        //    }
        //}

        public override IList<IPluginInfo> GetListOfInstalledApps()
        {
            throw new NotImplementedException();
        }

        public override void ShowApp(string appName)
        {
            throw new NotImplementedException();
        }

        public override void CloseApp(string appName)
        {
            throw new NotImplementedException();
        }


        public Dictionary<int, ProcessInfo> GetRunningDriversProcessInfo()
        {
            return null;
        }

        public List<string> GetListOfActiveDevices()
        {
            return null;
        }

        public void UpdateDriverList()
        {
            throw new NotImplementedException();
        }

        public double GetMeasurementFromDevice(int index)
        {
            throw new NotImplementedException();
        }

        public override Task NotifyEventToPlugin(KeyValuePair<string, string> value, int processId)
        {
            throw new NotImplementedException();
        }

        public override IList<IPluginInfo> GetListOfInstalledDeviceDrivers()
        {
            throw new NotImplementedException();
        }
    }
}