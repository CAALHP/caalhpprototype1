using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using CAALHP.Contracts;
using CAALHP.Library.Hosts;
using CAALHP.Library.Managers;
using CareStoreSoaIceHost;

namespace CareStoreSOAICE.Host
{
    public class ServiceHost : CareStoreSoaIceHost.Host, IServiceHost
    {
        //private readonly Dictionary<int, ServiceProcess> _serviceDictionary = new Dictionary<int, ServiceProcess>();
        //private int _serviceDictionaryIndex;

        //protected ServiceHostView HostView { get; private set; }

        public IHostManager HostManager { get; set; }

        public void ActivateDrivers()
        {
            HostManager.ActivateDeviceDrivers();
        }

        public ServiceHost(EventManager eventManager)
            : base(eventManager)
        {
            //HostView = new ServiceHostView(this);
            ActivateServices();
            //StartServices();
        }

        ~ServiceHost()
        {
            StopServices();
        }

        private void ActivateServices()
        {
            //UpdateAddInStore();
            ////var tokens = AddInStore.FindAddIns(typeof(IServiceView), AddInRoot);
            //foreach (var token in tokens)
            //{
            //    ActivateService(token);
            //}

            Console.WriteLine("Starting CareStoreHostNamingServer ...");

            var careStoreHostNamingServer = new CareStoreHostNamingServer();

            Task server = new Task(() =>
            {
                var args = new string[] { };
                Environment.Exit(careStoreHostNamingServer.main(args));
                //careStoreHostNamingServer.main(args);
            });
            server.Start();


            //Task task = new Task(() => {
            //    var args = new string[] { };
            //    Environment.Exit(careStoreHostNamingServer.main(args));
            //}
            //);

            Thread.Sleep(500);

          
     
        }

        //private async void ActivateService(AddInToken token)
        //{
        //    if (!TokenIsInDictionary(token))
        //    {
        //        await ActivateAsync(token);
        //    }
        //}

        //private async Task ActivateAsync(AddInToken token)
        //{
        //    await Task.Run(() =>
        //    {
        //        var service = token.Activate<IServiceView>(new AddInProcess(), AddInSecurityLevel.FullTrust);
        //        //var Service = token.Activate<IServiceView>(AddInSecurityLevel.Internet);

        //        var serviceProcess = new ServiceProcess(service);
        //        service.Initialize(HostView, serviceProcess.ProcessId);
        //        _serviceDictionary.Add(_serviceDictionaryIndex++, serviceProcess);
        //        Console.WriteLine("activated Service: " + token.Name);
        //    });
        //}

        //private void StartServices()
        //{
        //    foreach (var serviceProcess in _serviceDictionary)
        //    {
        //        serviceProcess.Value.Service.Start();
        //    }
        //}

        private void StopServices()
        {
            /*foreach (var serviceProcess in _serviceDictionary)
            {
                serviceProcess.Value.Service.Stop();
            }*/
        }

        //private bool TokenIsInDictionary(AddInToken token)
        //{
        //    if (token == null) throw new ArgumentNullException("token");
        //    return _serviceDictionary.Values.Any(service => AddInController.GetAddInController(service.Service).Token.Equals(token));
        //}

        //public async override Task NotifyEventToPlugin(KeyValuePair<string, string> value, int processId)
        //{
        //    for (var i = _serviceDictionary.Count - 1; i >= 0; i--)
        //    {
        //        var record = _serviceDictionary[i];
        //        if (record.ProcessId == processId)
        //        {
        //            await Task.Run(() => record.Service.Notify(value));
        //            //var t = new Thread(() => record.Service.Notify(value));
        //            //t.Start();
        //        }
        //    }
        //}

        public override IList<IPluginInfo> GetListOfInstalledApps()
        {
            return HostManager != null ? HostManager.GetListOfInstalledApps() : null;
        }

        public override IList<IPluginInfo> GetListOfInstalledDeviceDrivers()
        {
            return HostManager != null ? HostManager.GetListOfInstalledDeviceDrivers() : null;
        }

        public override void ShowApp(string appName)
        {
            throw new NotImplementedException();
        }

        public override void CloseApp(string appName)
        {
            if (HostManager != null)
            {
                HostManager.CloseApp(appName);
            }
        }



        public override Task NotifyEventToPlugin(KeyValuePair<string, string> value, int processId)
        {
            throw new NotImplementedException();
        }

        IHostManager IServiceHost.HostManager { get; set; }

        void IServiceHost.ActivateDrivers()
        {
            throw new NotImplementedException();
        }

        void IHost.SubscribeToEvents(int processId)
        {
            throw new NotImplementedException();
        }

        void IHost.UnSubscribeToEvents(int processId)
        {
            throw new NotImplementedException();
        }

        void IHost.ReportEvent(KeyValuePair<string, string> value)
        {
            throw new NotImplementedException();
        }

        void IHost.ReportEvent(string fullyQualifiedNameSpace, KeyValuePair<string, string> value)
        {
            throw new NotImplementedException();
        }

        Task IHost.NotifyEventToPlugin(KeyValuePair<string, string> value, int processId)
        {
            throw new NotImplementedException();
        }

        IList<IPluginInfo> IHost.GetListOfInstalledApps()
        {
            throw new NotImplementedException();
        }

        void IHost.ShowApp(string appName)
        {
            throw new NotImplementedException();
        }

        void IHost.CloseApp(string appName)
        {
            throw new NotImplementedException();
        }

        IList<IPluginInfo> IHost.GetListOfInstalledDeviceDrivers()
        {
            throw new NotImplementedException();
        }

        void IHost.UnSubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            throw new NotImplementedException();
        }

        void IHost.SubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            throw new NotImplementedException();
        }


        public IList<string> GetListOfEventTypes()
        {
            throw new NotImplementedException();
        }
    }
}