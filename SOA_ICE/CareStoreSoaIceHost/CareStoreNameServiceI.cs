﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAALHP.Library.Hosts;
using CareStoreSOAICEContracts;

namespace CareStoreSOAICE
{
    public class CareStoreNameServiceI : CareStoreNameServiceDisp_
    {
        public List<AppContract> RegisteredComponets { get; set; }
        //public List<AppContractPrx> Observers { get; set; } 
        public HostManager Hostmanager { get; set; }

        public CareStoreNameServiceI()
        {
            RegisteredComponets = new List<AppContract>();
            //Observers = new List<AppContractPrx>();
        }

        public override void ReportEvent(string fullyQualifiedNameSpace, string key, string value, Ice.Current current__)
        {
            lock (this)
            {

                //System.Console.Out.WriteLine("adding client `" + _communicator.identityToString(ident) + "'");

                /*foreach (var observer in Observers)
                {
                    Task task = new Task(() => observer.Notify(fullyQualifiedNameSpace, key, value));
                    task.Start();

                }*/
            }
        }

        public override void SubscribeToEvents(string fullyQualifiedNameSpace, Ice.Identity ident, Ice.Current current__)
        {
            lock (this)
            {
                //System.Console.Out.WriteLine("adding client `" + _communicator.identityToString(ident) + "'");
                Task task = new Task(() =>
                {

                    try
                    {

                        Ice.ObjectPrx @base = current__.con.createProxy(ident);
                        AppContractPrx client = AppContractPrxHelper.uncheckedCast(@base);
                        Observers.Add(client);
                        Console.WriteLine("added a new app: " + client.GetName());
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error during registration of CareStoreApp callback " + e);
                    }
                });
                task.Start();
            }

        }

        public override void UnSubscribeToEvents(string fullyQualifiedNameSpace, Ice.Identity ident, Ice.Current current__)
        {
            throw new NotImplementedException();
        }

        public override void Show(string AppNameToShow, Ice.Current current__)
        {
            foreach (var app in RegisteredComponets)
            {
                if (app.GetName().Equals(AppNameToShow))
                {
                    Task task = new Task(() => app.Show());
                    task.Start();

                }
            }
        }

        public override void RegisterCareStoreApp(Ice.Identity ident, Ice.Current current__)
        {
            lock (this)
            {
                //System.Console.Out.WriteLine("adding client `" + _communicator.identityToString(ident) + "'");
                Task task = new Task(() =>
                {

                    try
                    {
                        Ice.ObjectPrx @base = current__.con.createProxy(ident);
                        AppContractPrx client = AppContractPrxHelper.uncheckedCast(@base);
                        RegisteredComponets.Add(client);
                        Console.WriteLine("added a new app: " + client.GetName());
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error during registration of CareStoreApp callback " + e);
                    }
                });
                task.Start();
            }
        }
    }
}
