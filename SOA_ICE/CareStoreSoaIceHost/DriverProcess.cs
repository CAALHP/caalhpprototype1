﻿using System.AddIn.Hosting;
using System.Diagnostics;
using CAALHP.Utils.Helpers;
using Plugins.HostViewAdapters;

namespace Plugins.Host
{
    public class DriverProcess
    {
        public IDeviceDriverView Driver { get; private set; }
        public int ProcessId { get; private set; }
        public PerformanceCounter PerfCounter { get; private set; }
        public DriverProcess(IDeviceDriverView driver)
        {
            Driver = driver;
            ProcessId = AddInController.GetAddInController(driver).AddInEnvironment.Process.ProcessId;
            PerfCounter = PerformanceCounterHelper.GetPerformanceCounterByProcessId(ProcessId);
        }
    }
}