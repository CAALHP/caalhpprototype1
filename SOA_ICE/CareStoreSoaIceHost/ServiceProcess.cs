﻿using System.AddIn.Hosting;
using System.Diagnostics;
using CAALHP.Utils.Helpers;
using Plugins.HostViewAdapters;

namespace Plugins.Host
{
    public class ServiceProcess
    {
        public IServiceView Service { get; private set; }
        public int ProcessId { get; private set; }
        public PerformanceCounter PerfCounter { get; private set; }
        public ServiceProcess(IServiceView service)
        {
            Service = service;
            ProcessId = AddInController.GetAddInController(service).AddInEnvironment.Process.ProcessId;
            PerfCounter = PerformanceCounterHelper.GetPerformanceCounterByProcessId(ProcessId);
        }

        
    }
}