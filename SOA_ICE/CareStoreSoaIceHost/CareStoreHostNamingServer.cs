using System;
using CareStoreSOAICE;

namespace CareStoreSoaIceHost
{
    class CareStoreHostNamingServer : Ice.Application
    {
        //private bool HandleLifeCycle;
            
        public override int run(string[] args)
        {

            // lock (this) {
            // Terminate cleanly on receipt of a signal
            //
            shutdownOnInterrupt();

            // Create an object adapter (stored in the _adapter
            // static members)
            //
                
            var com = communicator();

            Ice.ObjectAdapter adapter = com.createObjectAdapterWithEndpoints("CareStoreNameServiceAdpater", "default -p 10001");
            adapter.add(new CareStoreNameServiceI(), com.stringToIdentity("CareStoreNameService"));
            //adapter.addWithUUID(new CareStoreNameServiceI());

            // All objects are created, allow client requests now
            adapter.activate();


            // Wait until we are done
            communicator().waitForShutdown();


            if (interrupted())
                Console.Error.WriteLine(appName() + ": terminating");

            return 0;
            //}
        }



        static void Main(string[] args)
        {
            //var careStoreHostNamingServer = new CareStoreHostNamingServer();
            //Console.WriteLine("Starting CareStoreHostNamingServer ...");

            //Environment.Exit(careStoreHostNamingServer.main(args));

        }
    }
}