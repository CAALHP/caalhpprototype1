﻿using System;
using System.Collections.Generic;
using CAALHP.Library.Hosts;
using Ice;
using OpenCare;

namespace SOA.IceHost
{
    /// <summary>
    /// servant class
    /// </summary>
    class HostCAALHPContractI : IHostCAALHPContractDisp_
    {
        private readonly IHost _host;
        public HostCAALHPContractI(IHost host)
        {
            _host = host;
        }
        public override void ReportEvent(StringKeyValuePair value, Current current__)
        {
            _host.ReportEvent(new KeyValuePair<string, string>(value.key, value.value));
        }

        public override void SubscribeToEvents(int processId, Current current__)
        {
            _host.SubscribeToEvents(processId);
        }

        public override void UnSubscribeToEvents(int processId, Current current__)
        {
            _host.UnSubscribeToEvents(processId);
        }

    }
}
