﻿using System;
using System.Collections.Generic;
using System.Security;
using CAALHP.Library.Hosts;
using OpenCare;


namespace SOA.IceHost
{
    public class IceHost : IHost
    {
        private int _status = 0;
        private readonly Ice.Communicator _ic = null;
        public IceHost()
        {
            try
            {
                _ic = Ice.Util.initialize();
                var adapter = _ic.createObjectAdapterWithEndpoints("HostCAALHPContractAdapter", "tcp -p 10000");
                IHostCAALHPContract hostCAALHP = new HostCAALHPContractI(this);
                adapter.add(hostCAALHP, _ic.stringToIdentity("HostCAALHPContract"));
                adapter.activate();
                //_ic.waitForShutdown();
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e);
                _status = 1;
            }
            
        }

        ~IceHost()
        {
            if (_ic != null)
            {
                // Clean up
                //
                try
                {
                    _ic.destroy();
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine(e);
                    _status = 1;
                }
            }
            Environment.Exit(_status);
        }

        public List<string> GetListOfInstalledApps()
        {
            throw new NotImplementedException();
        }

        public List<string> GetListOfRunningApps()
        {
            throw new NotImplementedException();
        }

        public List<string> GetListOfActiveDevices()
        {
            throw new NotImplementedException();
        }

        public Dictionary<int, ProcessInfo> GetRunningAppsProcessInfo()
        {
            throw new NotImplementedException();
        }

        public Dictionary<int, ProcessInfo> GetRunningDriversProcessInfo()
        {
            throw new NotImplementedException();
        }

        public void StartApp(int app)
        {
            throw new NotImplementedException();
        }

        public void StartApp(int appIndex, PermissionSet permissions)
        {
            throw new NotImplementedException();
        }

        public void CloseApp(int app)
        {
            throw new NotImplementedException();
        }

        public void SwitchToApp(int app)
        {
            throw new NotImplementedException();
        }

        public void SendMeasurementToActiveApps(double measurement)
        {
            throw new NotImplementedException();
        }

        public double GetMeasurementFromDevice(int index)
        {
            throw new NotImplementedException();
        }

        public void UpdateDriverList()
        {
            throw new NotImplementedException();
        }

        public void SubscribeToEvents(int processId)
        {
            Console.WriteLine("SubscribeToEvent was called with process id: {0}", processId);
        }

        public void UnSubscribeToEvents(int processId)
        {
            Console.WriteLine("UnSubscribeToEvent was called with process id: {0}", processId);
        }

        public void ReportEvent(KeyValuePair<string, string> value)
        {
            Console.WriteLine("ReportEvent was called with key: {0} and value: {1}", value.Key, value.Value);
        }

        //Make this threaded!
        public void NotifyEventToPlugin(KeyValuePair<string, string> value, int processId)
        {
            Console.WriteLine("NotifyEventToPlugin was called from process : {0} with key: {1} and value: {2}", processId, value.Key, value.Value);
            /*foreach (var record in _activeAppsDictionary)
            {
                if (record.Value.ProcessId == processId)
                    record.Value.App.Notify(value);
            }*/
        }


        public void ReportEvent(string fullyQualifiedNameSpace, KeyValuePair<string, string> value)
        {
            throw new NotImplementedException();
        }

        System.Threading.Tasks.Task IHost.NotifyEventToPlugin(KeyValuePair<string, string> value, int processId)
        {
            throw new NotImplementedException();
        }

        public void ShowApp(string appName)
        {
            throw new NotImplementedException();
        }

        public void CloseApp(string appName)
        {
            throw new NotImplementedException();
        }



        public void UnSubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            throw new NotImplementedException();
        }

        public void SubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            throw new NotImplementedException();
        }




        IList<CAALHP.Contracts.IPluginInfo> IHost.GetListOfInstalledApps()
        {
            throw new NotImplementedException();
        }

        public IList<CAALHP.Contracts.IPluginInfo> GetListOfInstalledDeviceDrivers()
        {
            throw new NotImplementedException();
        }
    }
}

