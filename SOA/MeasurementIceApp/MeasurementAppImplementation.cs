﻿using System;
using System.Collections.Generic;
using CAALHP.Contracts;

namespace MeasurementIceApp
{
    public class MeasurementAppImplementation : IAppCAALHPContract
    {
        private readonly List<double> _measurementList = new List<double>();

        public void NewMeasurement(double measurement)
        {
            _measurementList.Add(measurement);
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            var splitList = notification.Value.Split(',');
            var measurement = double.Parse(splitList[splitList.Length - 1]);
            _measurementList.Add(measurement);
        }

        public void Show()
        {
            Console.Clear();
            Console.WriteLine("###  Glorious Measurement App  ###");
            Console.WriteLine();
            Console.WriteLine("Recorded Measurements:");
            Console.WriteLine("---------------------------------");
            var index = 0;
            foreach (var measurement in _measurementList)
            {
                Console.WriteLine("[" + index++ + "] " + measurement);
            }
            Console.WriteLine("---------------------------------");
        }

        public string GetName()
        {
            return "Measurement App Plugin";
        }

        public void Initialize(IHostCAALHPContract hostObj, int processId)
        {
            throw new NotImplementedException();
        }

        public void Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            throw new NotImplementedException();
        }
    }
}
