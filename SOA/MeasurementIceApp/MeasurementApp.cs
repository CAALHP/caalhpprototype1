﻿using System;
using OpenCare;

namespace MeasurementIceApp
{
    public class MeasurementApp
    {
        private static void Main(string[] args)
        {
            var status = 0;
            Ice.Communicator ic = null;
            try
            {
                ic = Ice.Util.initialize(ref args);
                var obj = ic.stringToProxy("HostCAALHPContract:tcp -p 10000");
                var host = IHostCAALHPContractPrxHelper.checkedCast(obj);
                if (host == null)
                    throw new ApplicationException("invalid proxy");

                //Console.WriteLine("Server returned: " + name);
                host.ReportEvent(new StringKeyValuePair("bla", "blablabla"));
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
                Environment.Exit(1);
            }
        

            if (ic != null)
            {
                //clean up
                try
                {
                    ic.destroy();
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine(e);
                    status = 1;
                }
            }
            Environment.Exit(status);
        }
    }
}
