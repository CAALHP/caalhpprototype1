﻿using System.Collections.Generic;
//using CAALHP.Contracts;
using SOA;
//using IHostCAALHPContract = CAALHP.Contracts.IHostCAALHPContract;


namespace AppIceAdapter
{
    public abstract class AppIceAdapter : IAppCAALHPContract
    {
        public IAppCAALHPContract App { get; set; }

        //public IAppPrx Prx { get; set; }

        public virtual void Notify(KeyValuePair<string,string> notification)
        {
            Prx.Notify(new StringKeyValuePair(notification.Key,notification.Value));
        }

        public void Show()
        {
            Prx.Show();
        }

        public string GetName()
        {
            return Prx.GetName();
        }

        public void Initialize(IHostCAALHPContract hostObj, int processId)
        {
            throw new System.NotImplementedException();
        }

        public void Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            throw new System.NotImplementedException();
        }

        void IAppCAALHPContract.Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            throw new System.NotImplementedException();
        }

        void IAppCAALHPContract.Show()
        {
            throw new System.NotImplementedException();
        }

        void IBaseCAALHPContract.Notify(KeyValuePair<string, string> notification)
        {
            throw new System.NotImplementedException();
        }

        string IBaseCAALHPContract.GetName()
        {
            throw new System.NotImplementedException();
        }

        public Ice.DispatchStatus collocDispatch__(IceInternal.Direct request)
        {
            throw new System.NotImplementedException();
        }

        public Ice.DispatchStatus dispatch__(IceInternal.Incoming inc, Ice.Current current)
        {
            throw new System.NotImplementedException();
        }

        public Ice.DispatchStatus ice_dispatch(Ice.Request request)
        {
            throw new System.NotImplementedException();
        }

        public Ice.DispatchStatus ice_dispatch(Ice.Request request, Ice.DispatchInterceptorAsyncCallback cb)
        {
            throw new System.NotImplementedException();
        }

        public string ice_id(Ice.Current current)
        {
            throw new System.NotImplementedException();
        }

        public string ice_id()
        {
            throw new System.NotImplementedException();
        }

        public string[] ice_ids(Ice.Current current)
        {
            throw new System.NotImplementedException();
        }

        public string[] ice_ids()
        {
            throw new System.NotImplementedException();
        }

        public bool ice_isA(string s, Ice.Current current)
        {
            throw new System.NotImplementedException();
        }

        public bool ice_isA(string s)
        {
            throw new System.NotImplementedException();
        }

        public void ice_ping(Ice.Current current)
        {
            throw new System.NotImplementedException();
        }

        public void ice_ping()
        {
            throw new System.NotImplementedException();
        }

        public void ice_postUnmarshal()
        {
            throw new System.NotImplementedException();
        }

        public void ice_preMarshal()
        {
            throw new System.NotImplementedException();
        }

        public void read__(Ice.InputStream inS__)
        {
            throw new System.NotImplementedException();
        }

        public void read__(IceInternal.BasicStream is__)
        {
            throw new System.NotImplementedException();
        }

        public void write__(Ice.OutputStream outS__)
        {
            throw new System.NotImplementedException();
        }

        public void write__(IceInternal.BasicStream os__)
        {
            throw new System.NotImplementedException();
        }

        public object Clone()
        {
            throw new System.NotImplementedException();
        }

        public void Hide(Ice.Current current__)
        {
            throw new System.NotImplementedException();
        }

        public void Initialize(string endpoint, int processId, Ice.Current current__)
        {
            throw new System.NotImplementedException();
        }

        public void Show(Ice.Current current__)
        {
            throw new System.NotImplementedException();
        }

        public void Hide()
        {
            throw new System.NotImplementedException();
        }

        public void Initialize(string endpoint, int processId)
        {
            throw new System.NotImplementedException();
        }
    }
}
