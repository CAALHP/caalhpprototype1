#pragma once

module SOA
{
	struct StringKeyValuePair
	{
		string key;
		string value;
	};

	interface IBaseCAALHPContract
	{
		bool isAlive();
		void Shutdown();

        void Notify(StringKeyValuePair notification);
        string GetName();
	};

	interface IAppCAALHPContract
	{
        //void Initialize(IAppHostCAALHPContract hostObj, int processId); 
		void Hide();
        void Initialize(string endpoint, int processId);
		void Show();
	};

	interface IDriverCAALHPContract
	{
        //void Initialize(IDeviceDriverHostCAALHPContract hostObj, int processId); 
        void Initialize(string endpoint, int processId); 
		double GetMeasurement();
	};

	interface IHostCAALHPContract
	{
        //void ReportEvent(StringKeyValuePair notification);
        void ReportEvent(string fullyQualifiedNameSpace, StringKeyValuePair notification);
        //void SubscribeToEvents(int processId);
        void SubscribeToEvents(string fullyQualifiedNameSpace, int processId);
        //void UnSubscribeToEvents(int processId);
        void UnSubscribeToEvents(string fullyQualifiedNameSpace, int processId);
	};
	
};