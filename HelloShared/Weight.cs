﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Net4Care.Observation;
using System.Runtime.Serialization;

namespace Net4Care.HelloShared
{
    [DataContract(Namespace = "csharp.contrib.net4care.org")]
    public class Weight : IObservationSpecifics
	{
        [DataMember(Name = "quantity")]
		public ClinicalQuantity Quantity { get; private set; }
		
		/// <summary>
		/// Default constructor for the benefit of the serializer
		/// </summary>
		public Weight() { }

		public Weight(double weightMeasurement, Net4CareContext context)
		{
			Quantity = new ClinicalQuantity(weightMeasurement, "kg", "8341-0", "Dry body weight Measured", context.asContextCodeList());
		}

        [DataMember(Name = "observationAsHumanReadableText")]
		public string ObservationAsHumanReadableText
		{
            get
            {
    			return Quantity.ToString();
            }
            set
            {
                // NOOP
            }
        }

        public IList<ClinicalQuantity> Quantities
		{
			get { return new List<ClinicalQuantity>(new[] { Quantity }); }
        }

		public bool Equals(IObservationSpecifics other)
		{
			var compareTo = other as Weight;
			if (compareTo == null) return false;
			return Quantity.Equals(compareTo.Quantity);
		}
	}
}
