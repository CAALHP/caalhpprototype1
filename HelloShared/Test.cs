﻿#if DEBUG
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Net4Care.Serializer;
using Net4Care.Serializer.JSON;
using Net4Care.Observation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Net4Care.HelloShared
{
    [TestClass]
    public class Test
    {
        [TestMethod]
        public void TestSerializeSpirometry()
        {
            var spirometry = new Spirometry(3.5, 2.8, false);
            TestSerializeObservation(spirometry);
        }

        [TestMethod]
        public void TestSerializeWeight()
        {
            var context = new Net4CareContext(Net4CareContext.AuthorType.SELF, Net4CareContext.ProvisionType.ELECTRONIC);
            var weight = new Weight(87.0, context);
            TestSerializeObservation(weight);
        }

        private void TestSerializeObservation(IObservationSpecifics observation)
        {
            var device = new DeviceDescription("Spirometry", "MODEL1", "Manufac1", "1", "1", "1.0", "1.0", "Classification", DateTime.Now.Ticks);
            var stoi = new StandardTeleObservation("251248-4916", "myOrgID", "myTreatmentId", Codes.LOINC_OID, device, observation, "I have difficulties in breathing.");

            ISerializer serializer = new JSONSerializer(observation.GetType());
            var json = serializer.Serialize(stoi);

            Assert.IsFalse(String.IsNullOrEmpty(json));

            var stoo = serializer.Deserialize(json);

            Assert.AreEqual(stoi, stoo);
        }
    }
}
#endif