﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Net4Care.Observation;
using Newtonsoft.Json;

/** This is an example of how a SMB will adapt
 * Net4Care for a specific set of measurements 
 * used for their given program/device.
 *
 * You create a class implementing the ObservationSpecifics
 * interface.
 * 
 * This demo class defines two spirometry (lung capacity)
 * values as well as the answer to a questionaire question.
 * 
 *  @author Henrik Baerbak Christensen and Michael Christensen, Aarhus University
 *  Henrik wrote the original Java code, Michael ported it to C#
 * 
 */

namespace Net4Care.HelloShared
{
	public class Spirometry : IObservationSpecifics
	{
        [JsonProperty(PropertyName = "fvc")]
        public ClinicalQuantity FVC { get; set; }
       
        [JsonProperty(PropertyName = "fev1")]
        public ClinicalQuantity FEV1 { get; set; }
        
        public bool QuestionAAnswer { get; set; }

        public IList<ClinicalQuantity> Quantities
		{
			get { return new List<ClinicalQuantity>(new[] { FVC, FEV1 }); }
    	}

		public Spirometry() { }

		/** Create a spirometry observation
		 * 
		 * @param fvc FVC value
		 * @param fev1 FEV1 value
		 * @param questionAAnswer the bool answer to the questionaire's
		 * question A) "Are you feeling well?"
		 */
		public Spirometry(double fvc, double fev1, bool questionAAnswer)
		{
			FVC = new ClinicalQuantity(fvc, "L", "19868-9", "FVC"); // LOINC FVC
			FEV1 = new ClinicalQuantity(fev1, "L", "20150-9", "FEV1");
			QuestionAAnswer = questionAAnswer;
		}

		public override string ToString()
		{
			return "Spiro: " + FEV1 + "/" + FVC + "(" + QuestionAAnswer + ")";
		}

		public string ObservationAsHumanReadableText
		{
            get
            {
                return "Measured: " + FVC.ToString() + " / " + FEV1.ToString() + "(" + QuestionAAnswer + ")";
            }
            set
            {
                // NOOP
            }
		}

		public bool Equals(IObservationSpecifics other)
		{
			var compareTo = other as Spirometry;
			if (compareTo == null) return false;
			return FVC.Equals(compareTo.FVC) && FEV1.Equals(compareTo.FEV1) && QuestionAAnswer == compareTo.QuestionAAnswer;
		}
	}
}
