﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Net4Care.Forwarder;
using Net4Care.Serializer;
using Net4Care.Serializer.JSON;
using Net4Care.Forwarder.StandardUploader;
using Net4Care.Forwarder.StandardConnector;

/** Some shared functions for the demo.
 *
 *  @author Henrik Baerbak Christensen and Michael Christensen, Aarhus University
 *  Henrik wrote the original Java code, Michael ported it to C#
 */

namespace Net4Care.HelloShared
{
    public class Shared
	{
        /** Setup a configuration of the Net4Care architecture */
        public static IDataUploader setupN4CConfiguration(String serverAddress, params Type[] knownObservationTypes) {
            IDataUploader dataUploader = null;
            ISerializer serializer = new JSONSerializer(knownObservationTypes);
            dataUploader = new StandardDataUploader(serializer, new HttpConnector(serverAddress));
            return dataUploader;
        }
    }
}
