﻿using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace CareStoreServiceContracts
{
    [ServiceContract]
    public interface ICareStoreMarketplaceContract
    {
        [OperationContract]
        Uri GetDriverLink(string profile);

        [OperationContract]
        IList<string> GetAppLinks();

        /*
        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);
         */
    }
}