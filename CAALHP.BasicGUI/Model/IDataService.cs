﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using N4CLibrary;

namespace CAALHP.BasicGUI.Model
{
    public interface IDataService
    {
        IList<HomeBloodPressureObservation> GetHomeBloodPressures();
        void UploadHomeBloodPressure(HomeBloodPressureObservation homeBloodPressureObservation);
    }
}
