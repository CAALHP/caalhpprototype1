﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CAALHP.BasicGUI.Model;
using N4CLibrary;

namespace CAALHP.BasicGUI.Design
{
    public class DesignDataService : IDataService
    {
        public IList<HomeBloodPressureObservation> GetHomeBloodPressures()
        {
            var result = new List<HomeBloodPressureObservation>();
            for (var i = 0; i < 10; i++)
            {
                result.Add(
                    new HomeBloodPressureObservation
                        (
                        sys: 100+i*10, 
                        dia: 60+i*5, 
                        pulse: 40+i*10, 
                        noise: i%2==0, 
                        timeSeated: i*10
                        ));
            }
            return result;
        }

        public void UploadHomeBloodPressure(HomeBloodPressureObservation homeBloodPressureObservation)
        {
            throw new NotImplementedException();
        }
    }
}