﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using CAALHP.BasicGUI.ViewModel;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.ServiceLocation;

namespace CAALHP.BasicGUI.Views
{
    /// <summary>
    /// Description for HomeScreenView.
    /// </summary>
    public partial class HomeScreenView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the HomeScreenView class.
        /// </summary>
        public HomeScreenView()
        {
            InitializeComponent();

            MarketTile.IconSource = "Images/app1.png";
            HealthDataTile.IconSource = "Images/app2.png";
            ManualDataInputTile.IconSource = "Images/app3.png";
        }

        private void MarketTile_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            //throw new System.NotImplementedException();
        }

        private void HealthDataTile_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            ServiceLocator.Current.GetInstance<MainViewModel>().HealthDataViewCommand.Execute(this);
        }

        private void ManualDataInputTile_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            ServiceLocator.Current.GetInstance<MainViewModel>().ManualDataInputViewCommand.Execute(this);
        }
    }
}