﻿using System.Windows;
using System.Windows.Controls;

namespace CAALHP.BasicGUI.Views
{
	/// <summary>
	/// Description for HealthDataView.
	/// </summary>
	public partial class HealthDataView : UserControl
	{
		/// <summary>
		/// Initializes a new instance of the HealthDataView class.
		/// </summary>
		public HealthDataView()
		{
			InitializeComponent();
		}

		private void HealthDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			HealthDataGrid.UnselectAllCells();
		}

	}
}