﻿using System.Net.Mime;
using System.Windows;
using System.Windows.Controls;
using CAALHP.BasicGUI.ViewModel;
using Microsoft.Practices.ServiceLocation;

namespace CAALHP.BasicGUI.Views
{
    /// <summary>
    /// Description for ManualDataInputView.
    /// </summary>
    public partial class ManualDataInputView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the ManualDataInputView class.
        /// </summary>
        public ManualDataInputView()
        {
            InitializeComponent();
        }
    }
}