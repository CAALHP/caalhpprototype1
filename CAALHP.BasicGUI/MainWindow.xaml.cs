﻿using System.Windows;
using System.Windows.Input;
using CAALHP.BasicGUI.ViewModel;
using Microsoft.Practices.ServiceLocation;
[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace CAALHP.BasicGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        
        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            Closing += (s, e) => ViewModelLocator.Cleanup();
            TopMenu.HomeButton.MouseDown += HomeButton_MouseDown;
        }

        void HomeButton_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ServiceLocator.Current.GetInstance<MainViewModel>().HomeScreenViewCommand.Execute(this);
        }

    }
}