﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using Microsoft.Practices.ServiceLocation;

namespace CAALHP.BasicGUI.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class HomeScreenViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the HomeScreenViewModel class.
        /// </summary>
        public HomeScreenViewModel()
        {

        }
    }
}