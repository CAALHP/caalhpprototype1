﻿using GalaSoft.MvvmLight;
using N4CLibrary;

namespace CAALHP.BasicGUI.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class HomeBloodPressureViewModel : ViewModelBase
    {
        private readonly HomeBloodPressureObservation _homeBloodPressureObservation;

        public string Systolic
        {
            get
            {
                if (_homeBloodPressureObservation.Systolic != null)
                    return _homeBloodPressureObservation.Systolic.Value + " " + _homeBloodPressureObservation.Systolic.Unit;
                return "";
            }
        }

        public string Diastolic
        {
            get
            {
                if (_homeBloodPressureObservation.Diastolic != null)
                    return _homeBloodPressureObservation.Diastolic.Value + " " + _homeBloodPressureObservation.Diastolic.Unit;
                return "";
            }
        }

        public string Pulse
        {
            get
            {
                if (_homeBloodPressureObservation.Pulse != null) 
                    return _homeBloodPressureObservation.Pulse.Value + " /min";
                return "";
            }
        }

        public string NoiseDetected
        {
            get
            {
                return _homeBloodPressureObservation.NoiseDetectedDuringMeasurement ? "Yes" : "No";
            }
        }

        public string TimeSeated
        {
            get {
                if (_homeBloodPressureObservation.TimeSeated != null)
                    return _homeBloodPressureObservation.TimeSeated.Value + " " + _homeBloodPressureObservation.TimeSeated.Unit;
                return "";
            }
        }


        /// <summary>
        /// Initializes a new instance of the HomeBloodPressureViewModel class.
        /// </summary>
        public HomeBloodPressureViewModel(HomeBloodPressureObservation hbp)
        {
            _homeBloodPressureObservation = hbp;
        }
    }
}