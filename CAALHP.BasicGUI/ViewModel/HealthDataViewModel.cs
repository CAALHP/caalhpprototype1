﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using CAALHP.BasicGUI.Model;
using CAALPHUILib;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using N4CLibrary;

namespace CAALHP.BasicGUI.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public sealed class HealthDataViewModel : ViewModelBase
    {
        private readonly IDataService _dataService;
        private string _loadingStr;

        public ObservableCollection<HomeBloodPressureViewModel> HomeBloodPressures
        {
            get;
            private set; 
        }

        public string LoadingStr
        {
            get { return _loadingStr; }
            private set { _loadingStr = value; }
        }

        /// <summary>
        /// Initializes a new instance of the HealthDataViewModel1 class.
        /// </summary>
        public HealthDataViewModel(IDataService dataService)
        {
            _dataService = dataService;
            HomeBloodPressures = new ObservableCollection<HomeBloodPressureViewModel>();
            //RaisePropertyChanged("HomeBloodPressures");
            LoadDataCommand = new RelayCommand(LoadDataCommand_Execute);
            LoadingStr = "Loading data...";
            RaisePropertyChanged("LoadingStr");
        }

        

        #region Command
        public ICommand LoadDataCommand { get; set; }

        private void LoadDataCommand_Execute()
        {
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                try
                {
                    foreach (var homeBloodPressure in _dataService.GetHomeBloodPressures())
                    {
                        HomeBloodPressures.Add(new HomeBloodPressureViewModel(homeBloodPressure));
                    }
                    RaisePropertyChanged("HomeBloodPressures");
                    LoadingStr = "Data Loaded...";
                    RaisePropertyChanged("LoadingStr");
                }
                catch (Exception e)
                {
                    //Console.WriteLine(e);
                    //var alertScreen = new CSAlertScreen { AlertTextMessage = e.Message };
                    //MessageBox.Show(e.Message);
                    LoadingStr = "";
                    RaisePropertyChanged("LoadingStr");
                }    
            }));
        }
        #endregion
    }
}