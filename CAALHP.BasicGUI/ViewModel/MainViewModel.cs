﻿using System.Windows.Input;
using GalaSoft.MvvmLight;
using CAALHP.BasicGUI.Model;
using GalaSoft.MvvmLight.Command;

namespace CAALHP.BasicGUI.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private readonly IDataService _dataService;

        private readonly CAALHP.Library.CAALHP _caalhp;
        
        /// <summary>
        /// The current view.
        /// </summary>
        private ViewModelBase _currentViewModel;

        private static readonly HomeScreenViewModel _homeScreenViewModel = new HomeScreenViewModel();

        /// <summary>
        /// The CurrentView property.  The setter is private since only this 
        /// class can change the view via a command.  If the View is changed,
        /// we need to raise a property changed event (via INPC).
        /// </summary>
        public ViewModelBase CurrentViewModel
        {
            get
            {
                return _currentViewModel;
            }
            set
            {
                if (_currentViewModel == value)
                    return;
                _currentViewModel = value;
                RaisePropertyChanged("CurrentViewModel");
            }
        }

        /// <summary>
        /// Simple property to hold the 'HomeScreenViewCommand' - when executed
        /// it will change the current view to the 'HomeScreenView'
        /// </summary>
        public RelayCommand HomeScreenViewCommand { get; private set; }

        /// <summary>
        /// Simple property to hold the 'HealthDataViewCommand' - when executed
        /// it will change the current view to the 'HealthDataView'
        /// </summary>
        public ICommand HealthDataViewCommand { get; private set; }

        /// <summary>
        /// Simple property to hold the 'ManualDataInputViewCommand' - when executed
        /// it will change the current view to the 'ManualDataInputView'
        /// </summary>
        public ICommand ManualDataInputViewCommand { get; private set; }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(IDataService dataService)
        {
            _dataService = dataService;
            
            CurrentViewModel = _homeScreenViewModel;
            HomeScreenViewCommand = new RelayCommand(ExecuteHomeScreenDataViewCommand);
            HealthDataViewCommand = new RelayCommand(ExecuteHealthDataViewCommand);
            ManualDataInputViewCommand = new RelayCommand(ExecuteManualDataInputViewCommand);

            //System.Console.WriteLine("Initializing CAALHP");
            /*_caalhp = new CAALHP.Library.CAALHP();
            _caalhp.Host = new PluginHost();

            var addDeviceManager = new DeviceManager();
            addDeviceManager.InstallStrategy = new InstallDriverPluginStrategy(_caalhp.Host);

            //binding
            var binding = new BasicHttpBinding();
            //endpoint
            var endpoint = new EndpointAddress("http://carestoremarketplace.cloudapp.net/CareStoreMarketplaceService.svc");
            //channelfactory
            var channelFactory = new ChannelFactory<ICareStoreMarketplaceContract>(binding, endpoint);

            addDeviceManager.Market = channelFactory.CreateChannel();

            _caalhp.DeviceManager = addDeviceManager;

            var baseAddress = new Uri("http://localhost:8880/caalhp");
            //var wcfservice = new WCFService.WCFService(baseAddress) {DeviceManager = addDeviceManager};
            var restservice = new RESTService.RESTService(baseAddress, addDeviceManager);
            //caalhp.Service = wcfservice;
            _caalhp.Service = restservice;*/
            /*
            System.Console.WriteLine("CAALHP started");
            caalhp.Run();*/
            //_caalhp.Service.Start();
        }


        ~MainViewModel()
        {
            //_caalhp.Service.Stop();
        }


        private void ExecuteHomeScreenDataViewCommand()
        {
            CurrentViewModel = _homeScreenViewModel;
        }


        private void ExecuteHealthDataViewCommand()
        {
            CurrentViewModel = new HealthDataViewModel(_dataService);
        }

        private void ExecuteManualDataInputViewCommand()
        {
            CurrentViewModel = new ManualDataInputViewModel(_dataService);
        }
    }
}