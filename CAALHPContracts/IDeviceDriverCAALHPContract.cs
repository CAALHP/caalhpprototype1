﻿namespace CAALHP.Contracts
{
    public interface IDeviceDriverCAALHPContract : IBaseCAALHPContract
    {
        void Initialize(IDeviceDriverHostCAALHPContract hostObj, int processId); 
        //double GetMeasurement();
    }
}
