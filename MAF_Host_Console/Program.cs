﻿using System;

namespace MAF_Host_Console
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting MAF Host");
            var hostConsole = new MafHostConsole();
            Console.WriteLine("System running...");
            Console.WriteLine("Press 'q' to exit");
            while (!Console.ReadKey().KeyChar.Equals('q'))
            {

            }
        }
    }
}
