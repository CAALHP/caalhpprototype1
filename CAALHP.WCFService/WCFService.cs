﻿using System.IO;
using System.ServiceModel;
using System.ServiceModel.Description;
using CAALHP.Library.Managers;
using CAALHP.Library.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CareStoreServiceContracts;

namespace CAALHP.WCFService
{
    public class WCFService : ServiceHost, ICAALHPServiceContract, IService
    {
        private readonly Uri _baseAddress;
        private ServiceHost _host;

        public DeviceManager DeviceManager;
        
        public WCFService(Uri address)
        {
            _baseAddress = address;
        }

        public void Start()
        {
            // Create the ServiceHost.
            _host = new ServiceHost(this, _baseAddress);
            // Enable metadata publishing.
            var smb = new ServiceMetadataBehavior
            {
                HttpGetEnabled = true,
                MetadataExporter = { PolicyVersion = PolicyVersion.Policy15 }
            };
            _host.Description.Behaviors.Add(smb);
            var behaviour = _host.Description.Behaviors.Find<ServiceBehaviorAttribute>();
            behaviour.InstanceContextMode = InstanceContextMode.Single;

            // Open the ServiceHost to start listening for messages. Since
            // no endpoints are explicitly configured, the runtime will create
            // one endpoint per base address for each service contract implemented
            // by the service.
            _host.Open();

            Console.WriteLine("The service is ready at {0}", _baseAddress);
        }
        public void Stop()
        {
            // Close the ServiceHost.
            _host.Close();
        }

        public void AddDevice(DeviceProfile profile)
        {
            DeviceManager.AddDevice(profile.ModelName);
        }
    }
}
