﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;
using CAALHP.Library.Managers;
using CAALHP.Library.Services;

namespace CAALHP.WCFService
{
    public class WCFServer : IService
    {
        private readonly Uri _baseAddress;
        private readonly WCFService _service;
        private ServiceHost _host;

        public WCFServer(Uri address, WCFService service)
        {
            _baseAddress = address;
            _service = service;
        }

        public void Start()
        {
            // Create the ServiceHost.
            _host = new ServiceHost(_service, _baseAddress);
            // Enable metadata publishing.
            var smb = new ServiceMetadataBehavior
            {
                HttpGetEnabled = true,
                MetadataExporter = {PolicyVersion = PolicyVersion.Policy15}
            };
            _host.Description.Behaviors.Add(smb);
            var behaviour = _host.Description.Behaviors.Find<ServiceBehaviorAttribute>();
            behaviour.InstanceContextMode = InstanceContextMode.Single;

            // Open the ServiceHost to start listening for messages. Since
            // no endpoints are explicitly configured, the runtime will create
            // one endpoint per base address for each service contract implemented
            // by the service.
            _host.Open();

            Console.WriteLine("The service is ready at {0}", _baseAddress);
        }
        public void Stop()
        {
            // Close the ServiceHost.
            _host.Close();
        }
    }
}
