﻿using System.Collections.Generic;
using CAALHP.Library.Config;

namespace CAALHP.Library.Brokers
{
    public interface ICAALHPBroker
    {
        void ShowApp(string appName);
        void CloseApp(string appName);
        IList<PluginConfig> GetListOfInstalledApps();
        IList<PluginConfig> GetListOfInstalledDeviceDrivers();
        void ActivateDeviceDrivers();
        IList<PluginConfig> GetListOfRunningApps();
    }
}