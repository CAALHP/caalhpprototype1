﻿using System;
using CAALHP.Events;

namespace CAALHP.Library.Observer
{
    public class UserLoggedInEventArgs : EventArgs
    {
        public UserLoggedInEvent UserEvent { get; private set; }

        public UserLoggedInEventArgs(UserLoggedInEvent userEvent)
        {
            UserEvent = userEvent;
        }
    }
}