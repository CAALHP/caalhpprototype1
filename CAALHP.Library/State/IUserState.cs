﻿using System.Collections.Generic;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Events.Types;
using CAALHP.Library.Config;

namespace CAALHP.Library.State
{
    public interface IUserState
    {
        void UserLoggedIn(UserProfile user);
        void UserLoggedOut();
        IList<PluginConfig> GetListOfInstalledApps();
    }
}