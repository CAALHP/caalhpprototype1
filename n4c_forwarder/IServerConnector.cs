﻿using System;

/** An instance of ServerConnector is responsible for the 
 * actual upload of the on-the-wire formatted string 
 * of the observation to the Net4Care server side. 
 *  
 * You should not make instances of this interface but 
 * instead use the implementations present in the 
 * forwarder.delegate sub package! 
 *  
*  @author Henrik Baerbak Christensen and Michael Christensen, Aarhus University
 *  Henrik wrote the original Java code, Michael ported it to C#
 * 
 */ 

namespace Net4Care.Forwarder {
    public interface IServerConnector {

        /** Send the on-the-wire string to the server side.
         * 
         * @param onTheWireFormat the observation in the adopted
         * on-the-wire format.
         * @return a future that will eventually tell the status
         * of the transmission.
         */
		IFutureResult SendToServer(String onTheWireFormat);

		IFutureResult SendToServer(String onTheWireFormat, string userName, string password);

        /** Send a query for a set of observations to the server.
         * 
         * @param query the query for observations. Use one of
         * those defined in the forwarder.query sub package as
         * these are the ONLY ones the server understands.
         * @param reponseType define the type of the
         * returned observations, either as a list of
         * StandardTeleObserations or as PHMR documents.
         * @return a future with the query result as one big
         * string that needs deserializing before it can
         * by understood.
         * @throws IOException
         * @throws Net4CareException
         * @throws UnknownCPRException 
         */
        IFutureResult QueryToServer(IQuery query);

		IFutureResult QueryToServer(IQuery query, string userName, string password);
    }
}

