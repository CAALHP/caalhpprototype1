﻿using System;
using System.Threading.Tasks;

namespace Net4Care.Forwarder
{
	public class FutureResult : IFutureResult
	{
        //private static readonly ILog logger = LogManager.GetLogger(typeof(FutureResult));
        
        private Task<string> task;

		public FutureResult(Func<string> task)
		{
            this.task = new Task<string>(task, TaskCreationOptions.AttachedToParent);
			this.task.Start();
		}

		public bool IsSuccess { get { return task.Status == TaskStatus.RanToCompletion; } }

		public Exception Exception { get { return task.Exception; } }

		public string Result { get { return task.Result; } }

        public void Wait()
        {
            try
            {
                task.Wait();
            }
            catch (AggregateException e)
            {
                //logger.Debug("Ignoring exception in Wait", e);
            }
        }

		public bool IsDone { get { return task.IsCompleted; } }
	}
}
