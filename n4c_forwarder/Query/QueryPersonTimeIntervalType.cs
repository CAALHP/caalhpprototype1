﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Net4Care.Utility;

/** A query for a given person in a given time and a given set of types of observation 
 *  
 *  @author Henrik Baerbak Christensen and Michael Christensen, Aarhus University
 *  Henrik wrote the original Java code, Michael ported it to C#
 */ 

namespace Net4Care.Forwarder.Query {
	class QueryPersonTimeIntervalType : QueryPersonTimeInterval, IQuery
	{
        private string codeSystem;
        private string codeListInBarFormat;

        /** Construct a query for a given person in a given time interval and 
         * only those observations that match a given set of types of observations. 
         * @param cpr CPR of person 
         * @param beginTimeInterval start of interval in Unix time milliseconds 
         * @param endTimeInterval end of interval 
         * @param codeSystem HL7 OID of the code system used in the list of 
         * codes below. 
         * @param codeListInBarSeparatedFormat the list of codes as one 
         * string separated by vertical bar "|". If you have an array 
         *  of codes, use the Utility converter functions to produce this 
         *  string. 
         */
        public QueryPersonTimeIntervalType(String cpr, long beginTimeInterval, long endTimeInterval,
            string codeSystem, string codeListInBarSeparatedFormat) :  
            base(cpr, beginTimeInterval, endTimeInterval) {
            this.codeSystem = codeSystem;
            this.codeListInBarFormat = codeListInBarSeparatedFormat;
        }

        public override Dictionary<String, String> getDescriptionMap() {
            Dictionary<String, String> descriptionMap = base.getDescriptionMap();
            descriptionMap.Add(QueryKeys.QUERY_TYPE, QueryKeys.PERSON_TIME_OBSERVATION_TYPE_QUERY);
            descriptionMap.Add(QueryKeys.CODE_SYSTEM, this.codeSystem);
            descriptionMap.Add(QueryKeys.CODE_LIST_BAR_FORMAT, this.codeListInBarFormat);
            return descriptionMap;
        }

    }
}
