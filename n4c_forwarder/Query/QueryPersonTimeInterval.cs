﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Net4Care.Utility;

/** A query for observations on a given person in 
 * a given time interval. 
 *  
 *  @author Henrik Baerbak Christensen and Michael Christensen, Aarhus University
 *  Henrik wrote the original Java code, Michael ported it to C#
 * 
 */

namespace Net4Care.Forwarder.Query
{
	public class QueryPersonTimeInterval : IQuery
	{
		/** Define a query. Time is defined in terms 
		 * of Unix time (long integer milliseconds). 
		 *
		 * @param cpr person to query documents for 
		 * @param beginTimeInterval beginning of the time interval 
		 * @param endTimeInterval end of the time interval 
		 */
		public QueryPersonTimeInterval(String cpr, long beginTimeInterval, long endTimeInterval)
		{
			this.cpr = cpr;
			if (beginTimeInterval > endTimeInterval)
			{
				throw new SystemException("Your begin time is later than your end time.");
			}
			this.beginTimeInterval = beginTimeInterval;
			this.endTimeInterval = endTimeInterval;
			responseType = QueryResponseType.STANDARD_TELE_OBSERVATION;
		}

		private string cpr;
		private long beginTimeInterval;
		private long endTimeInterval;
		private QueryResponseType responseType;

		string getCpr()
		{
			return cpr;
		}
		long getBeginTimeInterval()
		{
			return beginTimeInterval;
		}
		long getEndTimeInterval()
		{
			return endTimeInterval;
		}

		public override string ToString()
		{
			return "Forwarder.QueryPersonTimeInterval for " + getCpr() + " in time (" + getBeginTimeInterval() + "-" + getEndTimeInterval() + ")";
		}

		public virtual Dictionary<String, String> getDescriptionMap()
		{
			Dictionary<String, String> themap = new Dictionary<String, String>();
			if (getFormatOfReturnedObservations() == QueryResponseType.PERSONAL_HEALTH_MONITORING_RECORD)    
				themap.Add(QueryKeys.FORMAT_KEY, QueryKeys.ACCEPT_XML_DATA);
            else
                themap.Add(QueryKeys.FORMAT_KEY, QueryKeys.ACCEPT_JSON_DATA);
            themap.Add(QueryKeys.QUERY_TYPE, QueryKeys.PERSON_TIME_QUERY);
			themap.Add(QueryKeys.CPR_KEY, getCpr());
			themap.Add(QueryKeys.BEGIN_TIME_INTERVAL, "" + getBeginTimeInterval());
			themap.Add(QueryKeys.END_TIME_INTERVAL, "" + getEndTimeInterval());
			return themap;
		}

		public void setFormatOfReturnedObservations(QueryResponseType responseType)
		{
			this.responseType = responseType;
		}

		public QueryResponseType getFormatOfReturnedObservations()
		{
			return responseType;
		}
	}
}
