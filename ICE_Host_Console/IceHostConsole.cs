//using System;
//using System.Collections.Generic;
//using CAALHP.Library;
//using CAALHP.Library.Hosts;
//using CAALHP.Library.Managers;
//using CAALHP.SOA.ICE.Host;

//namespace ICE_Host_Console
//{
//    public class IceHostConsole : IDisposable
//    {
//        private ICAALHP _caalhp;
//        private HostManager _hostManager;

//        public IceHostConsole()
//        {
//            var eventManager = new EventManager();
//            //start the servicehost first so we have loggingservice ready.
//            var serviceHost = new ServiceHost(eventManager, this);
//            var driverHost = new DeviceDriverHost(eventManager);
//            var appHost = new AppHost(eventManager);

//            _hostManager = new HostManager(appHost, driverHost, serviceHost);
//            _caalhp = new CAALHP.Library.CAALHP(eventManager, new List<IHostManager>(){_hostManager});
            
            
//            /*var apps = appHost.GetListOfInstalledApps();
//            var drivers = driverHost.GetListOfActiveDevices();

//            foreach (var app in apps)
//            {
//                Console.WriteLine("Found app: {0}", app.Name);
//            }

//            foreach (var driver in drivers)
//            {
//                Console.WriteLine("Found driver: {0}", driver);
//            }*/

//            //StartSystemApps(appHost);
//        }

//        private static void StartSystemApps(IAppHost appHost)
//        {
//            var apps = appHost.GetListOfInstalledApps();
//            for (var i = 0; i < apps.Count; i++)
//            {
//                if (apps[i].Name.StartsWith("System"))
//                {
//                    appHost.StartApp(i);
//                }
//            }
//        }

//        public void Dispose()
//        {
//            _hostManager.AppHost.ShutDown();
//            _hostManager.DeviceDriverHost.ShutDown();
//            _hostManager.ServiceHost.ShutDown();
//        }
//    }
//}