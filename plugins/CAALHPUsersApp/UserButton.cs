﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CAALHPUser
{
    public class UserButton : Button
    {
        private int _userIndex;
        public int UserIndex { get { return _userIndex; } }
        public UserButton(int userIndex) 
        {
            _userIndex = userIndex;
        }
    }
}
