﻿using System.AddIn;
using Plugins.ServicePluginAdapter;

namespace Plugins.Net4CareService
{
    // The AddInAttribute identifies this pipeline segment as an add-in.
    [AddIn("Net4Care Service", Version = "1.0.0.0")]
    public class Net4CareService : ServiceViewPluginAdapter
    {
        public Net4CareService()
        {
            Service = new Net4CareServiceImplementation();
        }
    }
}
