﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using N4CLibrary;
using Net4Care.HelloShared;
using Net4Care.Observation;

namespace Plugins.Net4CareService.Managers
{
    public class WeightManager : INet4CareManager
    {
        //private readonly string _cpr;
        private readonly N4CHelper _n4CHelper;

        public WeightManager()
        {
            var serverAddress = ConfigurationManager.AppSettings.Get("serveraddress");
           //_cpr = ConfigurationManager.AppSettings.Get("cpr");
            _n4CHelper = new N4CHelper(serverAddress, typeof(Weight));
        }

        public async void AddMeasurement(KeyValuePair<string, string> info)
        {
            await UploadMeasurement(info);
        }

        public async void AddMeasurement(double value)
        {
            var id = GetCpr();
            //todo 
            //var measurement = new Weight(value, new Net4CareContext(Net4CareContext.AuthorType.SELF, Net4CareContext.ProvisionType.ELECTRONIC));
            var measurement = new WeightObservation(value, new Net4CareContext(Net4CareContext.AuthorType.SELF, Net4CareContext.ProvisionType.ELECTRONIC));
            await UploadMeasurement(measurement, id);
        }

        public async void AddMeasurement(double value, string cpr)
        {
            var id = string.IsNullOrEmpty(cpr) ? GetCpr() : cpr;

            //todo 
            //var measurement = new Weight(value, new Net4CareContext(Net4CareContext.AuthorType.SELF, Net4CareContext.ProvisionType.ELECTRONIC));
            var measurement = new WeightObservation(value, new Net4CareContext(Net4CareContext.AuthorType.SELF, Net4CareContext.ProvisionType.ELECTRONIC));
            await UploadMeasurement(measurement, id);
        }

        private string GetCpr()
        {
            return Utils.Cpr;
        }

        //private async Task UploadMeasurement(IObservationSpecifics measurement)
        //{
        //    await Task.Run(() => _n4CHelper.UploadWeightObservation(GetCpr(), measurement));
        //}

        private async Task UploadMeasurement(IObservationSpecifics measurement, string cpr)
        {
            await Task.Run(() => _n4CHelper.UploadWeightObservation(cpr, measurement));
        }

        private async Task UploadMeasurement(KeyValuePair<string, string> info)
        {
            await Task.Run(() =>
            {
                var measurement = new Weight(double.Parse(info.Value.Split(';').Last()), new Net4CareContext(Net4CareContext.AuthorType.SELF,Net4CareContext.ProvisionType.ELECTRONIC));
                _n4CHelper.UploadWeightObservation(GetCpr(), measurement);
            });
        }
    }
}
