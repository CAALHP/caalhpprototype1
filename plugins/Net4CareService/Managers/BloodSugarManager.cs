﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using N4CLibrary;
using Net4Care.Observation;

namespace Plugins.Net4CareService.Managers
{
    public class BloodSugarManager : INet4CareManager
    {
        //private readonly string _cpr;
        private readonly N4CHelper _n4CHelper;

        public BloodSugarManager()
        {
            var serverAddress = ConfigurationManager.AppSettings.Get("serveraddress");
            //_cpr = ConfigurationManager.AppSettings.Get("cpr");
            _n4CHelper = new N4CHelper(serverAddress, typeof(GlucoseObservation));
        }

        public async void AddMeasurement(KeyValuePair<string, string> info)
        {
            await UploadMeasurement(info);
        }

        public async void AddMeasurement(double value)
        {
            var measurement = new GlucoseObservation(value);
            await UploadMeasurement(measurement);
        }

        public void AddMeasurement(double value, string id)
        {
            throw new System.NotImplementedException();
        }

        private string GetCpr()
        {
            return Utils.Cpr;
        }

        private async Task UploadMeasurement(IObservationSpecifics measurement)
        {
            await Task.Run(() => _n4CHelper.UploadSaturationObservation(GetCpr(), measurement));
        }

        private async Task UploadMeasurement(KeyValuePair<string, string> info)
        {
            await Task.Run(() =>
            {
                var measurement = new GlucoseObservation(double.Parse(info.Value.Split(';').Last()));
                _n4CHelper.UploadGlucoseObservation(GetCpr(), measurement);
            });
        }
    }
}
