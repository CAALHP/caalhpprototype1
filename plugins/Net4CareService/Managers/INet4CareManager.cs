﻿using System.Collections.Generic;

namespace Plugins.Net4CareService.Managers
{
    public interface INet4CareManager
    {
        void AddMeasurement(KeyValuePair<string, string> info);
        /*void AddOximeterMeasurement(KeyValuePair<string, string> notification);
        void AddHeartRateMeasurement(KeyValuePair<string, string> notification);
        void AddBloodPressureMeasurementPart(KeyValuePair<string, string> notification);
        void AddWeightMeasurement(KeyValuePair<string, string> notification);
        void AddBloodSugarMeasurement(KeyValuePair<string, string> notification);*/
        void AddMeasurement(double value);
        void AddMeasurement(double value, string id);
    }
}