﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using CAALHP.Events;
using N4CLibrary;

namespace Plugins.Net4CareService.Managers
{
    public class BloodPressureManager : INet4CareManager
    {
        //private readonly string _cpr;
        private readonly N4CHelper _n4CHelper;
        private readonly Dictionary<Guid,KeyValuePair<string, string>> _infos;

        public BloodPressureManager()
        {
            var serverAddress = ConfigurationManager.AppSettings.Get("serveraddress");
            //_cpr = ConfigurationManager.AppSettings.Get("cpr");
            _n4CHelper = new N4CHelper(serverAddress, typeof(HomeBloodPressureObservation));
            _infos = new Dictionary<Guid, KeyValuePair<string, string>>();
        }

        public async void AddMeasurement(KeyValuePair<string, string> info)
        {
            await UploadMeasurement(info);
        }

        public void AddMeasurement(double value)
        {
            //not used!
            //throw new NotImplementedException();
        }

        public void AddMeasurement(double value, string id)
        {
            throw new NotImplementedException();
        }

        private string GetCpr()
        {
            return Utils.Cpr;
        }

        private async Task UploadMeasurement(KeyValuePair<string, string> info)
        {
            await Task.Run(() =>
            {
                var first = info.Key.Split(';').First();
                var split = first.Split(' ');
                if (split.Count() != 3) return;
                var id = Guid.Parse(split[1]);
                if (_infos.ContainsKey(id))
                {
                    //
                    var storedInfo = _infos[id];
                    double dia;
                    double sys;
                    if (storedInfo.Key.Contains("Diastolic"))
                    {
                        dia = double.Parse(storedInfo.Value.Split(';').Last());
                        sys = double.Parse(split.Last());
                    }
                    else
                    {
                        dia = double.Parse(split.Last());
                        sys = double.Parse(storedInfo.Value.Split(';').Last());
                    }
                        
                    var measurement = new HomeBloodPressureObservation(sys,dia);
                    _n4CHelper.UploadHeartRateObservation(GetCpr(), measurement);

                    //remove value from dictionary
                    _infos.Remove(id);
                }
                else
                {
                    _infos.Add(id, info);
                }
            });
        }

        public async void AddMeasurement(BloodPressureMeasurementEvent bpMeasurement)
        {
            var measurement = new HomeBloodPressureObservation(bpMeasurement.Systolic, bpMeasurement.Diastolic);
            await UploadMeasurement(measurement);
        }

        private async Task UploadMeasurement(HomeBloodPressureObservation observation)
        {
            await Task.Run(() => _n4CHelper.UploadHeartRateObservation(GetCpr(), observation));
        }
    }
}
