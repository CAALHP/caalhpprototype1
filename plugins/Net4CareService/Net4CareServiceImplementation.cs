﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;
using Plugins.Net4CareService.Managers;

namespace Plugins.Net4CareService
{
    public class Net4CareServiceImplementation : IServiceCAALHPContract
    {
        private IServiceHostCAALHPContract _host;
        private int _processId;
        private readonly INet4CareManager _oximeterManager;
        private readonly INet4CareManager _bloodSugarManager;
        private readonly INet4CareManager _weightManager;
        private readonly INet4CareManager _heartRateManager;
        private readonly BloodPressureManager _bloodPressureManager;

        public Net4CareServiceImplementation()
        {
            //Debugger.Launch();

            Utils.Cpr = ConfigurationManager.AppSettings.Get("cpr");

            _oximeterManager = new OximeterManager();
            _bloodSugarManager = new BloodSugarManager();
            _weightManager = new WeightManager();
            _heartRateManager = new HeartRateManager();
            _bloodPressureManager = new BloodPressureManager();

        }

        /*private void HandleEvent(Event theEvent)
        {
            if(theEvent.GetType() == typeof(BloodPressureMeasurementEvent))
                HandleEvent(theEvent as BloodPressureMeasurementEvent);
            else if(theEvent.GetType() == typeof(SimpleMeasurementEvent))
                HandleEvent(theEvent as SimpleMeasurementEvent);
        }*/

        private void HandleEvent(BloodPressureMeasurementEvent bpMeasurement)
        {
            _bloodPressureManager.AddMeasurement(bpMeasurement);
        }

        private void HandleEvent(SimpleMeasurementEvent simpleMeasurement)
        {
            switch (simpleMeasurement.MeasurementType)
            {
                case "Oximeter":
                    {
                        _oximeterManager.AddMeasurement(simpleMeasurement.Value);
                    } break;
                case "HeartRate":
                    {
                        _heartRateManager.AddMeasurement(simpleMeasurement.Value);
                    } break;
                case "Weight":
                    {
                        _weightManager.AddMeasurement(simpleMeasurement.Value, simpleMeasurement.UserId);
                    } break;
                case "BloodSugar":
                    {
                        _bloodSugarManager.AddMeasurement(simpleMeasurement.Value);
                    } break;
            }
        }

        private void HandleEvent(UserLoggedInEvent obj)
        {
            Utils.Cpr = obj.User.UserId;
        }

        private void HandleEvent(UserLoggedOutEvent obj)
        {
            Utils.Cpr = ConfigurationManager.AppSettings.Get("cpr");
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            //var serializer = EventHelper.GetSerializationTypeFromFullyQualifiedNameSpace(notification.Key);
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }

        public string GetName()
        {
            return "Net4CareService";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IServiceHostCAALHPContract hostObj, int processId)
        {
           
            _host = hostObj;
            _processId = processId;
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(BloodPressureMeasurementEvent)),_processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(SimpleMeasurementEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserLoggedInEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserLoggedOutEvent)), _processId);
            //_host.Host.SubscribeToEvents(_processId);
        }

        public void Start()
        {
            //throw new System.NotImplementedException();
        }

        public void Stop()
        {
            //throw new System.NotImplementedException();
        }
    }
}