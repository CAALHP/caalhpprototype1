﻿using System.AddIn;
using Plugins.ServicePluginAdapter;

namespace Plugins.DeviceDriverService
{
    // The AddInAttribute identifies this pipeline segment as an add-in.
    [AddIn("Device Driver Service", Version = "1.0.0.0")]
    public class DeviceDriverService : ServiceViewPluginAdapter
    {
        public DeviceDriverService()
        {
            Service = new DeviceDriverServiceImplementation();
        }
    }
}
