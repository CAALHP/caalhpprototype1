﻿using System.Linq;
using CAALHP.Contracts;

namespace Plugins.DeviceDriverService
{
    public class DeviceDriverStrategy : IDeviceDriverStrategy
    {
        public bool IsDeviceDriverInstalled(string deviceName, IServiceHostCAALHPContract host)
        {
            var installedDeviceDrivers = host.GetListOfInstalledDeviceDrivers();
            var name = deviceName.Replace(".zip","");
            return installedDeviceDrivers.Any(installedDeviceDriver => installedDeviceDriver.LocationDir.EndsWith(name));
        }
    }
}