﻿using System;
using System.Collections.Generic;
using System.Linq;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;

namespace Plugins.DeviceDriverService
{
    public class DeviceDriverServiceImplementation : IServiceCAALHPContract
    {
        private IServiceHostCAALHPContract _host;
        private int _processId;
        private readonly IDeviceDriverStrategy _strategy;

        public DeviceDriverServiceImplementation()
        {
            _strategy = new DeviceDriverStrategy();
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }

        private void HandleEvent(DeviceFoundEvent e)
        {
            if (!_strategy.IsDeviceDriverInstalled(e.Name, _host))
            {
                SendInstallCommand(e.DeviceDriverFileName);
            }
        }

        private void SendInstallCommand(string deviceName)
        {
            if (string.IsNullOrWhiteSpace(deviceName)) return;
            var command = new DownloadDeviceDriverEvent
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                DeviceDriverFileName = deviceName
            };
            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);
            _host.Host.ReportEvent(serializedCommand);
        }

        public string GetName()
        {
            return "DeviceDriverService";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IServiceHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            //_host.Host.SubscribeToEvents(_processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(DeviceFoundEvent)), _processId);
        }

        public void Start()
        {
            //throw new System.NotImplementedException();
        }

        public void Stop()
        {
            //throw new System.NotImplementedException();
        }
    }
}