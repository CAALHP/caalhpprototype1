﻿using System.AddIn.Contract;
using System.AddIn.Pipeline;

namespace Plugins.Contracts
{
    [AddInContract]
    public interface IDeviceDriverHostContract : IContract
    {
        IHostContract Host { get; set; }
        //void ReportMeasurement(KeyValuePair<string, string> value);
    }
}