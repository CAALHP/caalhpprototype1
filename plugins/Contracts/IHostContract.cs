﻿using System.AddIn.Contract;
using System.AddIn.Pipeline;
using System.Collections.Generic;

namespace Plugins.Contracts
{
    [AddInContract]
    public interface IHostContract : IContract
    {
        void ReportEvent(KeyValuePair<string, string> value);
        //void ReportEvent(string fullyQualifiedNameSpace, KeyValuePair<string, string> value); 
        //void SubscribeToEvents(int processId);
        void SubscribeToEvents(string fullyQualifiedNameSpace,int processId);
        //void UnSubscribeToEvents(int processId);
        void UnSubscribeToEvents(string fullyQualifiedNameSpace, int processId);
    }
}