﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Reflection;
using System.Security.Policy;
using System.Windows;
using System.Windows.Media;
using CAALHP.Contracts;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using Plugins.HomeScreenApp.Views;

namespace Plugins.HomeScreenApp.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        //private IAppCAALHPContract _host;
        public IList<IPluginInfo> AppList { get; set; }

        //private IList<string> _imagePaths;
        private ObservableCollection<AppTileViewModel> _appTiles;
        private const string AppImage = "images\\app.png";
        private const string AppImageName = "app.png";

        /*public IList<string> ImagePaths
        {
            get
            {
                return _imagePaths;
            }
            set
            {
                _imagePaths = value;
                RaisePropertyChanged("ImagePaths");
            }
        }*/

        public ObservableCollection<AppTileViewModel> AppTiles
        {
            get
            {
                return _appTiles;
            }
            set
            {
                _appTiles = value;
                RaisePropertyChanged("AppTiles");
            }
        }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()//IAppCAALHPContract host)
        {
            //_host = host;
            AppList = new List<IPluginInfo>();
            _appTiles = new ObservableCollection<AppTileViewModel>();
            //_imagePaths = new List<string>();
            UpdateApps();
        }

        public void UpdateApps()
        {
            //UpdateImagePaths();
            UpdateAppTiles();
        }

        /*private void UpdateImagePaths()
        {
            foreach (var path in AppList.Select(app => Path.Combine(app.LocationDir, Appimage)).Where(File.Exists))
            {
                if (!_imagePaths.Contains(path))
                {
                    _imagePaths.Add(path);
                }
            }
            RaisePropertyChanged("ImagePaths");
        }*/

        private void UpdateAppTiles()
        {
            foreach (var app in AppList)
            {
                var imagePath = Path.Combine(app.LocationDir, AppImage);
                if (!File.Exists(imagePath)) continue;
                var current = AssemblyDirectory;
                if (current == null) continue;
                //create directory for file
                var targetDir = Path.Combine(current, "./Images/", app.Name);
                if (!Directory.Exists(targetDir))
                {
                    Directory.CreateDirectory(targetDir);
                }
                //copy file so we don't lock it from the plugin
                var targetImagePath = Path.Combine(targetDir, AppImageName);
                if(!File.Exists(targetImagePath))
                    File.Copy(imagePath, targetImagePath);
                //then refer to the copy
                if (AppTiles.All(x => x.MainTitle != app.Name))
                {
                    var tile = new AppTileViewModel { MainTitle = app.Name, ImagePath = targetImagePath };
                    AppTiles.Add(tile);
                }
            }
            //RaisePropertyChanged("AppTiles");
        }

        static private string AssemblyDirectory
        {
            get
            {
                var codeBase = Assembly.GetExecutingAssembly().CodeBase;
                var uri = new UriBuilder(codeBase);
                var path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        public void Show()
        {
            UpdateApps();
            Messenger.Default.Send(new NotificationMessage(this, typeof(MainView), "Show"));
        }

    }
}