﻿using System.AddIn;
using Plugins.AppPluginAdapters;

namespace Plugins.HomeScreenApp
{
    // The AddInAttribute identifies this pipeline segment as an add-in.
    [AddIn("System HomeScreen App", Version = "1.0.0.0")]
    public class HomeScreenBoilerplate : AppPluginAdapter
    {
        public HomeScreenBoilerplate()
        {
            App = new HomeScreenImplementation();
        }
    }
}
