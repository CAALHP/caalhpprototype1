﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Threading;
using IKriv.Wpf;
using Plugins.HomeScreenApp.ViewModel;
using Plugins.HomeScreenApp.Views;

namespace Plugins.HomeScreenApp
{
    public class HomeScreenImplementation : IAppCAALHPContract
    {
        private IAppHostCAALHPContract _host;
        private int _processId;
        private Thread _thread;
        private const string AppName = "HomeScreen";

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        //Tell compiler not to optimize the _app variable by declaring it as volatile!
        private volatile Application _app;

        public HomeScreenImplementation()
        {
            try
            {
                InitThread();
                //wait for the _app to be initialized
                //We declare _app as volatile so it won't be cached by optimizations
                //If _app is not volatile the following line could be cached in release mode and never finish.
                while (_app == null) { }

                //_vml = new ViewModelLocator();
                _app.Resources.Add("Locator", new ViewModelLocator());

                //Add datatemplates for subviews
                var manager = new DataTemplateManager(_app);
                manager.RegisterDataTemplate<MainViewModel, MainView>();
                manager.RegisterDataTemplate<AppTileViewModel, AppTileView>();

                //SimpleIoc.Default.Register<IAppCAALHPContract, this>();

                DispatchToApp(() => new MainView().Hide());
                // { DataContext = _vml.Main }.Hide());
                //Show homescreen as soon as the plugin is started

                //Show();
                Messenger.Default.Register<NotificationMessage>(this, SendStartAppMessage);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void SendStartAppMessage(NotificationMessage message)
        {
            if (!message.Target.Equals(GetType())) return;
            var showAppEvent = new ShowAppEvent
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                AppName = message.Notification
            };
            var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, showAppEvent);
            _host.Host.ReportEvent(serializedEvent);
            //var command = EventHelper.CreateEvent(_processId, GetName(), "ShowApp", message.Notification);
            //_host.Host.ReportEvent(command);
            _host.ShowApp(message.Notification);
        }

        /*
        private void InitLog()
        {
            _log = new StreamWriter( new FileStream(
                "C:\\Users\\rgst\\Documents\\Visual Studio 2012\\Projects\\CAALHPPrototype1\\MyApp\\Pipeline\\AddIns\\Plugins.HomeScreenApp\\HomeScreenApp.log", 
                FileMode.Append));
            
        }*/

        public void Notify(KeyValuePair<string, string> notification)
        {
            //We care about the "Home" message
            /*if (notification.Key.Contains("ShowApp") && notification.Value.Contains("HomeScreen"))
            {
                //Show homescreen
                Show();
            }
            else if (notification.Key.Contains("InstallCompleted"))
            {
                Show();
            }*/
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }

        private void HandleEvent(ShowAppEvent e)
        {
            if (e.AppName.Equals(AppName))
            {
                //Show homescreen
                Show();
            }
        }

        private void HandleEvent(InstallAppCompletedEvent e)
        {
            //Show homescreen
            Show();

        }

        public string GetName()
        {
            return AppName;
        }

        public void Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            //Subscribe to events so we can react to "Home" messages
            //_host.Host.SubscribeToEvents(_processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(ShowAppEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(InstallAppCompletedEvent)), _processId);
            //var list = _host.GetListOfInstalledApps();
            Show();
            var timer = new DispatcherTimer(new TimeSpan(0,0,1),DispatcherPriority.Normal,UpdateAppList,_app.Dispatcher );
            timer.Start();
        }

        private void UpdateAppList(object sender, EventArgs e)
        {
            DispatchToApp(() =>
            {
                var vml = _app.Resources["Locator"] as ViewModelLocator;
                if (vml == null) return;
                vml.Main.AppList = GetListOfInstalledApps();
                vml.Main.UpdateApps();
            });
        }

        public void Show()
        {
            //ref: http://reedcopsey.com/2011/11/28/launching-a-wpf-window-in-a-separate-thread-part-1/
            //Threading is used so we do not block the main thread.
            DispatchToApp(() =>
            {
                var vml = _app.Resources["Locator"] as ViewModelLocator;
                if (vml != null)
                {
                    var proc = Process.GetProcessById(_processId);
                    var handle = proc.MainWindowHandle;
                    ShowWindow(handle, 3);
                    vml.Main.AppList = GetListOfInstalledApps();
                    vml.Main.Show();
                }
            });

        }

        public IList<IPluginInfo> GetListOfInstalledApps()
        {
            return _host.GetListOfInstalledApps();
        }

        private void InitThread()
        {
            _thread = new Thread(() =>
            {
                _app = new Application();
                _app.ShutdownMode = ShutdownMode.OnExplicitShutdown;
                /*SynchronizationContext.SetSynchronizationContext(
                    new DispatcherSynchronizationContext(
                        Dispatcher.CurrentDispatcher));*/
                DispatcherHelper.Initialize();
                _app.Run();
            });

            //This is needed for GUI threads
            _thread.SetApartmentState(ApartmentState.STA);
            // Make the thread a background thread
            //_thread.IsBackground = true;

            _thread.Start();
        }

        private void DispatchToApp(Action action)
        {
            _app.Dispatcher.Invoke(action);
        }

    }
}