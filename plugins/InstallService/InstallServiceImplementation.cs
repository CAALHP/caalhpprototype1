﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Reflection;
using System.Threading;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;

namespace Plugins.InstallService
{
    public class InstallServiceImplementation : IServiceCAALHPContract
    {
        private IServiceHostCAALHPContract _host;
        private int _processId;
        private readonly IInstallStrategy _installer;

        public InstallServiceImplementation()
        {
            _installer = new InstallPluginStrategy();
        }

        /// <summary>
        /// Handle event notifications 
        /// </summary>
        /// <param name="notification">The notification object that the host sends to the subscribing plugin</param>
        public void Notify(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }

        private void HandleEvent(DownloadAppCompletedEvent e)
        {
            InstallApp(e.FileName);
        }

        private void HandleEvent(DownloadDeviceDriverCompletedEvent e)
        {
            InstallDeviceDriver(e.DeviceDriverFileName);
        }

        /// <summary>
        /// Install a device driver async
        /// </summary>
        /// <param name="filename"></param>
        private async void InstallDeviceDriver(string filename)
        {
            try
            {
                await _installer.InstallAsync(filename, CancellationToken.None);
                //Report success
                //var command = EventHelper.CreateEvent(_processId, GetName(), "InstallDeviceDriverCompleted", filename);
                var command = new InstallDeviceDriverCompletedEvent
                {
                    CallerName = GetName(),
                    CallerProcessId = _processId,
                    DeviceDriverFileName = filename
                };
                var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);
                _host.Host.ReportEvent(serializedCommand);
                PlayInstallCompleteSound();
                _host.ActivateDeviceDrivers();
            }
            catch (Exception ex)
            {
                //Report error to the host
                //var command = EventHelper.CreateEvent(_processId, GetName(), "InstallDeviceDriverFailed", filename);
                var command = new InstallDeviceDriverFailedEvent()
                {
                    CallerName = GetName(),
                    CallerProcessId = _processId,
                    DeviceDriverFileName = filename,
                    Reason = ex.Message
                };
                var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);
                _host.Host.ReportEvent(serializedCommand);
            }
        }

        /// <summary>
        /// Remember to document everything
        /// Play a sound when install completes
        /// </summary>
        private void PlayInstallCompleteSound()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var dir = Path.GetDirectoryName(assembly.Location);
            if (dir == null) return;
            var soundfile = Path.Combine(dir,"complete.wav");
            var player = new SoundPlayer(soundfile);
            player.Play();
        }


        /// <summary>
        /// Install an app async
        /// </summary>
        /// <param name="filename"></param>
        private async void InstallApp(string filename)
        {
            try
            {
                _host.CloseApp(Path.GetFileNameWithoutExtension(filename));
                await _installer.InstallAsync(filename, CancellationToken.None);
                //var command = EventHelper.CreateEvent(_processId, GetName(), "InstallCompleted", filename);
                var command = new InstallAppCompletedEvent()
                {
                    CallerName = GetName(),
                    CallerProcessId = _processId,
                    FileName = filename
                };
                var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);
                
                _host.Host.ReportEvent(serializedCommand);
                PlayInstallCompleteSound();
            }
            catch (Exception ex)
            {
                //var command = EventHelper.CreateEvent(_processId, GetName(), "InstallFailed", filename);
                var command = new InstallAppFailedEvent()
                {
                    CallerName = GetName(),
                    CallerProcessId = _processId,
                    AppFileName = filename,
                    Reason = ex.Message
                };
                var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);
                _host.Host.ReportEvent(serializedCommand);
            } 
        }

        /// <summary>
        /// Default method for getting the name of the plugin
        /// </summary>
        /// <returns></returns>
        public string GetName()
        {
            return "InstallService";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        /// <summary>
        /// Initialize the hostobject
        /// </summary>
        /// <param name="hostObj"></param>
        /// <param name="processId"></param>
        public void Initialize(IServiceHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            //This plugin needs to subscribe to events from the host
            //_host.Host.SubscribeToEvents(_processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(DownloadAppCompletedEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(DownloadDeviceDriverCompletedEvent)), _processId);
        }

        public void Start()
        {
            //throw new System.NotImplementedException();
        }

        public void Stop()
        {
            //throw new System.NotImplementedException();
        }
    }
}