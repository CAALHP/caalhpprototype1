﻿using System.AddIn;
using Plugins.ServicePluginAdapter;

namespace Plugins.InstallService
{
    // The AddInAttribute identifies this pipeline segment as an add-in.
    [AddIn("Install Service", Version = "1.0.0.0")]
    public class InstallService : ServiceViewPluginAdapter
    {
        public InstallService()
        {
            Service = new InstallServiceImplementation();
        }
    }
}
