﻿using System.Threading;
using System.Threading.Tasks;

namespace Plugins.InstallService
{
    public interface IInstallStrategy
    {
        Task InstallAsync(string pathtofile, CancellationToken cancellationToken);
    }
}
