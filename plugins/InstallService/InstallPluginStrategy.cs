﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Ionic.Zip;

namespace Plugins.InstallService
{
    public class InstallPluginStrategy : IInstallStrategy
    {
        private readonly string _downloadDir;
        private string _targetDir;
        //private readonly IDeviceDriverHost _host;

        public InstallPluginStrategy()
        {
            _downloadDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings.Get("downloadDirectory"));
            _targetDir = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings.Get("addInDirectory"));
        }

        public async Task InstallAsync(string filename, CancellationToken cancellationToken)
        {
            await Task.Run(() => UnZip(_downloadDir + filename, _targetDir), cancellationToken);
        }

        private static void UnZip(string pathtofile, string targetdir)
        {
            using (var zip = ZipFile.Read(pathtofile))
            {

                // This call to ExtractAll() assumes:
                //   - none of the entries are password-protected.
                //   - want to extract all entries to current working directory
                //   - none of the files in the zip already exist in the directory;
                //     if they do, the method will throw.
                if (zip != null)
                {

                    try
                    {
                        var dir = new DirectoryInfo(targetdir + Path.GetFileNameWithoutExtension(pathtofile));
                        if(dir.Exists)
                            CleanDirectory(new DirectoryInfo(targetdir + Path.GetFileNameWithoutExtension(pathtofile)));
                        zip.ExtractExistingFile = ExtractExistingFileAction.OverwriteSilently;
                        zip.ExtractAll(targetdir);
                    }
                    catch (Exception e)
                    {
                        throw;
                    }
                }
            }
        }

        private static void CleanDirectory(DirectoryInfo targetdir)
        {
            //recursively clean subdirectories
            foreach (var subdir in targetdir.EnumerateDirectories())
            {
                CleanDirectory(subdir);
            }
            //clean files from this directory
            foreach (var file in targetdir.EnumerateFiles()) //.Where(file => file.Extension.ToLower().Equals(".tmp")))
            {
                file.Delete();
            }
        }
    }
}
