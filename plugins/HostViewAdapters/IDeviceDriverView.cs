﻿namespace Plugins.HostViewAdapters
{
    public interface IDeviceDriverView : IPluginBaseView
    {
        //double GetMeasurement();
        void Initialize(IDeviceDriverHostView host, int processId);
    }
}
