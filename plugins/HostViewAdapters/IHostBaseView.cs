﻿using System.Collections.Generic;

namespace Plugins.HostViewAdapters
{
    public interface IHostBaseView
    {
        void ReportEvent(KeyValuePair<string, string> value);
        //void ReportEvent(string fullyQualifiedNameSpace, KeyValuePair<string, string> value);
        //void SubscribeToEvents(int processId);
        void SubscribeToEvents(string fullyQualifiedNameSpace, int processId);
        //void UnSubscribeToEvents(int processId);
        void UnSubscribeToEvents(string fullyQualifiedNameSpace, int processId);
    }
}