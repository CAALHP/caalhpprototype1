﻿using System.Collections.Generic;
using CAALHP.Contracts;

namespace Plugins.HostViewAdapters
{
    public interface IAppHostView //: IHostObjectBaseView
    {
        IHostBaseView Host { get; set; }
        void ShowApp(string appName);
        void CloseApp(string appName);
        IList<IPluginInfo> GetListOfInstalledApps();
    }
}