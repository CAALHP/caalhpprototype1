﻿using System.Collections.Generic;
using CAALHP.Contracts;

namespace Plugins.HostViewAdapters
{
    public interface IServiceHostView
    {
        IHostBaseView Host { get; set; }
        IList<IPluginInfo> GetListOfInstalledApps();
        IList<IPluginInfo> GetListOfInstalledDeviceDrivers();
        void CloseApp(string fileName);
        void ActivateDeviceDrivers();
        IList<string> GetListOfEventTypes();
    }
}