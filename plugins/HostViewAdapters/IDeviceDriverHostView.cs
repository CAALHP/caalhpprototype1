﻿namespace Plugins.HostViewAdapters
{
    public interface IDeviceDriverHostView
    {
        IHostBaseView Host { get; set; }
    }
}