﻿using System.Collections.Generic;
using System.IO;
using CAALHP.Contracts;
using Plugins.AddInViews;
using Plugins.PluginAdapter;

namespace Plugins.ServicePluginAdapter
{
    public class ServiceHostViewToCAALHPContractAdapter : IServiceHostCAALHPContract
    {
        public IHostCAALHPContract Host { get; set; }
        private readonly IServiceHostView _hostView;

        public ServiceHostViewToCAALHPContractAdapter(IServiceHostView host)
        {
            _hostView = host;
            Host = new HostViewToHostCAALHPContractAdapter(_hostView.Host);
        }

        public IList<IPluginInfo> GetListOfInstalledApps()
        {
            return _hostView.GetListOfInstalledApps();
        }
        
        public IList<IPluginInfo> GetListOfInstalledDeviceDrivers()
        {
            return _hostView.GetListOfInstalledDeviceDrivers();
        }

        public void CloseApp(string fileName)
        {
            _hostView.CloseApp(fileName);
        }

        public void ActivateDeviceDrivers()
        {
            _hostView.ActivateDeviceDrivers();
        }

        public IList<string> GetListOfEventTypes()
        {
            return _hostView.GetListOfEventTypes();
        }
    }
}