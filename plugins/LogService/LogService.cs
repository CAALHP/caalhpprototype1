﻿using System.AddIn;
using log4net.Config;
using Plugins.ServicePluginAdapter;

namespace Plugins.LogService
{
    // The AddInAttribute identifies this pipeline segment as an add-in.
    [AddIn("Log Service", Version = "1.0.0.0")]
    public class LogService : ServiceViewPluginAdapter
    {
        public LogService()
        {
            Service = new LogImplementation();
        }
    }
}
