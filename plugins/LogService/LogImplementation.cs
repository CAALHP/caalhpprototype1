﻿using System;
using System.Collections.Generic;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;
using log4net;
using log4net.Config;

namespace Plugins.LogService
{
    public class LogImplementation : IServiceCAALHPContract
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private IServiceHostCAALHPContract _host;
        private int _processId;

        public LogImplementation()
        {
            //Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        }
        
        public void Notify(KeyValuePair<string, string> notification)
        {
            Log.Info(notification.Key + " : " + notification.Value);
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            if (type != typeof (NewEventTypeAddedEvent)) return;
            var obj = JsonSerializer.DeserializeEvent(notification.Value, type) as NewEventTypeAddedEvent;
            if (obj == null) return;
            Log.Info("LogService subscribing to " + obj.EventType);
            _host.Host.SubscribeToEvents(obj.EventType, _processId);
        }

        public string GetName()
        {
            return "LogService";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IServiceHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            //_host.Host.SubscribeToEvents(_processId);
            SubscribeToAllEvents();
            Log.Info("LogService Initialized");
        }

        private void SubscribeToAllEvents()
        {
            
            var knownEvents = _host.GetListOfEventTypes();
            foreach (var knownEvent in knownEvents)
            {
                _host.Host.SubscribeToEvents(knownEvent, _processId);
                
                Log.Info("LogService subscribing to " + knownEvent);
            }
            //var fqns = EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof (NewEventTypeAddedEvent));
            //_host.Host.SubscribeToEvents(fqns, _processId);
            //Log.Info("LogService subscribing to " + fqns);
        }

        public void Start()
        {
            Log.Info("LogService started");
        }

        public void Stop()
        {
            Log.Info("LogService stopped");
        }
    }
}