﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FS.Events
{
    public class FSResponsEvent : Event
    {
        public string UserId { get; set; }
        public Boolean Result { get; set; }
        public string Message { get; set; }
        public RequestTypes RequestType { get; set; }
    }
}
