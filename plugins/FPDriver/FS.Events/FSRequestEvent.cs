﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FS.Events
{
    public enum RequestTypes
    {
        Auth,
        Enroll,
        CheckforScanner,
        DeleteUser,
        ClearDb
    }

    public class FSRequestEvent : Event
    {
        public string UserId { get; set; }
        public RequestTypes RequestType { get; set; }
    }
}
