﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FS.Events
{
    public class FingerEnrollErrorResponsEvent: Event
    {
        public string Error { get; set; }
    }
}
