﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Neurotec.Biometrics;

namespace FSAdaptor
{
    public class FingerServiceManually
    {
        #region fields

        private string _filePath;
        private uint _timeOutDuration;
        private string _scanner;

        #endregion

        #region constructor

        public FingerServiceManually()
        {
            //TODO MAKE SURE THIS WORKS. ELSE CHANGE IMPLEMENTATION FOR PATH
            //checks if templates folder exsists in current domain of the application, if not creates the folder.
            //if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "Templates"))
            //{
            //    Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "Templates");
            //}
           // _filePath = Assembly.GetExecutingAssembly().Location;
            //_filePath = _filePath.Replace("FSAdaptor.dll", "");
            _filePath = AssemblyDirectory + "\\Templates\\";

            if (!Directory.Exists(_filePath))
            {
                Directory.CreateDirectory(_filePath);
            }

            //_filePath =  "Templates\\";

            //string test = ConfigurationManager.AppSettings["enrolltimeout"];

            try
            {
                XDocument doc = XDocument.Load(AppDomain.CurrentDomain.BaseDirectory + "Config.xml");

                var enrolltimeout = doc.Root.Element("enrolltimeout").Value;
                uint.TryParse(enrolltimeout, out _timeOutDuration);

                _scanner = doc.Root.Element("scanner").Value;
            }
            catch (Exception e)
            {
                _timeOutDuration = 20000;
                _scanner = "futronic";
            }

            

           // uint.TryParse(ConfigurationManager.AppSettings["enrolltimeout"], out _timeOutDuration);
           // _scanner = ConfigurationManager.AppSettings["scannerdevice"];

        }

        #endregion

        #region public methods

        /// <summary>
        /// Enrolls a new user into the database, and checks against exsisting user.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool EnrollNewUser(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentNullException();

            var newUserFilePath = _filePath + id + ".dat";

            if (File.Exists(newUserFilePath))
            {
                var userExsists = Verify(id);
                if (userExsists)
                    throw new ArgumentException("User already exsists");
            }

            var engine = new Nffv(newUserFilePath, "", _scanner);

            NffvStatus status;
            try
            {
                engine.Enroll(_timeOutDuration, out status);
            }
            catch (Exception)
            {
                engine.Dispose();
                return false;
            }

            if (status != NffvStatus.TemplateCreated)
            {
                engine.Dispose();
                if (File.Exists(newUserFilePath))
                {
                    File.Delete(newUserFilePath);
                }

                throw new Exception(status.ToString());
            }


            engine.Dispose();
            return true;
        }

        /// <summary>
        /// Enrolls a new user into a template, and checks against exsisting user. Template is not stored in local database, and must be added later
        /// </summary>
        /// <param name="id"></param>
        /// <param name="template">bytearray containing the file used by the adaptor</param>
        /// <returns></returns>
        public bool EnrollNewUser(string id, out byte[] template)
        {
            template = null;

            if (string.IsNullOrEmpty(id))
                throw new ArgumentNullException();

            var newUserFilePath = _filePath + id + ".dat";

            if (File.Exists(newUserFilePath))
            {
                var userExsists = Verify(id);
                if (userExsists)
                    throw new ArgumentException("User already exsists");
            }

            var engine = new Nffv(newUserFilePath, "", _scanner);

            NffvStatus status;
            try
            {
                engine.Enroll(_timeOutDuration, out status);
            }
            catch (Exception)
            {
                engine.Dispose();
                return false;
            }

            if (status != NffvStatus.TemplateCreated)
            {
                engine.Dispose();
                if (File.Exists(newUserFilePath))
                {
                    File.Delete(newUserFilePath);
                }

                throw new Exception(status.ToString());
            }

            try
            {
                template = File.ReadAllBytes(newUserFilePath);
            }
            finally
            {
                File.Delete(newUserFilePath);
            }
            

            engine.Dispose();
            return true;
        }

        /// <summary>
        /// Verify the selected user against the database.
        /// </summary>
        /// <param name="id">Id of user to be verifyed</param>
        /// <returns></returns>
        public bool Verify(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentNullException();

            var userFilePath = _filePath + id + ".dat";

            if (!File.Exists(userFilePath))
                throw new ArgumentException("No user");

            var engine = new Nffv(userFilePath, "", _scanner);
            NffvStatus status;

            //if(engine.Users.Count == 0)

            var score = engine.Verify(engine.Users[0], _timeOutDuration, out status);

            if (status != NffvStatus.TemplateCreated)
            {
                engine.Dispose();
                throw new Exception(status.ToString());
            }

            engine.Dispose();
            return score > 0;
        }

        /// <summary>
        /// Checks if scanner is availeble.
        /// </summary>
        /// <returns></returns>
        public bool ScannerAvaileble()
        {
            string userFilePath = _filePath + "test" + ".dat";

            var engine = new Nffv(userFilePath, "", _scanner);
            engine.Users.Clear();

            NffvStatus status;
            engine.Enroll(200, out status);
            
            engine.Dispose();
            return status != NffvStatus.NoScanner;
        }

        /// <summary>
        /// Removes a user template permanently from the disk, no reversal.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>true if user deleted</returns>
        public bool RemoveUser(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentNullException();

            var userFilePath = _filePath + id + ".dat";

            if (!File.Exists(userFilePath))
                return false;

            File.Delete(userFilePath);

            return true;

        }

        /// <summary>
        /// Removes all templates stored on disk permanently, no reversal.
        /// </summary>
        public void CleanDb()
        {
            Array.ForEach(Directory.GetFiles(_filePath), File.Delete);
        }

        #endregion

        #region private methods

        private string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        #endregion

    }
}
