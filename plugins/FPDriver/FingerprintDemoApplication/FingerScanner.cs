﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Neurotec.Biometrics;

namespace FingerprintDemoApplication
{
    public class FingerScanner
    {
        public FingerScanner()
        {
            //if (string.IsNullOrEmpty(dbfileName) && string.IsNullOrEmpty(scanner))
            //    throw new ArgumentException( "Missing database or scanner name");
            try
            {
                Engine = new Nffv(ConfigurationManager.AppSettings["databasefilename"], "", ConfigurationManager.AppSettings["scannerdevice"]);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public Nffv Engine { get; private set; }

        public void Enroll(out EnrollmentResult status)
        {
            EnrollmentResult enrollmentResults = new EnrollmentResult();
            enrollmentResults.engineUser = Engine.Enroll(20000, out enrollmentResults.engineStatus);
            status = enrollmentResults;
        }

        public bool Verify(out VerificationResult result)
        {
            var tmpResult = new VerificationResult();

            foreach (NffvUser user in Engine.Users)
            {
                tmpResult.score = Engine.Verify(user, 20000, out tmpResult.engineStatus);
                
                if (tmpResult.score <= 0) continue;

                tmpResult.engineUser = user;
                result = tmpResult;
                return true;
            }
            result = tmpResult;
            return false;
        }
    }

    public class EnrollmentResult
    {
        public NffvStatus engineStatus;
        public NffvUser engineUser;
    };


    public class VerificationResult
    {
        public NffvStatus engineStatus;
        public int score;
        public NffvUser engineUser;
    };
}
