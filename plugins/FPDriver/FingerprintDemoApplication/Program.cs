﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using FSAdaptor;
using Neurotec.Biometrics;

namespace FingerprintDemoApplication
{
    class Program
    {
        static void Main(string[] args)
        {


            FingerServiceManually scanner = new FingerServiceManually();
            

            //if (scanner.ScannerAvaileble())
            //    Console.WriteLine("Vi har en scanner");
            //else
            //{
            //    Console.WriteLine("Ingen Scanner");
            //    return;
            //}

            Console.WriteLine("place finger on scanner, press anykey to test \n");
            Console.ReadKey();

            //Clears database before testing.
            scanner.CleanDb();
            
            try
            {
                scanner.Verify("Peter");
                
            }
            catch (Exception e)
            {
                
                Console.WriteLine(e.Message);
            }

            try
            {
                scanner.EnrollNewUser("Peter");
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
            }

            try
            {
                scanner.EnrollNewUser("Peter");
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
            }

            try
            {
                scanner.Verify("Peter");
                Console.WriteLine("Hej Peter");
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
            }

            Console.WriteLine("press anykey to test timeout\n");
            Console.ReadKey();

            try
            {
                scanner.Verify("Peter");
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
            }

            Console.WriteLine("enroll new user, press to continue\n");
            Console.ReadKey();

            try
            {
                if(scanner.EnrollNewUser("Stefan"))
                       Console.WriteLine("succes enrolling.");

            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
            }

            try
            {
                scanner.Verify("Stefan");
                Console.WriteLine("Hej Stefan");
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
            }


            Console.WriteLine("exit\n");
            Console.ReadKey();


            FingerScanner _engine;
            var fingerList = new List<KeyValuePair<NffvUser, string>>();
            bool running = true;
            

            Console.WriteLine("Initialising fingerprint scanner");

            //String temp = Nffv.GetAvailableScannerModules();

            //Console.WriteLine(temp);

            
            try
            {
                _engine = new FingerScanner();
            }
            catch (Exception)
            {
                Console.WriteLine("Failed loading engine\r\n");
                return;
            }

            while (running)
            {
                Console.WriteLine("Press a key to continue :\n 1) Enroll new person\n 2) Verify user \n 3) Exit");
                var keypressed = Console.ReadKey() ;

                switch (keypressed.KeyChar)
                {
                    case '1':
                        Console.WriteLine("Type in UserName :\n");
                        var username = Console.ReadLine();
                        Console.WriteLine("Place thump on scanner and press anykey\n");
                        Console.ReadKey();
                        try
                        {
                            var result = new EnrollmentResult();
                            _engine.Enroll(out result);
                            if (result.engineStatus == NffvStatus.TemplateCreated)
                            {
                                fingerList.Add(new KeyValuePair<NffvUser, string>(result.engineUser, username));
                                Console.WriteLine("User Added");
                            }
                            else
                            {
                                Console.WriteLine("Enrollment failed : {0}",result.engineStatus);
                            }
                        }
                        catch (Exception ex)
                        {

                            Console.WriteLine("Enrollment failed : {0}", ex.Message);
                        }

                        Console.WriteLine("Press anykey to continue\n\n");
                        Console.ReadKey();

                        break;
                    case '2':
                        Console.WriteLine("Place finger on scanner and press anykey\n");
                        Console.ReadKey();
                        try
                        {
                            var result = new VerificationResult();

                            //Stopwatch watch = new Stopwatch();
                            //watch.Start();
                            var match = _engine.Verify(out result);

                            //watch.Stop();
                            //Console.WriteLine(watch.ElapsedMilliseconds);
                            if (match)
                            {
                                Console.WriteLine("User identified as : {0}", fingerList.First( x => x.Key.Id == result.engineUser.Id).Value);
                            }
                            else
                            {
                                if (result.engineStatus == NffvStatus.TemplateCreated)
                                    Console.WriteLine("Verification failed : No match");
                                else
                                    Console.WriteLine("Verification failed : {0}", result.engineStatus);
                            }
                        }
                        catch (Exception ex)
                        {

                            Console.WriteLine("Verification failed : {0}", ex.Message);
                        }
                        break;
                    case '3':
                        running = false;
                        break;
                }
            }

            //Console.WriteLine("Press anykey to start enroll \n");
            //Console.ReadKey();

            //EnrollmentResult enrollmentResults = new EnrollmentResult();
            //enrollmentResults.engineUser = _engine.Enroll(20000, out enrollmentResults.engineStatus);

            //if (enrollmentResults.engineStatus == NffvStatus.TemplateCreated)
            //{
            //    string username;
            //    Console.WriteLine("Type username :\n");
            //    username = Console.ReadLine();

            //    fingerList.Add(new KeyValuePair<NffvUser, string>(enrollmentResults.engineUser, username));
            //}
            //else
            //{
            //    Console.WriteLine("enroll failed due to : {0}", enrollmentResults.engineStatus);
            //}

            //Console.WriteLine("Press key to verify");
            //Console.ReadKey();

            //var verificationres = new VerificationResult();

            //foreach (NffvUser user in _engine.Users)
            //{
            //    verificationres.score = _engine.Verify(user, 20000, out verificationres.engineStatus);

            //    if (verificationres.score > 0)
            //    {
            //        Console.WriteLine("Identifyed as {0}\n", user.Id);
            //        break;
            //    }
            //    else
            //    {
            //        Console.WriteLine("Failed to identify \n");
            //    }
            //}

            

            //verificationres.score = _engine.Verify(fingerList.First().Key, 20000, out verificationres.engineStatus);

            //if (verificationres.score > 0)
            //{
            //    Console.WriteLine("Identifyed as {0}\n", fingerList.First().Value);
            //}
            //else
            //{
            //    Console.WriteLine("Failed to identify \n");
            //}

            
        }

        private void Enroll()
        {
            
        }

    }


}
