﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FSAdaptor;
using FSAdaptor.Events;

namespace FingerServiceTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private FingerServiceAutomatic finger;
        private ObservableCollection<string> _userList = new ObservableCollection<string>();

        public MainWindow()
        {
            InitializeComponent();
            
            finger = new FingerServiceAutomatic();
            //clears the database before using it, this is only done for the purpose of demonstration.
            //For a proper implementation u should keep a database that is syncroniced with the internal database of the fingerservice. 
            finger.ClearDb();

            Closing += OnClosing;

            finger.EnrollEvent += EnrollEvent;
            finger.VerifiedEvent += VerifiedEvent;

            UserListView.ItemsSource = _userList;
        }

        public List<string> UserList { get; private set; }

        private void OnClosing(object sender, CancelEventArgs cancelEventArgs)
        {
            finger.Dispose();
        }

        private void VerifiedEvent(object sender, VerifyEventArgs verifyEventArgs)
        {
            Dispatcher.BeginInvoke((Action)(() => Verifyresult.Content = verifyEventArgs.Username ));
            
        }

        private void EnrollEvent(object sender, FingerEventArgs fingerEventArgs)
        {
            Dispatcher.BeginInvoke((Action)(() => UpdateOnEnroll(fingerEventArgs)));
        }

        private void UpdateOnEnroll(FingerEventArgs arg)
        {
            if (!arg.Status)
                Enrollresult.Content = arg.Message;
            else
            {
                Enrollresult.Content = "Enrollment succeded.";
                _userList.Add(arg.Message);
            }
            
            Usernamebox.Clear();
            finger.VerifiedEvent += VerifiedEvent;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            finger.VerifiedEvent -= VerifiedEvent;

            finger.EnrollNewUser(this.Usernamebox.Text);
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            bool result = finger.ClearDb();
            if (result)
            {
                _userList.Clear();
            }

        }

        private void RemoveUserButton_Click(object sender, RoutedEventArgs e)
        {
            if (UserListView.SelectedIndex == -1) return;

            var result =  finger.RemoveUser(_userList[UserListView.SelectedIndex]);
            if (result)
            {
                _userList.RemoveAt(UserListView.SelectedIndex);
            }

        }
    }
}
