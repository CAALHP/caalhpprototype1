﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAALHP.Contracts;
using CAALHP.Events.Types;
using CAALHP.Events.UserServiceEvents;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;
using FS.Events;
using FSAdaptor;
using RequestTypes = FS.Events.RequestTypes;

namespace CsFpDriverWrapper
{
    public class FSDriverImplementation : IDeviceDriverCAALHPContract
    {

        private IDeviceDriverHostCAALHPContract _host;
        private int _processId;
        private readonly string _deviceName;
        private FingerServiceManually _device;

        public FSDriverImplementation()
        {

            //Debugger.Launch();   

            _deviceName = "FSDriver";
            _device = new FingerServiceManually();

        }

        private void HandleEvent(FingerEnrollRequestEvent o)
        {
            bool result;
            string errorMsg = "";
            Event resEvent;
            byte[] template = null;
            
            if (_device.ScannerAvaileble())
            {
                try
                {
                    result = _device.EnrollNewUser(o.UserId, out template);
                }
                catch (Exception e)
                {
                    errorMsg = e.Message;
                    result = false;
                }
            }
            else
            {
                //do some no scanner here.
                result = false;
                errorMsg = "Scanner not available";
            }


            if (result)
            {
                resEvent = new FingerEnrollResponsEvent()
                    {
                        CallerName = GetName(),
                        CallerProcessId = _processId,
                        UserId = o.UserId,
                        Template = template
                    };   
            }
            else
            {
                resEvent = new FingerEnrollErrorResponsEvent()
                    {
                        CallerName = GetName(),
                        CallerProcessId = _processId,
                        Error = errorMsg
                    };
            }

            var serializedResponsEvent = EventHelper.CreateEvent(SerializationType.Json, resEvent, "FS.Events");
            _host.Host.ReportEvent(serializedResponsEvent);
        }


        private void HandleEvent(BiometricsRequestEvent o)
        {

            if (!_device.ScannerAvaileble()) return;

            var resEvent = new BiometricsResponsEvent()
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                Type = BiometricDeviceType.Finger
            };

            var serializedList = EventHelper.CreateEvent(SerializationType.Json, resEvent);
            _host.Host.ReportEvent(serializedList);
        }

        public void HandleEvent(FSRequestEvent requestEvent)
        {
            var reqType = requestEvent.RequestType;
            var respons = new FSResponsEvent
            {
                UserId = requestEvent.UserId,
                CallerName = GetName(),
                CallerProcessId = _processId,
                RequestType = reqType
            };

            if (!_device.ScannerAvaileble())
            {
                respons.RequestType = RequestTypes.CheckforScanner;
                respons.Result = false;
            }

            else
            {
                try
                {
                    switch (reqType)
                    {
                        case RequestTypes.Auth:
                            respons.Result = _device.Verify(requestEvent.UserId);
                            break;
                        case RequestTypes.Enroll:
                            respons.Result = _device.EnrollNewUser(requestEvent.UserId);
                            break;
                        case RequestTypes.CheckforScanner:
                            respons.Result = _device.ScannerAvaileble();
                            break;
                        case RequestTypes.DeleteUser:
                            respons.Result = _device.RemoveUser(requestEvent.UserId);
                            break;
                        case RequestTypes.ClearDb:
                            _device.CleanDb();
                            respons.Result = true;
                            return;
                    }

                }
                catch (Exception e)
                {

                    respons.Message = e.Message;
                    respons.Result = false;
                }
            }

            var serializedResponsEvent = EventHelper.CreateEvent(SerializationType.Json, respons, "FS.Events");

            _host.Host.ReportEvent(serializedResponsEvent);
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            // var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key, "CsFpDriverWrapper");
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }

        public string GetName()
        {
            return _deviceName;
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IDeviceDriverHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;

            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(FSRequestEvent), "FS.Events"), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(FingerEnrollRequestEvent), "FS.Events"), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(BiometricsRequestEvent)), _processId);

            //KeyValuePair<string, string> serializedEvent;

            //if (_device.ScannerAvaileble())
            //{
            //    var initEvent = new InitializedEvent
            //        {
            //            CallerName = GetName(),
            //            CallerProcessId = _processId,
            //            Name = GetName()
            //        };
            //    serializedEvent = EventHelper.CreateEvent(SerializationType.Json, initEvent);
            //}
            //else
            //{
            //    var errorEvent = new ErrorEvent
            //    {
            //        CallerName = GetName(),
            //        CallerProcessId = _processId,
            //        ErrorMessage = "No Scanner"
            //    };

            //    serializedEvent = EventHelper.CreateEvent(SerializationType.Json, errorEvent);

            //}

            //_host.Host.ReportEvent(serializedEvent);
        }



        public double GetMeasurement()
        {
            throw new NotImplementedException();
        }
    }
}
