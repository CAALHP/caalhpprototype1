﻿using System;
using System.AddIn;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugins.DriverPluginAdapters;

namespace CsFpDriverWrapper
{
    // The AddInAttribute identifies this pipeline segment as an add-in.
    [AddIn("Finger Scanner Driver Plugin", Version = "1.0.0.0")]
    public class FSDriver : DeviceDriverViewPluginAdapter
    {

        public FSDriver()
        {
            DeviceDriver = new FSDriverImplementation();
        }
    }
}
