﻿namespace Plugins.LookupService
{
    public interface ILookupStrategy
    {
        string Lookup(string value);
    }
}