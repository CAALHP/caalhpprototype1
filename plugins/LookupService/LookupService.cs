﻿using System.AddIn;
using Plugins.ServicePluginAdapter;

namespace Plugins.LookupService
{
    // The AddInAttribute identifies this pipeline segment as an add-in.
    [AddIn("Lookup Service", Version = "1.0.0.0")]
    public class LookupService : ServiceViewPluginAdapter
    {
        public LookupService()
        {
            Service = new LookupServiceImplementation();
        }
    }
}
