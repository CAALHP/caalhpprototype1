﻿using System;
using System.Collections.Generic;
using System.Linq;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;

namespace Plugins.LookupService
{
    public class LookupServiceImplementation : IServiceCAALHPContract
    {
        private IServiceHostCAALHPContract _host;
        private int _processId;
        private readonly ILookupStrategy _lookup;

        public LookupServiceImplementation()
        {
            _lookup = new DummyLookupStrategy();
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }

        private void HandleEvent(RFIDFoundEvent e)
        {
            var res = _lookup.Lookup(e.Tag);
            if (string.IsNullOrWhiteSpace(res)) return;
            var command = new DeviceFoundEvent()
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                DeviceDriverFileName = res,
                Name = res
            };
            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);
            _host.Host.ReportEvent(serializedCommand);
        }

        public string GetName()
        {
            return "LookupService";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IServiceHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            //_host.Host.SubscribeToEvents(_processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(RFIDFoundEvent)), _processId);
        }

        public void Start()
        {
            //throw new System.NotImplementedException();
        }

        public void Stop()
        {
            //throw new System.NotImplementedException();
        }
    }
}