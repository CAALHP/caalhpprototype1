﻿using CAALHP.Contracts;
using Plugins.AddInViews;
using Plugins.PluginAdapter;

namespace Plugins.DriverPluginAdapters
{
    public class DeviceDriverHostViewToCAALHPContractAdapter : IDeviceDriverHostCAALHPContract
    {
        private IDeviceDriverHostView _hostView;
        public IHostCAALHPContract Host { get; set; }

        public DeviceDriverHostViewToCAALHPContractAdapter(IDeviceDriverHostView host)
        {
            _hostView = host;
            Host = new HostViewToHostCAALHPContractAdapter(_hostView.Host);
        }

    }
}