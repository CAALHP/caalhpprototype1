﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Threading;
using IKriv.Wpf;
using Plugins.TopMenu.MessageTypes;
using Plugins.TopMenu.ViewModel;
using Plugins.TopMenu.Views;
using System.Security.Permissions;

namespace Plugins.TopMenu
{
    public class TopMenuImplementation : IAppCAALHPContract
    {
        private IAppHostCAALHPContract _host;
        private int _processId;

        private Thread _thread;

        //Tell compiler not to optimize the _app variable by declaring it as volatile!
        private volatile Application _app;

        public TopMenuImplementation()
        {
            //Show homescreen as soon as the plugin is started
           // Show();

           // Debugger.Launch();

            InitThread();
            //wait for the _app to be initialized
            //We declare _app as volatile so it won't be cached by optimizations
            //If _app is not volatile the following line could be cached in release mode and never finish.
            while (_app == null) { }
            //_vml = new ViewModelLocator();
            _app.Resources.Add("Locator", new ViewModelLocator());

            //Add datatemplates for subviews
            var manager = new DataTemplateManager(_app);
            manager.RegisterDataTemplate<MainViewModel, MainView>();
            manager.RegisterDataTemplate<TopButtonViewModel, TopButtonView>();
            manager.RegisterDataTemplate<SettingsMenuViewModel, SettingsMenuView>();
            manager.RegisterDataTemplate<SettingsButtonViewModel,SettingsButtonView>();
            manager.RegisterDataTemplate<UserViewModel, UserView>();

            DispatchToApp(() => new MainView().Show());

        }

        public void Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;

            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserLoggedInEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserLoggedOutEvent)), _processId);
            Messenger.Default.Register<ShowHomeMessage>(this, HandlerShowHomeApp);
            Messenger.Default.Register<LogoutMessage>(this, HandlerUserLoggedOut);
        }

        private void HandlerUserLoggedOut(LogoutMessage obj)
        {
            var command = new UserLoggedOutEvent()
            {
                CallerName = "TopMenu",
                CallerProcessId = _processId,
            };

            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);

            _host.Host.ReportEvent(serializedCommand);
        }

        private void HandlerShowHomeApp(ShowHomeMessage obj)
        {
            DoEvents();

            var command = new ShowAppEvent
            {
                CallerName = "TopMenu",
                CallerProcessId = _processId,   
                AppName = "HomeScreen",
            };

            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);

            _host.Host.ReportEvent(serializedCommand);

        }

        private void HandleCaalphEvent(UserLoggedInEvent user)
        {
            Messenger.Default.Send(new GenericMessage<UserLoggedInEvent>(this, user));
        }

        private void HandleCaalphEvent(UserLoggedOutEvent user)
        {
            if(_processId != user.CallerProcessId)
                Messenger.Default.Send(new ExternalLogoutMessage());
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleCaalphEvent(obj);
        }

        public string GetName()
        {
            return "CAALHP TopMenu";
        }

        public bool IsAlive()
        {
            DoEvents();
            return true;
        }

        public void ShutDown()
        {
            //Environment.Exit(0);
            _app.Dispatcher.BeginInvokeShutdown(DispatcherPriority.Normal);
            //_app.Shutdown();
        }

        public void Show()
        {
            var vml = _app.Resources["Locator"] as ViewModelLocator;
            if (vml != null)
            {
                //vml.Marketplace.InstalledApps = _host.GetListOfInstalledApps();
                vml.Main.Show();
            }
            //ref: http://reedcopsey.com/2011/11/28/launching-a-wpf-window-in-a-separate-thread-part-1/
            //Threading is used so we do not block the main thread.
            //var thread = new Thread(() =>
            //{
            //    // Create our context, and install it:
            //    SynchronizationContext.SetSynchronizationContext(
            //        new DispatcherSynchronizationContext(
            //            Dispatcher.CurrentDispatcher));

            //    var window = new MainView { DataContext = new MainViewModel() };
            //    // When the window closes, shut down the dispatcher
            //    window.Closed += (s, e) =>
            //       Dispatcher.CurrentDispatcher.BeginInvokeShutdown(DispatcherPriority.Background);
            //    window.Show();

            //    //var command = new ShowAppEvent
            //    //{
            //    //    CallerName = "TopMenu",
            //    //    CallerProcessId = _processId,
            //    //    AppName = "HomeScreen",
            //    //};
            //    //var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);
            //    //window.TopMenu.HomeButton.MouseUp +=
            //    //    (s, e) => _host.Host.ReportEvent(serializedCommand);
            //    Dispatcher.Run();
            //});

            ////This is needed for GUI threads
            //thread.SetApartmentState(ApartmentState.STA);
            //// Make the thread a background thread
            //thread.IsBackground = true;
            ////Start
            //thread.Start();
        }

        //public void Show()
        //{
        //    //ref: http://reedcopsey.com/2011/11/28/launching-a-wpf-window-in-a-separate-thread-part-1/
        //    //Threading is used so we do not block the main thread.
        //    var vml = _app.Resources["Locator"] as ViewModelLocator;
        //    if (vml != null)
        //    {
        //        //vml.Marketplace.InstalledApps = _host.GetListOfInstalledApps();
        //        //vml.Marketplace.InstalledApps.Clear();
        //        //foreach (var app in _host.GetListOfInstalledApps())
        //        //{
        //        //    vml.Marketplace.InstalledApps.Add(Path.GetFileName(app.LocationDir));
        //        //}
        //        vml.Main.Show();
        //    }

        //}

        private void InitThread()
        {
            _thread = new Thread(() =>
            {
                _app = new Application();
                _app.ShutdownMode = ShutdownMode.OnExplicitShutdown;
                SynchronizationContext.SetSynchronizationContext(
                    new DispatcherSynchronizationContext(
                        Dispatcher.CurrentDispatcher));
                DispatcherHelper.Initialize();
                //var window = new MainView { DataContext = new MainViewModel() };
                //// When the window closes, shut down the dispatcher
                //window.Closed += (s, e) =>
                //   Dispatcher.CurrentDispatcher.BeginInvokeShutdown(DispatcherPriority.Background);
                //window.Show();
                _app.Run();
            });

            //This is needed for GUI threads
            _thread.SetApartmentState(ApartmentState.STA);
            // Make the thread a background thread
            _thread.IsBackground = true;

            _thread.Start();
        }

        private void DispatchToApp(Action action)
        {
            DoEvents();
            _app.Dispatcher.Invoke(action, DispatcherPriority.Send);
            DoEvents();
        }

        public IList<IPluginInfo> GetListOfInstalledApps()
        {
            throw new System.NotImplementedException();
        }

        [SecurityPermissionAttribute(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
        public void DoEvents()
        {
            DispatcherFrame frame = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
                new DispatcherOperationCallback(ExitFrame), frame);
            Dispatcher.PushFrame(frame);
        }

        public object ExitFrame(object f)
        {
            ((DispatcherFrame)f).Continue = false;

            return null;
        }
    }
}