﻿using System.AddIn;
using Plugins.AppPluginAdapters;

namespace Plugins.TopMenu
{
    // The AddInAttribute identifies this pipeline segment as an add-in.
    [AddIn("System TopMenu App", Version = "1.0.0.0")]
    public class TopMenuBoilerplate : AppPluginAdapter
    {
        public TopMenuBoilerplate()
        {
            App = new TopMenuImplementation();
        }
    }
}
