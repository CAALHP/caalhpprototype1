﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Plugins.TopMenu
{
    public static class Utils
    {
        public static string AssemblyDirectory
        {
            get
            {
                var codeBase = Assembly.GetExecutingAssembly().CodeBase;
                var uri = new UriBuilder(codeBase);
                var path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        public static string GetUserImage(string photoLink, string sourcePath)
        {
            var imagePath =  AssemblyDirectory + "\\Images\\";

            if (string.IsNullOrEmpty(photoLink))
                return imagePath + "Default.png";

            try
            {
                var targetImgPath = Path.Combine(imagePath, photoLink);
                var sourceImgPath = Path.Combine(sourcePath, photoLink);

                CopyImagefromSource(sourceImgPath, targetImgPath);

                return targetImgPath; //image = Image.FromFile(targetImgPath);
            }
            catch (Exception)
            {
                return imagePath + "Default.png";
            }
        }

        private static void CopyImagefromSource(string sourceImgPath, string targetImgPath)
        {
            if (!File.Exists(targetImgPath) ||
                File.GetLastWriteTime(targetImgPath) < File.GetLastWriteTime(sourceImgPath))
            {
                File.Copy(sourceImgPath, targetImgPath, true);
            }

        }
    }
}
