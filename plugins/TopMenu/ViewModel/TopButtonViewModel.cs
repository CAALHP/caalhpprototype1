﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Plugins.TopMenu.MessageTypes;

namespace Plugins.TopMenu.ViewModel
{
    public class TopButtonViewModel: BaseButtonViewModel
    {
        public TopButtonViewModel(string iconPath, string backGroundColour, string highLightColour, RelayCommand clickCommand)
            : base(iconPath, backGroundColour, highLightColour, clickCommand)
        {
           // iconPath = "\Images\homeIcon.png";
        }

        //public override void ClickCommandExecute()
        //{
        //    Messenger.Default.Send(new ShowHomeMessage()); 
        //}
    }
}
