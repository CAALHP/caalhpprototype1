﻿using System;
using System.Windows;
using CAALHP.Events;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Plugins.TopMenu.MessageTypes;

namespace Plugins.TopMenu.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private BaseButtonViewModel _homeButton;
        private BaseButtonViewModel _settingsButton;
        private UserViewModel _user;
        private SettingsMenuViewModel _settingsMenu;


        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {

            var homeCommand = new RelayCommand(ShowHomeClickCommandExecute);
            HomeButton = new TopButtonViewModel(Utils.AssemblyDirectory + "\\Images\\homeIcon.png", "#FFFFFFFF", "#FF000000", homeCommand);

            var showSettingsCommand = new RelayCommand(ShowSettingsCommand);
            SettingsButton = new TopButtonViewModel(Utils.AssemblyDirectory + "\\Images\\menuIcon.png", "#FF3DBFD9", "#FF000000", showSettingsCommand);

            SettingsMenu = new SettingsMenuViewModel();

            //HomeButton = new HomeButtonViewModel(@"Images\homeIcon.png", "#FF000000", "#FFFFFFFF");
            Messenger.Default.Register<GenericMessage<UserLoggedInEvent>>(this, HandleUserLoggedInEvent);

            Messenger.Default.Register<LogoutMessage>(this, HandlerUserLoggedOut);
            Messenger.Default.Register<ExternalLogoutMessage>(this, HandlerUserLoggedOut);
        }

        private void HandlerUserLoggedOut(LogoutMessage obj)
        {
            if (IsUserLogged)
                User = null;
        }

        private void HandlerUserLoggedOut(ExternalLogoutMessage obj)
        {
            if (IsUserLogged)
                User = null;
        }

        private void ShowSettingsCommand()
        {
            SettingsMenu.IsSettingsEnabled = !SettingsMenu.IsSettingsEnabled;
        }

        private void HandleUserLoggedInEvent(GenericMessage<UserLoggedInEvent> obj)
        {
            var user = obj.Content.User;
            var sourcePath = obj.Content.PhotoRoot;

            try
            {
                var imgPath = Utils.GetUserImage(user.PhotoLink, sourcePath);

                User = new UserViewModel()
                {
                    UserName = user.FullName,
                    UserLevel = "", //At somepoint something for this would be nice.
                    UserImage = imgPath
                };
                
            }
            catch (Exception e)
            {
                User = null;
                throw new Exception(e.Message);
            }
        }

        public BaseButtonViewModel HomeButton
        {
            get { return _homeButton; }
            set
            {
                _homeButton = value;
                RaisePropertyChanged(() => HomeButton);
            }
        }

        public BaseButtonViewModel SettingsButton
        {
            get { return _settingsButton; }
            set
            {
                _settingsButton = value;
                RaisePropertyChanged(() => SettingsButton);
            }
        }

        public SettingsMenuViewModel SettingsMenu
        {
            get { return _settingsMenu; }
            set
            {
                _settingsMenu = value;
                RaisePropertyChanged(() => SettingsMenu);
            }
        }

        public UserViewModel User
        {
            get { return _user; }
            set
            {
                _user = value;
                RaisePropertyChanged(() => User);
                RaisePropertyChanged(() => IsUserLogged);
            }
        }

        public bool IsUserLogged
        {
            get { return _user != null; }
        }

        public  void ShowHomeClickCommandExecute()
        {
            Messenger.Default.Send(new ShowHomeMessage());
            
        }

        public void Show()
        {
            
        }

    }
}