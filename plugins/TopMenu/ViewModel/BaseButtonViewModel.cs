﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace Plugins.TopMenu.ViewModel
{
    public class BaseButtonViewModel : ViewModelBase
    {
        private string _iconPath;
        private string _backGroundColour;
        private string _highLightColour;
        private string _currentBackgroundColour;

        protected BaseButtonViewModel(string iconPath, string backGroundColour, string highLightColour)
        {
            IconPath = iconPath;
            BackgroundColour = backGroundColour;
            HighLightColour = highLightColour;

            //CurrentBackgroundColour = (Brush)new BrushConverter().ConvertFromString(_backGroundColour);
            //CurrentBackgroundColour = _backGroundColour;

            ClickCommand = new RelayCommand(ClickCommandExecute);

            //   MouseEnterCommand = new RelayCommand(MouseEnterExecute);
            //  MouseLeaveCommand = new RelayCommand(MouseLeaveExecute);
        }

        protected BaseButtonViewModel(string iconPath, string backGroundColour, string highLightColour, RelayCommand clickCommand)
        {
            IconPath = iconPath;
            BackgroundColour = backGroundColour;
            HighLightColour = highLightColour;

            //CurrentBackgroundColour = (Brush)new BrushConverter().ConvertFromString(_backGroundColour);
            //CurrentBackgroundColour = _backGroundColour;

            ClickCommand = clickCommand;

            //   MouseEnterCommand = new RelayCommand(MouseEnterExecute);
            //  MouseLeaveCommand = new RelayCommand(MouseLeaveExecute);
        }

        public string IconPath
        {
            get { return _iconPath; }
            private set
            {
                _iconPath = value;
                RaisePropertyChanged(() => IconPath);
                //RaisePropertyChanged("IconPath");
            }
        }

        public string BackgroundColour
        {
            get
            {
                return _backGroundColour;
            }
            private set
            {
                _backGroundColour = value;
            }
        }

        public string HighLightColour
        {
            get
            {
                return _highLightColour;
            } 
            private set
            {
                _highLightColour = value;
            }
        }


        //public string CurrentBackgroundColour
        //{
        //    get
        //    {
        //        //   MessageBox.Show("Getting background");
        //        return _currentBackgroundColour;
        //    }
        //    set
        //    {
        //        _currentBackgroundColour = value;
        //        RaisePropertyChanged(() => CurrentBackgroundColour);
        //        //RaisePropertyChanged("CurrentBackgroundColour");
        //    }
        //}

        #region Command

        public ICommand ClickCommand { get; private set; }

        public virtual void ClickCommandExecute()
        {
            // nothing will happen unless its overloaded :D    
        }

        #endregion
    }
}
