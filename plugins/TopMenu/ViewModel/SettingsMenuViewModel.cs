﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using CAALHP.Events;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Plugins.TopMenu.MessageTypes;

namespace Plugins.TopMenu.ViewModel
{
    public class SettingsMenuViewModel : ViewModelBase
    {

        private ObservableCollection<SettingsButtonViewModel> _settingsButtonList = new ObservableCollection<SettingsButtonViewModel>();
        private bool _isSettingsEnabled;

        public SettingsMenuViewModel()
        {
            IsSettingsEnabled = false;

            var logoutCommand = new RelayCommand(LogOutCommand);
            var logoutButton = new SettingsButtonViewModel( SettingsButtonType.User, Utils.AssemblyDirectory + "\\Images\\menuIcon.png", "#FF3DBFD9", "#FF000000", logoutCommand)
                {
                    ButtonText = "Sign out"
                };

            SettingsButtonList.Add(logoutButton);

            Messenger.Default.Register<GenericMessage<UserLoggedInEvent>>(this, HandleUserLoggedInEvent);
            Messenger.Default.Register<ExternalLogoutMessage>(this, HandlerExternalUserLoggedOut);
        }

        private void HandlerExternalUserLoggedOut(ExternalLogoutMessage obj)
        {
            EnableUserButtons(false);
        }

        private void HandleUserLoggedInEvent(GenericMessage<UserLoggedInEvent> obj)
        {
            EnableUserButtons(true);
        }


        public bool IsSettingsEnabled
        {
            get { return _isSettingsEnabled; }
            set
            {
                _isSettingsEnabled = value;
                RaisePropertyChanged(() => IsSettingsEnabled);
            }
        }

        public ObservableCollection<SettingsButtonViewModel> SettingsButtonList
        {
            get { return _settingsButtonList; }
            set
            {
                _settingsButtonList = value;
                RaisePropertyChanged(() => SettingsButtonList);
            }
        }

        private void LogOutCommand()
        {

            EnableUserButtons(false);

            Messenger.Default.Send(new LogoutMessage());
            IsSettingsEnabled = false;
        }

        private void EnableUserButtons(bool value)
        {
            foreach (var settingsButtonViewModel in SettingsButtonList.Where(settingsButtonViewModel => settingsButtonViewModel.Type == SettingsButtonType.User))
            {
                settingsButtonViewModel.IsEnabled = value;
            }
        }

    }
}
