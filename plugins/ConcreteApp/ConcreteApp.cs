﻿using System.AddIn;
using Plugins.AppPluginAdapters;

namespace Plugins.ConcreteApp
{
    [AddIn("Concrete App", Version = "1.0.0.0")]
    public class ConcreteAppView : AppViewPluginAdapter
    {
        public ConcreteAppView()
        {
            App = new ConcreteAppImplementation();
        }
    }
}
