﻿using System.Collections.Generic;
using System.Threading;
using CAALHP.Contracts;

namespace Plugins.ConcreteApp
{
    class ConcreteAppImplementation : IAppCAALHPContract
    {
        public void NewMeasurement(double measurement)
        {
            //throw new NotImplementedException();
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            throw new System.NotImplementedException();
        }

        public void Show()
        {
            //throw new NotImplementedException();
            Thread.Sleep(2000);
        }

        public string GetName()
        {
            return "Concrete APP!!!";
        }

        public void Initialize(IHostCAALHPContract hostObj, int processId)
        {
            throw new System.NotImplementedException();
        }
    }
}
