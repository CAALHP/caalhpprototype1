﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using IEEE11073Parser;

namespace Plugins.Contextua11073HubDriver
{
    public class Contextua11073HubDeviceDriverImplementation : IDeviceDriverCAALHPContract
    {
        private IDeviceDriverHostCAALHPContract _host;
        private int _processId;
        private readonly string _server;
        private readonly string _port;
        private readonly IEEE11073ParserFacade _facade;

        public Contextua11073HubDeviceDriverImplementation()
        {
            _server = ConfigurationManager.AppSettings.Get("IEEEServer");
            _port = ConfigurationManager.AppSettings.Get("IEEEPort");
            _facade = new IEEE11073ParserFacade();
        }

        public double GetMeasurement()
        {
            //var rnd = new System.Random();
            return 0.0;
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            //throw new System.NotImplementedException();
        }

        public string GetName()
        {
            return "Contextua11073Hub Driver";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IDeviceDriverHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            var initEvent = new InitializingEvent
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                Name = GetName()
            };
            var preInit = EventHelper.CreateEvent(SerializationType.Json, initEvent);
            //_host.Host.ReportEvent(EventHelper.CreateEvent(_processId, GetName(), "Initializing " + GetName()));
            _host.Host.ReportEvent(preInit);
            _facade.NewIEEE11073Measurement += _facade_NewIEEE11073Measurement;
            //TODO THIS LINE WILL CAUSE MASSIVE CPU USAGE IF NO HUB IS FOUND ON NETWORK, its a issue in the implementation found in ieee11073Parser.dll
            _facade.StartIEEE11073Parser(new[] { _server, _port });
            var initializedEvent = new InitializedEvent()
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                Name = GetName()
            };
            var postInit = EventHelper.CreateEvent(SerializationType.Json, initializedEvent);
            //_host.Host.ReportEvent(EventHelper.CreateEvent(_processId, GetName(), "Started IEEE11073Parser using " + _server + ":" + _port));
            _host.Host.ReportEvent(postInit);
        }

        private void _facade_NewIEEE11073Measurement(object sender, NewIEEE11073MeasurementArgs e)
        {
            ParseAndReportResult(e.data);
        }

        private void ParseAndReportResult(BasicParseResult data)
        {
            switch (data.Type)
            {
                case ParseResultType.BloodPressure:
                    {
                        var res = data as BloodPressureParseResult;
                        if (res != null)
                        {
                            var theEvent = new BloodPressureMeasurementEvent
                            {
                                CallerName = GetName(),
                                CallerProcessId = _processId,
                                Diastolic = res.DiastolicValue,
                                Systolic = res.SystolicValue
                            };
                            var measurementEvent = EventHelper.CreateEvent(SerializationType.Json, theEvent);
                            _host.Host.ReportEvent(measurementEvent);
                        }
                    }
                    break;
                default:
                    {
                        var theEvent = new SimpleMeasurementEvent
                        {
                            CallerName = GetName(),
                            CallerProcessId = _processId,
                            MeasurementType = Enum.GetName(typeof (ParseResultType), data.Type),
                            Value = data.Value
                        };
                        var measurementEvent = EventHelper.CreateEvent(SerializationType.Json, theEvent);
                        _host.Host.ReportEvent(measurementEvent);
                    }
                    break;
            }
        }
    }
}