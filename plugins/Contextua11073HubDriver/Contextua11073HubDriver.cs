﻿using System.AddIn;
using Plugins.DriverPluginAdapters;

namespace Plugins.Contextua11073HubDriver
{
    // The AddInAttribute identifies this pipeline segment as an add-in.
    [AddIn("Contextua11073Hub Driver Plugin", Version = "1.0.0.0")]
    public class Contextua11073HubDeviceDriver : DeviceDriverViewPluginAdapter
    {
        public Contextua11073HubDeviceDriver()
        {
            DeviceDriver = new Contextua11073HubDeviceDriverImplementation();
            
        }
    }
}