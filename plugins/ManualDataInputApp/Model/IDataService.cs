﻿using System.Collections.Generic;
using N4CLibrary;

namespace Plugins.ManualDataInputApp.Model
{
    public interface IDataService
    {
        IList<HomeBloodPressureObservation> GetHomeBloodPressures();
        void UploadHomeBloodPressure(HomeBloodPressureObservation homeBloodPressureObservation);
    }
}
