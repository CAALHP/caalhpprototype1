﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Threading;
using IKriv.Wpf;
using Plugins.Marketplace.ViewModel;
using Plugins.Marketplace.Views;

namespace Plugins.Marketplace
{
    public class MarketplaceImplementation : IAppCAALHPContract
    {
        private IAppHostCAALHPContract _host;
        private int _processId;
        private Thread _thread;
        //private MainView _view;

        //Tell compiler not to optimize the _app variable by declaring it as volatile!
        private volatile Application _app;

        public MarketplaceImplementation()
        {
            InitThread();
            //wait for the _app to be initialized
            //We declare _app as volatile so it won't be cached by optimizations
            //If _app is not volatile the following line could be cached in release mode and never finish.
            while (_app == null) { }
            //_vml = new ViewModelLocator();
            _app.Resources.Add("Locator", new ViewModelLocator());

            //Add datatemplates for subviews
            var manager = new DataTemplateManager(_app);
            manager.RegisterDataTemplate<MarketplaceViewModel, MarketplaceView>();
            manager.RegisterDataTemplate<AppItemViewModel, AppItemView>();

            DispatchToApp(() => new MarketplaceView().Hide());

            Messenger.Default.Register<NotificationMessage>(this, SendDownloadRequest);
        }

        private void SendDownloadRequest(NotificationMessage message)
        {
            if (!message.Target.Equals(GetType())) return;

            var downloadAppEvent = new DownloadAppEvent
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                FileName = message.Notification
            };
            var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, downloadAppEvent);
            _host.Host.ReportEvent(serializedEvent);
                
            //var command = EventHelper.CreateEvent(_processId, GetName(), "DownloadApp", message.Notification);
            //_host.Host.ReportEvent(command);
        }


        public void Notify(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }

        private void HandleEvent(ShowAppEvent showAppEvent)
        {
            if(showAppEvent.AppName.Equals(GetName()))
                Show();
        }

        private void HandleEvent(DownloadAppCompletedEvent downloadAppCompletedEvent)
        {
            Messenger.Default.Send(new NotificationMessage(this, typeof(AppItemViewModel), downloadAppCompletedEvent.FileName + ";" + "DownloadCompleted"));
        }

        private void HandleEvent(DownloadProgressEvent downloadProgressEvent)
        {
            var progress = downloadProgressEvent.PercentDone;
            var fileName = downloadProgressEvent.FileName;
            Messenger.Default.Send(new NotificationMessage(this, typeof(AppItemViewModel), "DownloadProgress" + ";" + fileName + ";" + progress));
        }

        private void HandleEvent(InstallAppCompletedEvent installAppCompletedEvent)
        {
            Messenger.Default.Send(new NotificationMessage(this, typeof(AppItemViewModel), installAppCompletedEvent.FileName + ";" + "InstallCompleted"));
        }

        public string GetName()
        {
            return "Marketplace";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            _app.Dispatcher.BeginInvokeShutdown(DispatcherPriority.Normal);
        }

        public void Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            //Subscribe to events so we can react to "Home" messages
            //_host.Host.SubscribeToEvents(_processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(ShowAppEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(DownloadAppCompletedEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(DownloadProgressEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(InstallAppCompletedEvent)), _processId);
        }

        public void Show()
        {
            //ref: http://reedcopsey.com/2011/11/28/launching-a-wpf-window-in-a-separate-thread-part-1/
            //Threading is used so we do not block the main thread.
            var vml = _app.Resources["Locator"] as ViewModelLocator;
            if (vml != null)
            {
                //vml.Marketplace.InstalledApps = _host.GetListOfInstalledApps();
                vml.Marketplace.InstalledApps.Clear();
                foreach (var app in _host.GetListOfInstalledApps())
                {
                    vml.Marketplace.InstalledApps.Add(Path.GetFileName(app.LocationDir));
                }
                vml.Marketplace.Show();
            }
        }

        /*public IList<IPluginInfo> GetListOfInstalledApps()
        {
            throw new NotImplementedException();
        }*/

        private void InitThread()
        {
            _thread = new Thread(() =>
            {
                _app = new Application();
                _app.ShutdownMode = ShutdownMode.OnExplicitShutdown;
                /*SynchronizationContext.SetSynchronizationContext(
                    new DispatcherSynchronizationContext(
                        Dispatcher.CurrentDispatcher));*/
                DispatcherHelper.Initialize();
                _app.Run();
            });

            //This is needed for GUI threads
            _thread.SetApartmentState(ApartmentState.STA);
            // Make the thread a background thread
            //_thread.IsBackground = true;

            _thread.Start();
        }

        private void DispatchToApp(Action action)
        {
            _app.Dispatcher.Invoke(action);
        }

    }
}