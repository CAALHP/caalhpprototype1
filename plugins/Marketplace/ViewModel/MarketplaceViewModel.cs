﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Threading;
using Plugins.Marketplace.Model;
using Plugins.Marketplace.Views;

namespace Plugins.Marketplace.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MarketplaceViewModel : ViewModelBase
    {
        private IDataService _dataService;
        
        private ObservableCollection<AppItemViewModel> _appItems;
        private string _loading;

        public ObservableCollection<AppItemViewModel> AppItems
        {
            get{ return _appItems; }
            set
            {
                _appItems = value;
                RaisePropertyChanged("AppItems");
            }
        }

        public string Loading
        {
            get { return _loading; }
            set
            {
                _loading = value; 
                RaisePropertyChanged("Loading");
            }
        }

        public ObservableCollection<string> InstalledApps { get; set; }

        /// <summary>
        /// Initializes a new instance of the MarketplaceViewModel class.
        /// </summary>
        public MarketplaceViewModel(IDataService dataService)
        {
            _dataService = dataService;
            AppItems = new ObservableCollection<AppItemViewModel>();
            InstalledApps = new ObservableCollection<string>();
        }

        private async void LoadAppNames()
        {
            Loading = "Loading...";
            var appNames = await _dataService.LoadAppListAsync(CancellationToken.None);
            var items = new ObservableCollection<AppItemViewModel>();

            foreach (var item in appNames.Select(name => new AppItemViewModel { AppName = name, IsInstalled = InstalledApps.Contains(name.Replace(".zip","")) }))
            {
                items.Add(item);
            }
            DispatcherHelper.CheckBeginInvokeOnUI(() => { AppItems = items; });
            Loading = "";
        }
 
        public void Show()
        {
            LoadAppNames();
            Messenger.Default.Send(new NotificationMessage(this,typeof(MarketplaceView),"Show"));
        }
    }
}