﻿using System;
using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace Plugins.Marketplace.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class AppItemViewModel : ViewModelBase
    {
        private string _appName;
        private bool _isDownloading;
        private bool _isInstalling;
        private bool _isInstalled;
        private int _downloadProgress;

        public bool IsDownloading
        {
            get { return _isDownloading; }
            private set
            {
                _isDownloading = value;
                RaisePropertyChanged("IsDownloading");
            }
        }

        public bool IsInstalling
        {
            get { return _isInstalling; }
            private set
            {
                _isInstalling = value;
                RaisePropertyChanged("IsInstalling");
            }
        }

        public bool IsInstalled
        {
            get { return _isInstalled; }
            set
            {
                _isInstalled = value; 
                RaisePropertyChanged("IsInstalled");
            }
        }

        public string AppName
        {
            get { return _appName.Replace("Plugins.", "").Replace(".zip", ""); }
            set
            {
                _appName = value;
                RaisePropertyChanged("AppName");
            }
        }

        public int DownloadProgress
        {
            get { return _downloadProgress; }
            private set
            {
                _downloadProgress = value; 
                RaisePropertyChanged("DownloadProgress");
            }
        }

        /// <summary>
        /// Initializes a new instance of the AppItemViewModel class.
        /// </summary>
        public AppItemViewModel()
        {
            _appName = "";
            IsDownloading = false;
            IsInstalling = false;
            IsInstalled = false;
            ClickCommand = new RelayCommand(ClickCommand_Execute);
            Messenger.Default.Register<NotificationMessage>(this, NotificationReceived);
        }

        private void NotificationReceived(NotificationMessage message)
        {
            if (!message.Target.Equals(GetType())) return;
            var splitmessage = message.Notification.Split(';');
            if (!splitmessage.Contains(_appName)) return;
            if (splitmessage.Contains("DownloadCompleted"))
            {
                IsDownloading = false;
                IsInstalling = true;
            }
            else if (splitmessage.Contains("DownloadProgress") && splitmessage.Count() > 1 )
            {
                int progress;
                if(int.TryParse(splitmessage.Last(), out progress))
                {
                    DownloadProgress = progress;
                }
            }
            else if (splitmessage.Contains("InstallCompleted"))
            {
                IsInstalling = false;
                IsInstalled = true;
            }
        }

        #region ClickCommand

        public ICommand ClickCommand { get; private set; }

        private void ClickCommand_Execute()
        {
            Messenger.Default.Send(new NotificationMessage(this, typeof(MarketplaceImplementation), _appName));
            IsDownloading = true;
        }

        #endregion
    }
}