﻿using System.AddIn;
using Plugins.AppPluginAdapters;

namespace Plugins.Marketplace
{
    // The AddInAttribute identifies this pipeline segment as an add-in.
    [AddIn("Marketplace", Version = "1.0.0.0")]
    public class MarketplaceBoilerplate : AppPluginAdapter
    {
        public MarketplaceBoilerplate()
        {
            App = new MarketplaceImplementation();
        }
    }
}
