﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Configuration;
using System.Threading;
using System.Threading.Tasks;
using CareStoreServiceContracts;

namespace Plugins.Marketplace.Model
{
    public class DataService : IDataService
    {
        private ICareStoreMarketplaceContract _market;

        public DataService()
        {
            //binding
            var binding = new BasicHttpsBinding();
            //endpoint
            var endpoint = new EndpointAddress(ConfigurationManager.AppSettings.Get("marketplaceAddress"));
            //channelfactory
            var channelFactory = new ChannelFactory<ICareStoreMarketplaceContract>(binding, endpoint);
            System.Net.ServicePointManager.ServerCertificateValidationCallback +=
                (sender, certificate, chain, sslPolicyErrors) => true;
            _market = channelFactory.CreateChannel();
        }

        public IList<string> GetAppList()
        {
            var appNames = _market.GetAppLinks();
            return appNames;
        }

        public void GetData(Action<IList<string>, Exception> callback)
        {
            var appNames = _market.GetAppLinks();
            callback(appNames, null);
        }

        public Task<IList<string>> LoadAppList(CancellationToken cancellationToken)
        {
            return LoadAppListAsync(cancellationToken);
        }

        public async Task<IList<string>> LoadAppListAsync(CancellationToken cancellationToken)
        {
            return await Task.Run(() => _market.GetAppLinks(), cancellationToken);
        }
    }
}