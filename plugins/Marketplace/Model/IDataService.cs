﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Plugins.Marketplace.Model
{
    public interface IDataService
    {
        IList<string> GetAppList();
        void GetData(Action<IList<string>, Exception> callback);
        Task<IList<string>> LoadAppList(CancellationToken cancellationToken);
        Task<IList<string>> LoadAppListAsync(CancellationToken cancellationToken);
    }
}

