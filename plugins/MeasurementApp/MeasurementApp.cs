﻿using System.AddIn;
using Plugins.AppPluginAdapters;

namespace Plugins.MeasurementApp
{
    // The AddInAttribute identifies this pipeline segment as an add-in.
    [AddIn("Measurement App Plugin", Version = "1.0.0.0")]
    public class MeasurementAppView : AppViewPluginAdapter
    {
        public MeasurementAppView()
        {
            App = new MeasurementAppImplementation();
        }
    }
}
