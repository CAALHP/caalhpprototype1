﻿using System;
using System.Collections;
using System.Collections.Generic;
using CAALHP.Contracts;
using CAALHP.Library.Hosts;

namespace Plugins.MeasurementApp
{
    public class MeasurementAppImplementation : IAppCAALHPContract
    {
        private readonly List<double> _measurementList = new List<double>();
        private IHostCAALHPContract _host;
        private int _processId;

        public void Notify(KeyValuePair<string, string> notification)
        {
            var splitList = notification.Value.Split(';');
            var measurement = double.Parse(splitList[splitList.Length - 1]);
            _measurementList.Add(measurement);
        }

        public void Show()
        {
            Console.Clear();
            Console.WriteLine("###  Glorious Measurement App  ###");
            Console.WriteLine();
            Console.WriteLine("Recorded Measurements:");
            Console.WriteLine("---------------------------------");
            var index = 0;
            foreach (var measurement in _measurementList)
            {
                Console.WriteLine("[" + index++ + "] " + measurement);
            }
            Console.WriteLine("---------------------------------");
        }

        public IList<IAppInfo> GetListOfInstalledApps()
        {
            throw new NotImplementedException();
        }

        public string GetName()
        {
            return "Measurement App Plugin";
        }

        public void Initialize(IHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            _host.SubscribeToEvents(_processId);
        }
    }
}
