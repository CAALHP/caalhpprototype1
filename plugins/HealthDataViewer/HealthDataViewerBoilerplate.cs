﻿using System.AddIn;
using Plugins.AppPluginAdapters;

namespace Plugins.HealthDataViewer
{
    // The AddInAttribute identifies this pipeline segment as an add-in.
    [AddIn("HealthDataViewer App", Version = "1.0.0.0")]
    public class HealthDataViewerBoilerplate : AppPluginAdapter
    {
        public HealthDataViewerBoilerplate()
        {
            App = new HealthDataViewerImplementation();
        }
    }
}
