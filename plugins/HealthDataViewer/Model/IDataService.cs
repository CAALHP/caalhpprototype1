﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Net4Care.Observation;

namespace Plugins.HealthDataViewer.Model
{
    public interface IDataService
    {
        Task<IList<StandardTeleObservation>> GetHomeBloodPressures();
        Task<IList<StandardTeleObservation>> GetWeights();
        Task UploadHomeBloodPressure(StandardTeleObservation homeBloodPressure);
        Task<IList<StandardTeleObservation>> GetSaturations();
        Task<IList<StandardTeleObservation>> GetGlucoses();
        //void InitCollections();
    }
}
