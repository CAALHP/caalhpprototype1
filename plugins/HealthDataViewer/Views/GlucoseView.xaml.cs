﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Plugins.HealthDataViewer.Views
{
    /// <summary>
    /// Description for WeightView.
    /// </summary>
    public partial class GlucoseView
    {
        /// <summary>
        /// Initializes a new instance of the WeightView class.
        /// </summary>
        public GlucoseView()
        {
            InitializeComponent();

            //oxyplot won't update by itself unless we do this:
            var dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += DispatcherTimerOnTick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 50);
            dispatcherTimer.Start();
        }

        private void DispatcherTimerOnTick(object sender, EventArgs eventArgs)
        {
            Plot.InvalidatePlot();
        }

        private void GlucoseDataGrid_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            GlucoseDataGrid.UnselectAllCells();
        }

        private void ScrollUpButton_Click(object sender, RoutedEventArgs e)
        {
            var row = UiHelper.GetControl<DataGridRow>(GlucoseDataGrid);
            var scroller = UiHelper.GetControl<ScrollViewer>(GlucoseDataGrid);
            scroller.ScrollToVerticalOffset(scroller.VerticalOffset - row.ActualHeight);
        }

        private void ScrollDownButton_Click(object sender, RoutedEventArgs e)
        {
            var row = UiHelper.GetControl<DataGridRow>(GlucoseDataGrid);
            var scroller = UiHelper.GetControl<ScrollViewer>(GlucoseDataGrid);
            scroller.ScrollToVerticalOffset(scroller.VerticalOffset + row.ActualHeight);
        }
    }
}