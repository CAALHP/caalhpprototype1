using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Plugins.HealthDataViewer.Views
{
    public static class UiHelper
    {
        /// Method to get scrollbar in a visual object
        public static ScrollViewer GetScrollbar(DependencyObject dep)
        {
            for (var i = 0; i < VisualTreeHelper.GetChildrenCount(dep); i++)
            {
                var child = VisualTreeHelper.GetChild(dep, i);
                if (child != null && child is ScrollViewer)
                    return child as ScrollViewer;
                var sub = GetScrollbar(child);
                if (sub != null)
                    return sub;
            }
            return null;
        }

        public static DataGridRow GetDataGridRow(DependencyObject dep)
        {
            for (var i = 0; i < VisualTreeHelper.GetChildrenCount(dep); i++)
            {
                var child = VisualTreeHelper.GetChild(dep, i);
                if (child != null && child is DataGridRow)
                    return child as DataGridRow;
                var sub = GetDataGridRow(child);
                if (sub != null)
                    return sub;
            }
            return null;
        }

        public static T GetControl<T>(DependencyObject dep) where T : class
        {
            for (var i = 0; i < VisualTreeHelper.GetChildrenCount(dep); i++)
            {
                var child = VisualTreeHelper.GetChild(dep, i);
                if (child != null && child is T)
                    return child as T;
                var sub = GetControl<T>(child);
                if (sub != null)
                    return sub;
            }
            return null;
        }

    }
}