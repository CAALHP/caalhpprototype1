using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Threading;
using CAALHP.Events.Types;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Plugins.HealthDataViewer.Model;
using Plugins.HealthDataViewer.State;
using Plugins.HealthDataViewer.ViewModel.Data;

namespace Plugins.HealthDataViewer.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {

        private BaseGridViewModel _currentViewModel;

        private readonly BloodPressureGridViewModel _bloodPressureGridViewModel;
        private readonly WeightGridViewModel _weightGridViewModel;
        private readonly SaturationGridViewModel _saturationGridViewModel;
        private readonly GlucoseGridViewModel _glucoseGridViewModel;
        private readonly IDataService _dataService;
        private List<UserProfile> _userList;
        private UserProfile _selectedUser;
        private bool _userListVisible;

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(IDataService dataService)
        {
            _dataService = dataService;
            ////if (IsInDesignMode)
            ////{
            ////    // Code runs in Blend --> create design time data.
            ////}
            ////else
            ////{
            ////    // Code runs "for real"
            ////}
            //var dataService = new DataService();
            _bloodPressureGridViewModel = new BloodPressureGridViewModel(_dataService);
            _weightGridViewModel = new WeightGridViewModel(_dataService);
            _saturationGridViewModel = new SaturationGridViewModel(_dataService);
            _glucoseGridViewModel = new GlucoseGridViewModel(_dataService);
            CurrentViewModel = _bloodPressureGridViewModel;

            BloodPressureCommand = new RelayCommand(BloodPressureCommand_Execute);
            WeightCommand = new RelayCommand(WeightCommand_Execute);
            SaturationCommand = new RelayCommand(SaturationCommand_Execute);
            GlucoseCommand = new RelayCommand(GlucoseCommand_Execute);

            YearCommand = new RelayCommand(YearCommand_Execute);
            MonthCommand = new RelayCommand(MonthCommand_Execute);
            WeekCommand = new RelayCommand(WeekCommand_Execute);
            //InitData();

            State = new UserLoggedOutState(this);

            UserListVisible = false;
        }

        public void Show()
        {
            Messenger.Default.Send(new NotificationMessage("Show"));
            //State.Show();

            Application.Current.MainWindow.WindowState = WindowState.Minimized;
            Application.Current.MainWindow.WindowState = WindowState.Maximized;
        }

        public List<UserProfile> UserList
        {
            get { return _userList; }
            set
            {
                if (_userList == value)return;

                _userList = value;
                RaisePropertyChanged(() => UserList);
            }
        }

        public UserProfile SelectedUser
        {
            get { return _selectedUser; }
            set {
                if (_selectedUser == value) return;

                _selectedUser = value;
                RaisePropertyChanged(() => SelectedUser);

                if (_selectedUser != null)
                    Utility.Cpr = _selectedUser.UserId;
            }
        }

        public bool UserListVisible
        {
            get { return _userListVisible; }
            set
            {
                if (_userListVisible == value) return;
                _userListVisible = value;
                RaisePropertyChanged(() => UserListVisible);
            }
        }

        //public void UserLogIn()
        //{
        //    State.UserLoggedIn();
        //}

        //public void UserLogOut()
        //{
        //    State.UserLoggedOut();
        //}

        /// <summary>
        /// The CurrentView property.  The setter is private since only this 
        /// class can change the view via a command.  If the View is changed,
        /// we need to raise a property changed event (via INPC).
        /// </summary>
        public BaseGridViewModel CurrentViewModel
        {
            get
            {
                return _currentViewModel;
            }
            set
            {
                if (_currentViewModel == value)
                    return;
                _currentViewModel = value;
                RaisePropertyChanged("CurrentViewModel");
            }
        }

        public IHealthDateViewState State { get; set; }

        /// <summary>
        /// Simple property to hold the 'HealthDataViewCommand' - when executed
        /// it will change the current view to the 'HealthDataView'
        /// </summary>
        public RelayCommand BloodPressureCommand { get; private set; }
        public RelayCommand GlucoseCommand { get; private set; }
        public RelayCommand SaturationCommand { get; private set; }
        public RelayCommand WeightCommand { get; private set; }

        public RelayCommand YearCommand { get; private set; }
        public RelayCommand MonthCommand { get; private set; }
        public RelayCommand WeekCommand { get; private set; }

        private void BloodPressureCommand_Execute()
        {
            CurrentViewModel = _bloodPressureGridViewModel;
        }

        private void GlucoseCommand_Execute()
        {
            CurrentViewModel = _glucoseGridViewModel;
        }

        private void SaturationCommand_Execute()
        {
            CurrentViewModel = _saturationGridViewModel;
        }

        private void WeightCommand_Execute()
        {
            CurrentViewModel = _weightGridViewModel;
        }

        private void YearCommand_Execute()
        {
            CurrentViewModel.SetYear();
        }

        private void MonthCommand_Execute()
        {
            CurrentViewModel.SetMonth();
        }

        private void WeekCommand_Execute()
        {
            CurrentViewModel.SetWeek();
        }
    }
}