﻿using System;
using GalaSoft.MvvmLight;
using N4CLibrary;
using Net4Care.Observation;

namespace Plugins.HealthDataViewer.ViewModel.Data
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class SaturationViewModel : ViewModelBase, IEquatable<SaturationViewModel>
    {
        private readonly StandardTeleObservation _sto;
        private readonly SaturationObservation _saturation;

        /// <summary>
        /// Initializes a new instance of the Saturation class.
        /// </summary>
        public SaturationViewModel(StandardTeleObservation sto)
        {
            _sto = sto;
            _saturation = _sto.ObservationSpecifics as SaturationObservation;
        }

        public string Time
        {
            get
            {
                return TimeConverter.GetTime(_sto.Time).ToString();
            }
        }

        public DateTime TimeValue
        {
            get
            {
                return TimeConverter.GetTime(_sto.Time);
            }
        }

        public string Saturation
        {
            get
            {
                return _saturation != null ? _saturation.Saturation.Value + _saturation.Saturation.Unit : null;
            }
        }

        public double SaturationValue
        {
            get
            {
                return _saturation.Saturation.Value;
            }
        }

        public bool Equals(SaturationViewModel other)
        {
            return Saturation == other.Saturation && Time == other.Time;
        }
    }
}