﻿using System;
using GalaSoft.MvvmLight;
using N4CLibrary;
using Net4Care.Observation;

namespace Plugins.HealthDataViewer.ViewModel.Data
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class HomeBloodPressureViewModel : ViewModelBase, IEquatable<HomeBloodPressureViewModel>
    {
        private readonly HomeBloodPressureObservation _homeBloodPressureObservation;
        private readonly StandardTeleObservation _sto;

        public string Systolic
        {
            get
            {
                if (_homeBloodPressureObservation.Systolic != null)
                    return _homeBloodPressureObservation.Systolic.Value + " " + _homeBloodPressureObservation.Systolic.Unit;
                return "";
            }
        }

        public double SystolicValue
        {
            get
            {
                return _homeBloodPressureObservation.Systolic != null ? _homeBloodPressureObservation.Systolic.Value : 0;
            }
        }

        public string Diastolic
        {
            get
            {
                if (_homeBloodPressureObservation.Diastolic != null)
                    return _homeBloodPressureObservation.Diastolic.Value + " " + _homeBloodPressureObservation.Diastolic.Unit;
                return "";
            }
        }

        public double DiastolicValue
        {
            get
            {
                return _homeBloodPressureObservation.Diastolic != null ? _homeBloodPressureObservation.Diastolic.Value : 0;
            }
        }

        public string Pulse
        {
            get
            {
                if (_homeBloodPressureObservation.Pulse != null)
                    return _homeBloodPressureObservation.Pulse.Value + " /min";
                return "";
            }
        }

        public string NoiseDetected
        {
            get
            {
                return _homeBloodPressureObservation.NoiseDetectedDuringMeasurement ? "Yes" : "No";
            }
        }

        public string TimeSeated
        {
            get
            {
                if (_homeBloodPressureObservation.TimeSeated != null)
                    return _homeBloodPressureObservation.TimeSeated.Value + " " + _homeBloodPressureObservation.TimeSeated.Unit;
                return "";
            }
        }

        public string Time
        {
            get
            {
                return TimeConverter.GetTime(_sto.Time).ToString();
                //return _sto.Time.ToString();
            }
        }

        public DateTime TimeValue
        {
            get { return TimeConverter.GetTime(_sto.Time); }
        }


        /// <summary>
        /// Initializes a new instance of the HomeBloodPressureViewModel class.
        /// </summary>
        /*public HomeBloodPressureViewModel(HomeBloodPressure hbp)
        {
            _homeBloodPressure = hbp;
        }*/

        public HomeBloodPressureViewModel(StandardTeleObservation sto)
        {
            _sto = sto;
            _homeBloodPressureObservation = _sto.ObservationSpecifics as HomeBloodPressureObservation;
        }

        public bool Equals(HomeBloodPressureViewModel other)
        {
            return Time == other.Time
                   && Diastolic == other.Diastolic
                   && NoiseDetected == other.NoiseDetected
                   && Pulse == other.Pulse
                   && Systolic == other.Systolic
                   && TimeSeated == other.TimeSeated;
        }
    }
}