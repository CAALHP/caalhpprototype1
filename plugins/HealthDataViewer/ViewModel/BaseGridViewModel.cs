﻿using System;
using System.Configuration;
using System.Security.Cryptography.X509Certificates;
using GalaSoft.MvvmLight;

namespace Plugins.HealthDataViewer.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public abstract class BaseGridViewModel : ViewModelBase
    {
        private double _daysToShow;

        protected double DaysToShow
        {
            get { return _daysToShow; }
            set
            {
                _daysToShow = value;
                DaysToShowUpdated();
            }
        }

        protected abstract void DaysToShowUpdated();

        /// <summary>
        /// Initializes a new instance of the BaseGridViewModel class.
        /// </summary>
        protected BaseGridViewModel()
        {
            double daysToShow;
            DaysToShow = Double.TryParse(ConfigurationManager.AppSettings.Get("standardDaysTimeSpan"), out daysToShow) ? daysToShow : 7;
        }

        public void SetYear()
        {
            DaysToShow = 365;
        }

        public void SetMonth()
        {
            DaysToShow = 30;
        }

        public void SetWeek()
        {
            DaysToShow = 7;
        }
    }
}