﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using Plugins.HealthDataViewer.Model;
using Plugins.HealthDataViewer.ViewModel.Data;

namespace Plugins.HealthDataViewer.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class WeightGridViewModel : BaseGridViewModel
    {
        private readonly IDataService _dataService;
        private DispatcherTimer _updateDataTimer;
        private ObservableCollection<WeightViewModel> _weights;
        private ScatterSeries _series;
        private PlotModel _weightPlotModel;
        private ICollectionView _weightsView;

        public ICollectionView WeightsView
        {
            get { return _weightsView; }
            private set
            {
                _weightsView = value;
                RaisePropertyChanged("WeightsView");
            }
        }

        public ObservableCollection<WeightViewModel> Weights
        {
            get { return _weights; }
            private set
            {
                _weights = value;
                RaisePropertyChanged("Weights");
            }
        }

        public PlotModel WeightPlotModel
        {
            get { return _weightPlotModel; }
            private set
            {
                _weightPlotModel = value;
                RaisePropertyChanged("WeightPlotModel");
            }
        }

        /// <summary>
        /// Initializes a new instance of the WeightViewModel class.
        /// </summary>
        /// <param name="dataService"></param>
        public WeightGridViewModel(IDataService dataService)
        {
            _dataService = dataService;
            Weights = new ObservableCollection<WeightViewModel>();
            WeightsView = CollectionViewSource.GetDefaultView(Weights);
            WeightsView.SortDescriptions.Add(new SortDescription("TimeValue", ListSortDirection.Descending));
            LoadDataCommand = new RelayCommand(LoadDataCommand_Execute);
            LoadDataCommand_Execute();
            SetupSeries();
            SetupPlot();
            SetupTimer();
        }

        private void SetupSeries()
        {
            _series = new ScatterSeries { MarkerType = MarkerType.Circle, MarkerSize = 3, MarkerStrokeThickness = 1.0, MarkerStroke = OxyColors.Black, MarkerFill = OxyColors.LawnGreen};
        }

        private void SetupPlot()
        {
            var temp = new PlotModel("Weight Chart");
            //var ls = new ScatterSeries();
            //temp.Series.Add(ls);
            var dateAxis = new DateTimeAxis
            {
                Minimum = DateTimeAxis.ToDouble(DateTime.Now.Subtract(TimeSpan.FromDays(DaysToShow))),
                Maximum = DateTimeAxis.ToDouble(DateTime.Now)
            };
            temp.Axes.Add(dateAxis);
            var axis = new LinearAxis
            {
               // Minimum = 0,
               // Maximum = 100,
                //TextColor = OxyColors.Gold
                //Palette = 
                ShowMinorTicks = false,
            };

            temp.Axes.Add(axis);
            WeightPlotModel = temp;
        }

        private void SetupTimer()
        {
            _updateDataTimer = new DispatcherTimer();
            _updateDataTimer.Tick += UpdateDataTimerOnTick;
            _updateDataTimer.Interval = new TimeSpan(0, 0, 10);
            _updateDataTimer.Start();
        }

        private void UpdateDataTimerOnTick(object sender, EventArgs eventArgs)
        {
            if (string.IsNullOrEmpty(Utility.Cpr))  
                Weights.Clear();
            else
                LoadDataCommand.Execute(this);
        }

        private void LoadDataCommand_Execute()
        {
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background, new Action(async () =>
            {
                try
                {
                   
                    //removes old data from last patient.
                    if (Weights.Count > 0 && Weights[0].PatientId != Utility.Cpr)
                    {
                        WeightPlotModel.Series.Clear();
                        Weights.Clear();
                    }
                        

                    var weights = await _dataService.GetWeights();

                    if (weights.Count == 0) return;

                    foreach (var weight in weights)
                    {
                        var newWeight = new WeightViewModel(weight);
                        if (!Weights.Contains(newWeight))
                        {
                            Weights.Add(newWeight);
                            UpdatePlotModel(newWeight);
                        }
                    }

                    var max = Weights.Max( x => x.WeightValue);
                    var min = Weights.Min(x => x.WeightValue);

                    UpdateAxis(min, max);
                    UpdateFilter();
                    
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }));
        }

        private void UpdatePlotModel(WeightViewModel weight)
        {
            SetupPlot();
                        
            //var ls = SaturationPlotModel.Series[0] as ScatterSeries;
            if (_series == null) return;

            _series.Points.Add(new ScatterPoint(DateTimeAxis.ToDouble(weight.TimeValue), weight.WeightValue));

            WeightPlotModel.Series.Add(_series);

            //SaturationPlotModel = temp;
            RaisePropertyChanged("WeightPlotModel");
        }

        public RelayCommand LoadDataCommand { get; set; }

        private void UpdateAxis()
        {
            if (WeightPlotModel != null)
            {
                if (WeightPlotModel.Axes != null)
                {
                    var dateAxis = WeightPlotModel.Axes[0] as DateTimeAxis;
                    if (dateAxis == null) return;
                    dateAxis.Minimum = DateTimeAxis.ToDouble(DateTime.Now.Subtract(TimeSpan.FromDays(DaysToShow)));
                    WeightPlotModel.Axes[0] = dateAxis;
                    
                    var yAxis = WeightPlotModel.Axes[1] as LinearAxis;
                    if (yAxis == null) return;
                    yAxis.Minimum = yAxis.Minimum - 5;
                    yAxis.Maximum = yAxis.Maximum + 5;
                    WeightPlotModel.Axes[1] = yAxis;
                }
            }
        }

        private void UpdateAxis(double min, double max)
        {
            if (WeightPlotModel != null)
            {
                if (WeightPlotModel.Axes != null)
                {
                    var dateAxis = WeightPlotModel.Axes[0] as DateTimeAxis;
                    if (dateAxis == null) return;
                    dateAxis.Minimum = DateTimeAxis.ToDouble(DateTime.Now.Subtract(TimeSpan.FromDays(DaysToShow)));
                    WeightPlotModel.Axes[0] = dateAxis;



                    var yAxis = WeightPlotModel.Axes[1] as LinearAxis;
                    if (yAxis == null) return;
                    yAxis.Minimum = min - 10;
                    yAxis.Maximum = max + 10;
                    WeightPlotModel.Axes[1] = yAxis;
                }
            }
        }

        protected override void DaysToShowUpdated()
        {
            UpdateAxis();
            UpdateFilter();
        }

        private void UpdateFilter()
        {
            var dayFilter =
                new Predicate<object>(
                    item => item != null && ((WeightViewModel)item).TimeValue >=
                            DateTime.Now.Subtract(TimeSpan.FromDays(DaysToShow)));
            if (WeightsView != null && WeightsView.CanFilter)
                WeightsView.Filter = dayFilter;
        }
    }
}