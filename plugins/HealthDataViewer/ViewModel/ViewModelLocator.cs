/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:Plugins.HealthDataViewer"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using System.IO.Ports;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using Plugins.HealthDataViewer.Design;
using Plugins.HealthDataViewer.Model;

namespace Plugins.HealthDataViewer.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            
            if (ViewModelBase.IsInDesignModeStatic)
            {
                // Create design time view services and models
                SimpleIoc.Default.Register<IDataService, DesignDataService>();
            }
            else
            {
                // Create run time view services and models
                SimpleIoc.Default.Register<IDataService, DataService>();
            }

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<BloodPressureGridViewModel>();
            SimpleIoc.Default.Register<WeightGridViewModel>();
            SimpleIoc.Default.Register<SaturationGridViewModel>();
            SimpleIoc.Default.Register<GlucoseGridViewModel>();
        }

        /*public SaturationGridViewModel SaturationGrid
        {
            get
            {
                return ServiceLocator.Current.GetInstance<SaturationGridViewModel>();
            }
        }

        public WeightGridViewModel WeightGrid
        {
            get
            {
                return ServiceLocator.Current.GetInstance<WeightGridViewModel>();
            }
        }*/

        /*public SaturationGridViewModel SaturationGrid
        {
            get
            {
                return ServiceLocator.Current.GetInstance<SaturationGridViewModel>();
            }
        }*/

        public MainViewModel Main
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainViewModel>();
            }
        }
        
        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}