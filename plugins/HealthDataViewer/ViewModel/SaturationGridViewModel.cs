﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Net4Care.Observation;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using Plugins.HealthDataViewer.Model;
using Plugins.HealthDataViewer.ViewModel.Data;

namespace Plugins.HealthDataViewer.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class SaturationGridViewModel : BaseGridViewModel
    {
        private readonly IDataService _dataService;
        private ObservableCollection<SaturationViewModel> _saturations;

        private DispatcherTimer _updateDataTimer;
        private PlotModel _saturationPlotModel;
        private ScatterSeries _series;
        private ICollectionView _saturationsView;

        public ICollectionView SaturationsView
        {
            get { return _saturationsView; }
            private set
            {
                _saturationsView = value;
                RaisePropertyChanged("SaturationsView");
            }
        }

        public ObservableCollection<SaturationViewModel> Saturations
        {
            get { return _saturations; }
            private set
            {
                _saturations = value;
                RaisePropertyChanged("Saturations");
            }
        }

        public PlotModel SaturationPlotModel
        {
            get { return _saturationPlotModel; }
            private set
            {
                _saturationPlotModel = value;
                RaisePropertyChanged("SaturationPlotModel");
            }
        }

        //default constructor for serialization only!
        //public SaturationGridViewModel(){}

        /// <summary>
        /// Initializes a new instance of the SaturationViewModel class.
        /// </summary>
        /// <param name="dataService"></param>
        public SaturationGridViewModel(IDataService dataService)
        {
            _dataService = dataService;
            Saturations = new ObservableCollection<SaturationViewModel>();
            //filter?
            SaturationsView = CollectionViewSource.GetDefaultView(Saturations);
            SaturationsView.SortDescriptions.Add(new SortDescription("TimeValue", ListSortDirection.Descending));
            LoadDataCommand = new RelayCommand(LoadDataCommand_Execute);
            LoadDataCommand_Execute();
            SetupSeries();
            SetupPlot();
            SetupTimer();
        }

        private void UpdateFilter()
        {
            var dayFilter =
                new Predicate<object>(
                    item => item != null && ((SaturationViewModel)item).TimeValue >=
                            DateTime.Now.Subtract(TimeSpan.FromDays(DaysToShow)));
            if (SaturationsView != null && SaturationsView.CanFilter)
                SaturationsView.Filter = dayFilter;
        }

        private void SetupSeries()
        {
            _series = new ScatterSeries { MarkerType = MarkerType.Square, MarkerSize = 6, MarkerStrokeThickness = 1.0, MarkerStroke = OxyColors.Black };
        }

        private void SetupPlot()
        {
            var temp = new PlotModel("Saturation Chart");
            //var ls = new ScatterSeries();
            //temp.Series.Add(ls);
            var dateAxis = new DateTimeAxis
            {
                Minimum = DateTimeAxis.ToDouble(DateTime.Now.Subtract(TimeSpan.FromDays(DaysToShow))),
                Maximum = DateTimeAxis.ToDouble(DateTime.Now)
            };
            temp.Axes.Add(dateAxis);
            var axis = new LinearAxis
            {
                Minimum = 85,
                Maximum = 100,
                //TextColor = OxyColors.Gold
                //Palette = 
                ShowMinorTicks = false,
            };

            temp.Axes.Add(axis);
            SaturationPlotModel = temp;
        }

        private void SetupTimer()
        {
            _updateDataTimer = new DispatcherTimer();
            _updateDataTimer.Tick += UpdateDataTimerOnTick;
            _updateDataTimer.Interval = new TimeSpan(0, 0, 10);
            _updateDataTimer.Start();
        }

        private void UpdateDataTimerOnTick(object sender, EventArgs eventArgs)
        {
            if (string.IsNullOrEmpty(Utility.Cpr))
                Saturations.Clear();
            else
                LoadDataCommand.Execute(this);
        }

        private void LoadDataCommand_Execute()
        {
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background, new Action(async () =>
            {
                try
                {
                    var saturations = await _dataService.GetSaturations();
                    foreach (var sto in saturations)
                    {
                        var sat = new SaturationViewModel(sto);
                        //Only add to collection if it is not already in it!
                        //if (!Saturations.Any(item => item.Saturation == sat.Saturation && item.Time == sat.Time))
                        if (!Saturations.Contains(sat))
                        {
                            Saturations.Add(sat);
                            UpdatePlotModel(sat);
                        }
                    }
                    UpdateFilter();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }));
        }

        private void UpdatePlotModel(SaturationViewModel sat)
        {
            SetupPlot();
            //var ls = SaturationPlotModel.Series[0] as ScatterSeries;
            if (_series == null) return;

            _series.Points.Add(new ScatterPoint(DateTimeAxis.ToDouble(sat.TimeValue), sat.SaturationValue));

            SaturationPlotModel.Series.Add(_series);

            //SaturationPlotModel = temp;
            RaisePropertyChanged("SaturationPlotModel");
        }


        public RelayCommand LoadDataCommand { get; set; }

        private void UpdateAxis()
        {
            if (SaturationPlotModel == null) return;
            if (SaturationPlotModel.Axes == null) return;
            var axis = SaturationPlotModel.Axes[0] as DateTimeAxis;
            if (axis == null) return;
            axis.Minimum = DateTimeAxis.ToDouble(DateTime.Now.Subtract(TimeSpan.FromDays(DaysToShow)));
            SaturationPlotModel.Axes[0] = axis;
        }

        protected override void DaysToShowUpdated()
        {
            UpdateFilter();
            UpdateAxis();
        }
    }
}