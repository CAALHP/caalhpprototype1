﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugins.HealthDataViewer.ViewModel;

namespace Plugins.HealthDataViewer.State
{
    public abstract class HealthDateViewBaseState: IHealthDateViewState
    {
        protected readonly MainViewModel MainView;

        protected HealthDateViewBaseState(MainViewModel mainView)
        {
            MainView = mainView;
        }

        public abstract void UserLoggedIn();
        public abstract void UserLoggedOut();
        public abstract void Show();
    }
}
