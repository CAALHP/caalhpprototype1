﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using Plugins.HealthDataViewer.ViewModel;

namespace Plugins.HealthDataViewer.State
{
    public class UserLoggedOutState: HealthDateViewBaseState
    {
        public UserLoggedOutState(MainViewModel mainView) : base(mainView)
        {
        }

        public override void UserLoggedIn()
        {
            MainView.State = new UserLoggedInState(MainView);
            //Messenger.Default.Send(new NotificationMessage("Hide"));
        }

        public override void UserLoggedOut()
        {
            //empty function by statepattern design
        }

        public override void Show()
        {
            //Messenger.Default.Send(new NotificationMessage(this,"Hide"));
            //empty function by statepattern design
        }
    }
}
