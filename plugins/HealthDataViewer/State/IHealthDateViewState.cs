﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plugins.HealthDataViewer.State
{
    public interface IHealthDateViewState
    {
        void UserLoggedIn();
        void UserLoggedOut();
        void Show();
    }
}
