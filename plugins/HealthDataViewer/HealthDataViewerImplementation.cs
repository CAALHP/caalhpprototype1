﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Events.UserServiceEvents;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;
using GalaSoft.MvvmLight.Threading;
using IKriv.Wpf;
using Plugins.HealthDataViewer.Model;
using Plugins.HealthDataViewer.ViewModel;
using Plugins.HealthDataViewer.Views;

namespace Plugins.HealthDataViewer
{
    public class HealthDataViewerImplementation : IAppCAALHPContract
    {
        private IAppHostCAALHPContract _host;
        private int _processId;
        private Thread _thread;
        //private MainView _view;

        //Tell compiler not to optimize the _app variable by declaring it as volatile!
        private volatile Application _app;

        public HealthDataViewerImplementation()
        {
            InitThread();
            //wait for the _app to be initialized
            //We declare _app as volatile so it won't be cached by optimizations
            //If _app is not volatile the following line could be cached in release mode and never finish.
            while (_app == null) { }
            //_vml = new ViewModelLocator();
            _app.Resources.Add("Locator", new ViewModelLocator());

            //Add datatemplates for subviews
            var manager = new DataTemplateManager(_app);
            manager.RegisterDataTemplate<MainViewModel, MainView>();
            manager.RegisterDataTemplate<BloodPressureGridViewModel, BloodPressureView>();
            manager.RegisterDataTemplate<WeightGridViewModel, WeightView>();
            manager.RegisterDataTemplate<SaturationGridViewModel, SaturationView>();
            manager.RegisterDataTemplate<GlucoseGridViewModel, GlucoseView>();

            DispatchToApp(() => new MainView().Hide());
            // { DataContext = _vml.Main }.Hide());
            //Show homescreen as soon as the plugin is started
            //Show();

        }



        public string GetName()
        {
            return "HealthDataViewer";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            _app.Dispatcher.BeginInvokeShutdown(DispatcherPriority.Normal);
        }

        public void Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            //Subscribe to events so we can react to "Home" messages
            //_host.Host.SubscribeToEvents(_processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(ShowAppEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserListResponseEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserLoggedInEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserLoggedOutEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(IsUserLoggedInResponsEvent)), _processId);

            RequestIsUserLogged();
        }

        public void Show()
        {
            //ref: http://reedcopsey.com/2011/11/28/launching-a-wpf-window-in-a-separate-thread-part-1/
            //Threading is used so we do not block the main thread.


            DispatchToApp(() =>
            {
                            var vml = _app.Resources["Locator"] as ViewModelLocator;
            if (vml != null)
                vml.Main.Show();
            });
        }

        public IList<IPluginInfo> GetListOfInstalledApps()
        {
            throw new NotImplementedException();
        }

        private void InitThread()
        {
            _thread = new Thread(() =>
            {
                _app = new Application();
                _app.ShutdownMode = ShutdownMode.OnExplicitShutdown;
                /*SynchronizationContext.SetSynchronizationContext(
                    new DispatcherSynchronizationContext(
                        Dispatcher.CurrentDispatcher));*/
                DispatcherHelper.Initialize();
                _app.Run();
            });

            //This is needed for GUI threads
            _thread.SetApartmentState(ApartmentState.STA);
            // Make the thread a background thread
            //_thread.IsBackground = true;

            _thread.Start();
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            //We care about the "Home" message
            /*if (notification.Value.Contains("HealthDataViewer"))
            {
                //Show homescreen
                Show();
            }*/
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }



        private void HandleEvent(ShowAppEvent e)
        {
            if (e.AppName.Equals("HealthDataViewer"))
            {
                //Show homescreen
                //Show();
                RequestIsUserLogged();
            }
        }

        private void HandleEvent(UserListResponseEvent e)
        {
            var vml = _app.Resources["Locator"] as ViewModelLocator;
            if (vml != null)
            {
                vml.Main.UserList = e.Users;
            }
                
            //ViewModelEventAggregator.EventAggregator.Publish(new UserListResponseMessage()
            //{
            //    Sender = GetType(),
            //    Users = e.Users,
            //    PhotoRoot = e.PhotoRoot
            //});
        }

        //private void HandleEvent(UserLoggedInEvent e)
        //{
        //    Utility.Cpr = e.User.userName;

        //    var vml = _app.Resources["Locator"] as ViewModelLocator;
        //    if (vml != null)
        //        vml.Main.UserLogIn();
        //}

        //private void HandleEvent(UserLoggedOutEvent e)
        //{
        //    Utility.Cpr = null;


        //    var vml = _app.Resources["Locator"] as ViewModelLocator;
        //    if (vml != null)
        //        vml.Main.UserLogOut();
        //}

        private void HandleEvent(UserLoggedInEvent obj)
        {
            Utility.Cpr = obj.User.UserId;

            var vml = _app.Resources["Locator"] as ViewModelLocator;
            if (vml != null)
                vml.Main.UserListVisible = true;
           
            RequestUserListFromCaalhp();
        }

        private void HandleEvent(UserLoggedOutEvent obj)
        {

            var vml = _app.Resources["Locator"] as ViewModelLocator;
            if (vml != null)
            {
                vml.Main.UserListVisible = false;
                vml.Main.SelectedUser = null;
            }

            Utility.Cpr = ConfigurationManager.AppSettings.Get("cpr");

        }

        private void HandleEvent(IsUserLoggedInResponsEvent e)
        {

            if (e.User != null)
            {
                Utility.Cpr = e.User.UserId;
                var vml = _app.Resources["Locator"] as ViewModelLocator;
                if (vml != null)
                    vml.Main.UserListVisible = true;
            }
                

            Show();
            RequestUserListFromCaalhp();
        }

        private void RequestIsUserLogged()
        {
            var req = new IsUserLoggedInRequestEvent()
                {
                    CallerProcessId = _processId,
                    CallerName = GetName()
                };

            var serializedResponse = EventHelper.CreateEvent(SerializationType.Json, req);
            _host.Host.ReportEvent(serializedResponse);
        }

        private void RequestUserListFromCaalhp()
        {
            var reqEvent = new UserListRequestEvent()
            {
                CallerName = GetName(),
                CallerProcessId = _processId
            };

            var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, reqEvent);
            _host.Host.ReportEvent(serializedEvent);
        }

        private void DispatchToApp(Action action)
        {
            _app.Dispatcher.Invoke(action);
        }

    }
}