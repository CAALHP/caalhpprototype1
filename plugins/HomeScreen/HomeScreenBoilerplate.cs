﻿using System.AddIn;
using Plugins.AppPluginAdapters;

namespace Plugins.HomeScreen
{
    // The AddInAttribute identifies this pipeline segment as an add-in.
    [AddIn("Measurement App Plugin", Version = "1.0.0.0")]
    public class HomeScreenBoilerplate : AppViewPluginAdapter
    {
        public HomeScreenBoilerplate()
        {
            App = new HomeScreenImplementation();
        }
    }
}
