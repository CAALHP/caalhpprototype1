﻿using System.Windows;
using System.Windows.Input;
using Microsoft.Practices.ServiceLocation;
using Plugins.HomeScreen.ViewModel;

namespace Plugins.HomeScreen.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        
        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            Closing += (s, e) => ViewModelLocator.Cleanup();
        }

        void HomeButton_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //ServiceLocator.Current.GetInstance<MainViewModel>().HomeScreenViewCommand.Execute(this);
        }

    }
}