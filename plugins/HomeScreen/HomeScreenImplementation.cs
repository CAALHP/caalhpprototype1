﻿using System;
using System.Collections.Generic;
using System.Windows;
using CAALHP.Contracts;
using Plugins.HomeScreen.ViewModel;
using Plugins.HomeScreen.Views;

namespace Plugins.HomeScreen
{
    public class HomeScreenImplementation : IAppCAALHPContract
    {
        private readonly MainWindow _mainWindow;
        public HomeScreenImplementation()
        {
            var myResourceDictionary = new ResourceDictionary();
            myResourceDictionary.Source = new Uri("Simple Styles.xaml", UriKind.Relative);
            var vmLocator = new ViewModelLocator();
            //myResourceDictionary.Add("Locator", vmLocator);
            Application.Current.Resources.MergedDictionaries.Add(myResourceDictionary);
            _mainWindow = new MainWindow();
            _mainWindow.DataContext = vmLocator;
            Show();

        }
        public void Notify(KeyValuePair<string, string> notification)
        {
            throw new System.NotImplementedException();
        }

        public string GetName()
        {
            return "CAALHP HomeScreen";
        }

        public void Initialize(IHostCAALHPContract hostObj, int processId)
        {
            throw new System.NotImplementedException();
        }

        public void Show()
        {
            _mainWindow.Show();
        }
    }
}