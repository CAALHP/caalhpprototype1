﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using AttributeRouting.Web.Http;
using CareStoreServiceContracts;

namespace Plugins.RestService
{
    public class LoginController : ApiController
    {
        [POST("login")]
        public HttpResponseMessage PostLogin(SecurityToken token)
        {
            //implementation – return a code 200 if all is well
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            return response;
        } 
    }
}