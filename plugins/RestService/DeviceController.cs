﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AttributeRouting.Web.Http;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;
using CareStoreServiceContracts;

namespace Plugins.RestService
{
    /*public class DeviceProfile
    {
        public Guid Id { get; set; }
        public string ModelName { get; set; }
        public string DeviceType { get; set; }
        public string ManufacturerName { get; set; }
    }*/

    //[DefaultHttpRouteConvention]
    public class DeviceController : ApiController
    {
        private readonly IServiceHostCAALHPContract _host;
        private readonly int _processId;

        public DeviceController(IServiceHostCAALHPContract host, int processId)
        {
            _host = host;
            _processId = processId;
        }

        [POST("device")]
        public HttpResponseMessage PostDevice(DeviceProfile profile)
        {
            //implementation
            //profile must be sent in x-www-form-urlencoded format.
            //thanks to http://stackoverflow.com/questions/14624306/web-api-parameter-always-null
            Console.WriteLine("Call to DeviceController.Post");
            var deviceEvent = new DeviceProfileEvent
            {
                CallerName = ToString(),
                CallerProcessId = _processId,
                DeviceId = profile.DeviceId,
                DeviceType = profile.DeviceType,
                ManufacturerName = profile.ManufacturerName,
                ModelName = profile.ModelName,
                Timestamp = DateTime.Now
            };
            //var key = EventHelper.CreateKeyFromEvent("json", deviceEvent);
            //var json = JsonSerializer.SerializeEvent(deviceEvent);
            var theEvent = EventHelper.CreateEvent(SerializationType.Json, deviceEvent);
            _host.Host.ReportEvent(theEvent);
            //_host.AddDevice(profile.ModelName);
            //_deviceManager.AddDevice("PI");
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            return response;
        }

        [GET("device")]
        public string Get()
        {
            Console.WriteLine("Call to DeviceController.Get");
            return "Hello from DeviceController";
        }
    }
}
