﻿using System.AddIn;
using Plugins.ServicePluginAdapter;

namespace Plugins.RestService
{
    // The AddInAttribute identifies this pipeline segment as an add-in.
    [AddIn("Rest Service", Version = "1.0.0.0")]
    public class RestService : ServiceViewPluginAdapter
    {
        public RestService()
        {
            Service = new RestServiceImplementation();
        }
    }
}
