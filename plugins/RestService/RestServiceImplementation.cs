﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Routing;
using System.Web.Http.SelfHost;
using AttributeRouting.Web.Http.SelfHost;
using AttributeRouting.Web.Http.SelfHost.Logging;
using CAALHP.Contracts;
using CAALHP.Library.Managers;
using Microsoft.Practices.Unity;
using Plugins.ServicePluginAdapter;

namespace Plugins.RestService
{
    public class RestServiceImplementation : IServiceCAALHPContract
    {
        private IServiceHostCAALHPContract _host;
        private int _processId;
        private readonly Uri _baseAddress;
        private HttpSelfHostServer _server;
        private readonly HttpSelfHostConfiguration _config;

        public RestServiceImplementation()
        {
            //_baseAddress = address;
            var address = ConfigurationManager.AppSettings.Get("baseaddress");
            _baseAddress = new Uri(address);
            _config = new HttpSelfHostConfiguration(_baseAddress);
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            
        }

        public string GetName()
        {
            return "Rest Service";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IServiceHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            //_host.SubscribeToEvents(_processId);
        }

        public void Start()
        {
            var unity = new UnityContainer();
            unity.RegisterInstance(_host);
            unity.RegisterInstance(_processId);
            unity.RegisterType<DeviceController>(new InjectionConstructor(_host, _processId));
            unity.RegisterInstance(this);
            unity.RegisterType<IServiceHostCAALHPContract, ServiceHostViewToCAALHPContractAdapter>(new PerResolveLifetimeManager());
            _config.DependencyResolver = new IoCContainer(unity);

            _config.Routes.MapHttpRoute(
                name: "Default",
                routeTemplate: "{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            _config.Routes.MapHttpAttributeRoutes(cfg => cfg.AddRoutesFromAssembly(Assembly.GetExecutingAssembly()));

            _server = new HttpSelfHostServer(_config);

            _server.OpenAsync().Wait();

            Console.WriteLine("Routes begin:");
            _config.Routes.Cast<HttpRoute>().ToArray().LogTo(Console.Out);
            Console.WriteLine("Routes end:");

            Console.WriteLine("The service is ready at {0}", _baseAddress);
        }
        public void Stop()
        {
            // Close the ServiceHost.
            _server.CloseAsync();
        }
    }
}
