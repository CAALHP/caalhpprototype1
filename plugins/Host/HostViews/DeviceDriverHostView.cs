﻿using CAALHP.Library.Hosts;
using Plugins.HostViewAdapters;

namespace Plugins.Host.HostViews
{
    public class DeviceDriverHostView : IDeviceDriverHostView
    {
        private readonly IDeviceDriverHost _serviceHost;

        public IHostBaseView Host { get; set; }

        public DeviceDriverHostView(IDeviceDriverHost host)
        {
            _serviceHost = host;
            Host = new HostView(_serviceHost);
        }
    }
}