﻿using System.Collections.Generic;
using System.Linq;
using CAALHP.Contracts;
using CAALHP.Library.Hosts;
using Plugins.HostViewAdapters;

namespace Plugins.Host.HostViews
{
    /// <summary>
    /// This class can be called from addins
    /// </summary>
    public class HostView : IHostBaseView
    {
        private readonly IHost _host;

        public HostView(IHost host)
        {
            _host = host;
        }

        public void ReportEvent(KeyValuePair<string,string> value)
        {
            //update UI
            _host.ReportEvent(value);
            //Console.WriteLine(DateTime.Now.ToShortTimeString()+" Got measurement: " + value);
        }

        /*public void ReportEvent(string fullyQualifiedNameSpace, KeyValuePair<string, string> value)
        {
            _host.ReportEvent(fullyQualifiedNameSpace, value);
        }

        public void SubscribeToEvents(int processId)
        {
            _host.SubscribeToEvents(processId);
        }*/

        public void SubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            _host.SubscribeToEvents(fullyQualifiedNameSpace, processId);
        }

        /*public void UnSubscribeToEvents(int processId)
        {
            _host.UnSubscribeToEvents(processId);
        }*/

        public void UnSubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            _host.UnSubscribeToEvents(fullyQualifiedNameSpace, processId);
        }

        public IList<IPluginInfo> GetListOfInstalledApps()
        {
            return _host.GetListOfInstalledApps().Select(plugin => new PluginInfo {LocationDir = plugin.Directory, Name = plugin.Config.PluginName}).Cast<IPluginInfo>().ToList();
        }

        public IList<IPluginInfo> GetListOfInstalledDeviceDrivers()
        {
            return _host.GetListOfInstalledDeviceDrivers().Select(plugin => new PluginInfo { LocationDir = plugin.Directory, Name = plugin.Config.PluginName }).Cast<IPluginInfo>().ToList();
        }

        public void ShowApp(string appName)
        {
            _host.ShowApp(appName);
        }

        public void CloseApp(string appName)
        {
            _host.CloseApp(appName);
        }

    }
}
