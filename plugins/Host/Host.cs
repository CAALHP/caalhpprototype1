﻿using System;
using System.AddIn.Hosting;
using System.Collections.Generic;
using System.Threading.Tasks;
using CAALHP.Contracts;
using CAALHP.Library.Config;
using CAALHP.Library.Hosts;
using CAALHP.Library.Managers;
using CAALHP.Library.Observer;
using CAALHP.Library.Visitor;
using Plugins.Host.HostViews;
using Plugins.HostViewAdapters;

namespace Plugins.Host
{
    public abstract class Host : IHost
    {
        protected string AddInRoot { get; private set; }
        protected IHostBaseView HostObjectView { get; private set; }
        protected EventManager EventManager { get; private set; }

        protected Host(EventManager eventManager)
        {
            EventManager = eventManager;
            // Assume that the current directory is the application folder,  
            // and that it contains the pipeline folder structure.
            AddInRoot = Environment.CurrentDirectory + "\\Pipeline";

            UpdateAddInStore();

            HostObjectView = new HostView(this);
        }

        protected void UpdateAddInStore()
        {
            // Update the cache files of the pipeline segments and add-ins. 
            var warnings = AddInStore.Update(AddInRoot);
            foreach (var warning in warnings)
            {
                Console.WriteLine(warning);
            }
        }

        public abstract void SubscribeToEvents(string fullyQualifiedNameSpace, int processId);
        /*{
            //Console.WriteLine("Subscribing process:{0} to {2} events via {1}", processId, this, fullyQualifiedNameSpace);
            EventManager.EventSubject.Attach(new EventObserver(EventManager.EventSubject, processId, this, fullyQualifiedNameSpace),
                fullyQualifiedNameSpace);
            
        }*/

        public abstract void UnSubscribeToEvents(string fullyQualifiedNameSpace, int processId);
        /*{
            EventManager.EventSubject.Detach(processId, fullyQualifiedNameSpace);
        }*/

        public void ReportEvent(KeyValuePair<string, string> value)
        {
            //EventManager.EventSubject.SubjectState = value;
            EventManager.EventSubject.Notify(value);
        }

        public abstract void RegisterPlugin(string name, string category);
        public abstract IList<PluginConfig> GetListOfInstalledApps();
        public abstract void ShowApp(string appName);
        public abstract void CloseApp(string appName);
        public abstract IList<PluginConfig> GetListOfInstalledDeviceDrivers();
        public abstract void ShutDown();
        public abstract void Accept(IHostVisitor visitor);
    }
}