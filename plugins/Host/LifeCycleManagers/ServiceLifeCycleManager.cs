﻿using System;
using System.AddIn.Hosting;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using CAALHP.Library.Config;
using Plugins.Host.Processes;
using Plugins.HostViewAdapters;

namespace Plugins.Host.LifeCycleManagers
{
    public class ServiceLifeCycleManager : LifeCycleManager
    {
        private readonly IServiceHostView _hostView;
        public Dictionary<int, ServiceProcess> ServiceDictionary { get; private set; }
        //private readonly string _pluginRoot;

        public ServiceLifeCycleManager(string pluginRoot, IServiceHostView hostView)
            : base(pluginRoot)
        {
            _hostView = hostView;
            ServiceDictionary = new Dictionary<int, ServiceProcess>();
            //_pluginRoot = pluginRoot;
        }

        protected override void KeepAliveTimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            foreach (var serviceProcess in ServiceDictionary)
            {
                var process = serviceProcess;
                Task.Run(() =>
                {
                    if (process.Value.Service == null) return;
                    try
                    {
                        if (!process.Value.Service.IsAlive())
                        {
                            RestartPlugin(process.Key);
                        }
                    }
                    catch (Exception e)
                    {
                        //Console.WriteLine(e);
                        RestartPlugin(process.Key);
                    }
                });
            }
        }

        public override async void ActivatePlugin(PluginConfig plugin)
        {
            await ActivatePluginAsync(plugin);
        }

        protected async Task ActivatePluginAsync(PluginConfig plugin)
        {
            await Task.Run(() =>
            {
                if (PluginIsActive(plugin)) return;
                UpdateAddInStore();
                var tokens = AddInStore.FindAddIns(typeof(IServiceView), PluginRoot);
                //var app = tokens[appIndex].Activate<IApp>(AddInSecurityLevel.Internet);
                //activate in a new process
                //var app = tokens[appIndex].Activate<IApp>(new AddInProcess(),AddInSecurityLevel.Internet);
                //activate in a new process and set permissions with permissionset
                foreach (var token in tokens)
                {
                    if (token.Name.Equals(plugin.Config.PluginName))
                    {
                        var app = token.Activate<IServiceView>(new AddInProcess(), AddInSecurityLevel.FullTrust);
                        var process = new ServiceProcess(app, plugin);
                        ServiceDictionary.Add(process.ProcessId, process);
                        app.Initialize(_hostView, process.ProcessId);
                        Console.WriteLine("activated service: " + token.Name);
                    }
                }


                /*if (PluginIsActive(plugin)) return;
                //start the first file
                var processStartInfo =
                    new ProcessStartInfo(Path.Combine(plugin.Directory, plugin.Config.RunFiles.First()));
                //processStartInfo.
                
                var process = Process.Start(processStartInfo);
                var pluginProcess = new ServiceProcess(process, plugin);
                if (process != null)
                {
                    ServiceDictionary.Add(process.Id, pluginProcess);
                    Console.WriteLine("activated service: " + plugin.Config.PluginName);
                }*/
            });
        }

        public override void StopPlugins()
        {
            KeepAliveTimer.Stop();
            ShouldKeepAlive = false;
            foreach (var serviceProcess in ServiceDictionary.Values.Where(serviceProcess => serviceProcess.Service != null))
            {
                //We expect communication to break after this call, so we will not wait for a response.

                var process = serviceProcess;
                Task.Run(() =>
                {
                    try
                    {
                        process.Service.ShutDown();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Plugin already shut down");
                    }
                });
            }
        }

        protected override int GetRestartCounter(int processId)
        {
            return ServiceDictionary.ContainsKey(processId) ? ServiceDictionary[processId].RestartCount : int.MaxValue;
        }

        protected override void IncrementRestartCounter(int processId)
        {
            if (ServiceDictionary.ContainsKey(processId)) ServiceDictionary[processId].RestartCount++;
        }

        protected override void ReactivatePlugin(int key)
        {
            UpdateAddInStore();
            var tokens = AddInStore.FindAddIns(typeof(IServiceView), PluginRoot);
            var pluginName = ServiceDictionary[key].Config.Config.PluginName;
            foreach (var token in tokens.Where(token => token.Name.Equals(pluginName)))
            {
                ServiceDictionary[key].Service = token.Activate<IServiceView>(new AddInProcess(), AddInSecurityLevel.FullTrust);
                var newProcessId =
                    AddInController.GetAddInController(ServiceDictionary[key].Service).AddInEnvironment.Process.ProcessId;

                ServiceDictionary[key].Service.Initialize(_hostView, newProcessId);
                ServiceDictionary.Add(newProcessId, ServiceDictionary[key]);
                ServiceDictionary.Remove(key);
                Console.WriteLine("reactivated service: " + token.Name);
                //DeviceDriverDictionary[key].DeviceDriver.Show();
            }
        }

        protected override void DeactivatePlugin(int processId)
        {
            try
            {
                if (!ServiceDictionary.ContainsKey(processId)) return;
                var view = ServiceDictionary[processId].Service;
                if (view == null) return;
                ServiceDictionary[processId].Service = null;
                var controller = AddInController.GetAddInController(view);
                controller.Shutdown();
            }
            catch (Exception e)
            {
                Console.WriteLine("plugin was already closed");
            }
        }

        private bool PluginIsActive(PluginConfig config)
        {
            return ServiceDictionary.Values.Any(serviceProcess => serviceProcess.Config.Directory.Equals(config.Directory));
        }
    }
}