﻿using System;
using System.AddIn.Hosting;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using CAALHP.Library.Config;
using Plugins.Host.Processes;
using Plugins.HostViewAdapters;

namespace Plugins.Host.LifeCycleManagers
{
    public class AppLifeCycleManager : LifeCycleManager
    {
        private readonly IAppHostView _hostView;
        public Dictionary<int, AppProcess> AppDictionary { get; private set; }

        public AppLifeCycleManager(string pluginRoot, IAppHostView hostView)
            : base(pluginRoot)
        {
            _hostView = hostView;
            AppDictionary = new Dictionary<int, AppProcess>();
        }

        protected override void KeepAliveTimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            foreach (var appProcess in AppDictionary)
            {
                var process = appProcess;
                Task.Run(() =>
                {
                    if (process.Value.App == null) return;
                    try
                    {
                        if (!process.Value.App.IsAlive())
                        {
                            RestartPlugin(process.Key);
                        }
                    }
                    catch (Exception e)
                    {
                        //Console.WriteLine(e);
                        RestartPlugin(process.Key);
                    }
                });
            }
        }

        protected override int GetRestartCounter(int processId)
        {
            return AppDictionary.ContainsKey(processId) ? AppDictionary[processId].RestartCount : int.MaxValue;
        }

        protected override void IncrementRestartCounter(int processId)
        {
            if (AppDictionary.ContainsKey(processId)) AppDictionary[processId].RestartCount++;
        }

        protected override void ReactivatePlugin(int key)
        {
            UpdateAddInStore();
            var tokens = AddInStore.FindAddIns(typeof(IAppView), PluginRoot);
            var pluginName = AppDictionary[key].Config.Config.PluginName;
            foreach (var token in tokens.Where(token => token.Name.Equals(pluginName)))
            {
                AppDictionary[key].App = token.Activate<IAppView>(new AddInProcess(), AddInSecurityLevel.FullTrust);
                var newProcessId =
                    AddInController.GetAddInController(AppDictionary[key].App).AddInEnvironment.Process.ProcessId;
                AppDictionary[key].App.Initialize(_hostView, newProcessId);
                AppDictionary.Add(newProcessId, AppDictionary[key]);
                AppDictionary.Remove(key);
                Console.WriteLine("reactivated app: " + token.Name);
                AppDictionary[newProcessId].App.Show();
            }
        }

        protected override void DeactivatePlugin(int processId)
        {
            try
            {
                if (!AppDictionary.ContainsKey(processId)) return;
                var view = AppDictionary[processId].App;
                if (view == null) return;
                AppDictionary[processId].App = null;
                var controller = AddInController.GetAddInController(view);
                controller.Shutdown();
            }
            catch (Exception e)
            {
                Console.WriteLine("plugin was already closed");
            }
        }

        public override async void ActivatePlugin(PluginConfig plugin)
        {
            await ActivatePluginAsync(plugin);
        }

        private async Task ActivatePluginAsync(PluginConfig plugin)
        {
            await Task.Run(() =>
            {
                if (!PluginIsActive(plugin))
                {
                    UpdateAddInStore();
                    var tokens = AddInStore.FindAddIns(typeof(IAppView), PluginRoot);
                    //var app = tokens[appIndex].Activate<IApp>(AddInSecurityLevel.Internet);
                    //activate in a new process
                    //var app = tokens[appIndex].Activate<IApp>(new AddInProcess(),AddInSecurityLevel.Internet);
                    //activate in a new process and set permissions with permissionset
                    foreach (var token in tokens)
                    {
                        if (!token.Name.Equals(plugin.Config.PluginName)) continue;
                        var app = token.Activate<IAppView>(new AddInProcess(), AddInSecurityLevel.FullTrust);
                        var process = new AppProcess(app, plugin);
                        AppDictionary.Add(process.ProcessId, process);
                        app.Initialize(_hostView, process.ProcessId);
                        Console.WriteLine("activated app: " + token.Name);
                        app.Show();
                    }
                }
                else
                {
                    var app = AppDictionary.FirstOrDefault(x => x.Value.Config.Directory.Equals(plugin.Directory));
                    app.Value.App.Show();
                }
            });
        }

        private bool PluginIsActive(PluginConfig plugin)
        {
            return AppDictionary.Values.Any(appProcess => appProcess.Config.Directory.Equals(plugin.Directory) && appProcess.App != null);
        }

        public override void StopPlugins()
        {
            KeepAliveTimer.Stop();
            ShouldKeepAlive = false;
            foreach (var appProcess in AppDictionary.Values.Where(appProcess => appProcess.App != null))
            {
                //We expect communication to break after this call, so we will not wait for a response.
                var process = appProcess;
                Task.Run(() =>
                {
                    try
                    {
                        process.App.ShutDown();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Plugin already shut down");
                    }
                });
            }
        }
    }
}
