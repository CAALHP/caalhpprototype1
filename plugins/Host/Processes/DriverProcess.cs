﻿using System;
using System.AddIn.Hosting;
using System.Collections.Generic;
using CAALHP.Library.Config;
using Plugins.HostViewAdapters;

namespace Plugins.Host.Processes
{
    public class DriverProcess : BaseProcess
    {
        public IDeviceDriverView DeviceDriver { get; set; }
        public DriverProcess(IDeviceDriverView deviceDriver, PluginConfig config) : base(config)
        {
            DeviceDriver = deviceDriver;
            ProcessId = AddInController.GetAddInController(deviceDriver).AddInEnvironment.Process.ProcessId;
            Init();
        }

        public override void Update(KeyValuePair<string, string> theEvent)
        {
            if (DeviceDriver != null) DeviceDriver.Notify(theEvent);
        }

        public override void NewEventTypeUpdate(string newFqns)
        {
            //throw new NotImplementedException();
        }
    }
}