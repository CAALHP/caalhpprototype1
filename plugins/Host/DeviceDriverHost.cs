﻿using System;
using System.AddIn.Hosting;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CAALHP.Library.Brokers;
using CAALHP.Library.Config;
using CAALHP.Library.Hosts;
using CAALHP.Library.Managers;
using CAALHP.Library.Observer;
using CAALHP.Library.Visitor;
using Plugins.Host.HostViews;
using Plugins.Host.LifeCycleManagers;
using Plugins.Host.Processes;
using Plugins.HostViewAdapters;

namespace Plugins.Host
{
    public class DeviceDriverHost : Host, IDeviceDriverHost
    {
        //private readonly Dictionary<int, DriverProcess> _driverDictionary = new Dictionary<int, DriverProcess>();
        private int _driverDictionaryIndex;
        private readonly ICAALHPBroker _broker;
        public DeviceDriverLifeCycleManager LifeCycleManager { get; private set; }

        protected IDeviceDriverHostView HostView { get; private set; }

        public DeviceDriverHost(EventManager eventManager, ICAALHPBroker broker)
            : base(eventManager)
        {
            _broker = broker;
            HostView = new DeviceDriverHostView(this);
            LifeCycleManager = new DeviceDriverLifeCycleManager(AddInRoot, HostView);
            ActivateDrivers();
        }

        public async void ActivateDrivers()
        {
            LifeCycleManager.StartPlugins();

            //await Task.Run(() =>
            //{
            //    UpdateAddInStore();
            //    var tokens = AddInStore.FindAddIns(typeof(IDeviceDriverView), AddInRoot);
            //    foreach (var token in tokens)
            //    {
            //        //async activation of each driver:
            //        ActivateDriver(token);
            //    }
            //});
        }

        //private async void ActivateDriver(AddInToken token)
        //{
        //    if (!TokenIsInDictionary(token))
        //    {
        //        await ActivateAsync(token);
        //        //Console.WriteLine("activated driver: " + token.Name);
        //    }
        //}

        //private async Task ActivateAsync(AddInToken token)
        //{
        //    await Task.Run(() =>
        //    {
        //        //var driver = token.Activate<IDriverView>(new AddInProcess(), AddInSecurityLevel.Internet);
        //        var driver = token.Activate<IDeviceDriverView>(new AddInProcess(), AddInSecurityLevel.FullTrust);
        //        //var driver = token.Activate<IDriverView>(AddInSecurityLevel.Internet);
        //        var installedDrivers = GetLocalInstalledDeviceDrivers();
        //        var config = installedDrivers.FirstOrDefault(x => x.Config.PluginName.Equals(token.Name));
        //        var driverProcess = new DriverProcess(driver, config);
        //        driver.Initialize(HostView, driverProcess.ProcessId);
        //        LifeCycleManager.DeviceDriverDictionary.Add(_driverDictionaryIndex++, driverProcess);
        //        Console.WriteLine("activated device driver: " + token.Name);
        //    });
        //}

        //private bool TokenIsInDictionary(AddInToken token)
        //{
        //    if (token == null) throw new ArgumentNullException("token");
        //    return LifeCycleManager.DeviceDriverDictionary.Values.Any(driver => AddInController.GetAddInController(driver.DeviceDriver).Token.Equals(token));
        //}

        public void driver_NewMeasurementEvent(object sender, EventArgs e)
        {
            Console.WriteLine("A new measurement was made");
        }

        /// <summary>
        /// Bending the truth on this one.
        /// </summary>
        /// <returns>List of drivers</returns>
        public List<string> GetListOfActiveDevices()
        {
            return LifeCycleManager.DeviceDriverDictionary.Select(record => record.Key + " : " + AddInController.GetAddInController(record.Value.DeviceDriver).Token.Name).ToList();
        }

        public void UpdateDriverList()
        {
            ActivateDrivers();
        }

        public IList<PluginConfig> GetLocalInstalledDeviceDrivers()
        {
            UpdateAddInStore();
            var plugins = GetInstalledPlugins();
            var tokens = AddInStore.FindAddIns(typeof(IDeviceDriverView), AddInRoot);
            return (from token in tokens from pluginConfig in plugins where pluginConfig.Config.PluginName.Equals(token.Name) select pluginConfig).ToList();
        }

        private IList<PluginConfig> GetInstalledPlugins()
        {
            var plugins = new List<PluginConfig>();
            var folders = Directory.EnumerateDirectories(Path.Combine(AddInRoot, "AddIns"));
            foreach (var folder in folders)
            {
                foreach (var file in Directory.GetFiles(folder))
                {
                    var fileName = Path.GetFileName(file);
                    if (fileName == null || !fileName.Equals("CareStorePlugin.xml")) continue;
                    var config = PluginConfigurationManager.GetConfig(file);
                    var plugin = new PluginConfig { Config = config, Directory = Path.GetDirectoryName(file) };
                    plugins.Add(plugin);
                    /*foreach (var runfile in config.RunFiles)
                        {
                            var plugin = new PluginInfo();
                            plugin.LocationDir = Path.GetDirectoryName(file);
                            plugin.Name = runfile;
                            plugins.Add(plugin);
                        }*/
                }
            }
            return plugins;
        }

        public override IList<PluginConfig> GetListOfInstalledDeviceDrivers()
        {
            return _broker.GetListOfInstalledDeviceDrivers();
        }

        public override void UnSubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            if (!LifeCycleManager.DeviceDriverDictionary.ContainsKey(processId)) return;
            var plugin = LifeCycleManager.DeviceDriverDictionary[processId];
            EventManager.EventSubject.Detach(plugin, fullyQualifiedNameSpace);
        }

        public override void SubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            if (!LifeCycleManager.DeviceDriverDictionary.ContainsKey(processId)) return;
            var plugin = LifeCycleManager.DeviceDriverDictionary[processId];
            EventManager.EventSubject.Attach(plugin, fullyQualifiedNameSpace);
        }

        public override void ShutDown()
        {
            LifeCycleManager.StopPlugins();
        }

        public override void Accept(IHostVisitor visitor)
        {
            visitor.Visit(this);
        }

        public void CloseDeviceDriver(int index)
        {
            if (LifeCycleManager.DeviceDriverDictionary.ContainsKey(index))
            {

                try
                {
                    EventManager.EventSubject.Detach(LifeCycleManager.DeviceDriverDictionary[index]);
                    var controller = AddInController.GetAddInController(LifeCycleManager.DeviceDriverDictionary[index].DeviceDriver);
                    if (controller.AddInEnvironment != null)
                        if (controller.AddInEnvironment.Process != null)
                            controller.AddInEnvironment.Process.Shutdown();
                    if (controller.AppDomain != null)
                        AppDomain.Unload(controller.AppDomain);
                    controller.Shutdown();
                }
                finally
                {
                    LifeCycleManager.DeviceDriverDictionary.Remove(index);
                }
            }
        }

        public override void RegisterPlugin(string name, string category)
        {
            //null implementation
        }

        public override IList<PluginConfig> GetListOfInstalledApps()
        {
            return _broker.GetListOfInstalledApps();
        }

        public override void ShowApp(string appName)
        {
            _broker.ShowApp(appName);
        }

        public override void CloseApp(string appName)
        {
            _broker.CloseApp(appName);
        }
    }
}
