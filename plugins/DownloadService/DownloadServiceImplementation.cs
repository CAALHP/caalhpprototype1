﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Media;
using System.Threading;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;

namespace Plugins.DownloadService
{
    public class DownloadServiceImplementation : IServiceCAALHPContract
    {
        private IServiceHostCAALHPContract _host;
        private int _processId;
        private IDownloadManager _downloadManager;

        public DownloadServiceImplementation()
        {

        }
        
        public void Notify(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);

        }

        private void HandleEvent(DownloadAppEvent e)
        {
            DownloadApp(e.FileName);
        }

        private void HandleEvent(DownloadDeviceDriverEvent e)
        {
            DownloadDeviceDriver(e.DeviceDriverFileName);
        }

        private async void DownloadDeviceDriver(string filename)
        {
            if (filename == null) return;
            PlayStartDownloadSound();
            var progress = new Progress<int>(i => DownloadProgress(i, filename));
            await _downloadManager.DownloadDeviceDriverAsync(filename, CancellationToken.None, progress);
            //var command = EventHelper.CreateEvent(_processId, GetName(), "DownloadDeviceDriverCompleted", filename);
            var command = new DownloadDeviceDriverCompletedEvent
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                DeviceDriverFileName = filename
            };
            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);
            //var command = EventHelper.CreateEvent(_processId, GetName(), "DownloadDeviceDriver", deviceName);
            _host.Host.ReportEvent(serializedCommand);
            //_host.Host.ReportEvent(command);
        }

        private void PlayStartDownloadSound()
        {
            SystemSounds.Beep.Play();
        }

        private async void DownloadApp(string filename)
        {
            PlayStartDownloadSound();
            var progress = new Progress<int>(i => DownloadProgress(i,filename));
            await _downloadManager.DownloadAppAsync(filename, CancellationToken.None, progress);
            //var command = EventHelper.CreateEvent(_processId, GetName(), "DownloadAppCompleted", filename);
            var command = new DownloadAppCompletedEvent()
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                FileName = filename
            };
            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);
            _host.Host.ReportEvent(serializedCommand);
        }

        private void DownloadProgress(int progress, string filename)
        {
            //var command = EventHelper.CreateEvent(_processId, GetName(), "DownloadProgress", filename + ";" + progress.ToString(CultureInfo.InvariantCulture));
            var command = new DownloadProgressEvent()
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                PercentDone = progress,
                FileName = filename
            };
            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);
            _host.Host.ReportEvent(serializedCommand);
        }

        public string GetName()
        {
            return "DownloadService";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IServiceHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            _downloadManager = new DownloadManager();
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(DownloadAppEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(DownloadDeviceDriverEvent)), _processId);
            //_host.Host.SubscribeToEvents(_processId);
        }

        public void Start()
        {
            //throw new System.NotImplementedException();
        }

        public void Stop()
        {
            //throw new System.NotImplementedException();
        }
    }
}