﻿using System.AddIn;
using Plugins.ServicePluginAdapter;

namespace Plugins.DownloadService
{
    // The AddInAttribute identifies this pipeline segment as an add-in.
    [AddIn("Download Service", Version = "1.0.0.0")]
    public class DownloadService : ServiceViewPluginAdapter
    {
        public DownloadService()
        {
            Service = new DownloadServiceImplementation();
        }
    }
}
