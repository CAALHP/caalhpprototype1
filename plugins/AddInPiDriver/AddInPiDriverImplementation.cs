﻿using System;
using System.Collections.Generic;
using CAALHP.Contracts;
using Timer = System.Timers.Timer;

namespace Plugins.AddInPiDriver
{
    public class AddInPiDeviceDriverImplementation : IDeviceDriverCAALHPContract
    {
        private IDeviceDriverHostCAALHPContract _host;
        private readonly Timer _timer;
        private int _processId;
        public AddInPiDeviceDriverImplementation()
        {
            _timer = new Timer(3000) {AutoReset = true};
            //_timer.Elapsed += timer_Elapsed;
        }

        void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            var key = _processId + ";AddInPiDriver;" + DateTime.Now;
            var value = 1 + ";" + "PI;" + GetMeasurement();
            var pair = new KeyValuePair<string, string>(key,value);
            _host.Host.ReportEvent(pair);
        }    

        public double GetMeasurement()
        {
            return Math.PI;
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            throw new NotImplementedException();
        }

        public string GetName()
        {
            return "AddInPiDriver";
        }

        public void Initialize(IDeviceDriverHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            //_host.SubscribeToEvents(_processId);
            //_timer.Start();
            timer_Elapsed(null,null);
        }
    }
}
