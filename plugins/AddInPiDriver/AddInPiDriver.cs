﻿using System.AddIn;
using Plugins.DriverPluginAdapters;

namespace Plugins.AddInPiDriver
{
    // The AddInAttribute identifies this pipeline segment as an add-in.
    [AddIn("Pi Driver Plugin", Version = "1.0.0.0")]
    public class AddInPiDeviceDriver : DeviceDriverViewPluginAdapter
    {
        public AddInPiDeviceDriver()
        {
            DeviceDriver = new AddInPiDeviceDriverImplementation();
        }
    }
}
