﻿using CAALHP.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAALHP.Events.Types;

namespace Plugins.UserService.Managers
{

    public class AuthenticationManager
    {


        private IServiceHostCAALHPContract _host;
        //private UserProfile _currentUser;
        private UserManager _userManager;



        public AuthenticationManager(IServiceHostCAALHPContract host, UserManager userManager)
        {
            _host = host;
            _userManager = userManager;
        }

        public AuthenticationManager()
        {
            // TODO: Complete member initialization
        }

        public bool authenticateUser(string username, string pincode)
        {


            //get uers list
            //UserManager um = new UserManager();
            var users = _userManager.getUserList();

            //authentication decision for client
            var authenticationDecision = false;

            foreach (UserProfile user in users.Where(user => user.UserName == username))
            {
                authenticationDecision = user.Pin == pincode;
                break;
            }

            return authenticationDecision;
         }
    }
}