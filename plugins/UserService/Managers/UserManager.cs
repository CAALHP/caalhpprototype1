﻿using System.Diagnostics;
using System.Reflection;
using CAALHP.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Drawing;
using System.Configuration;
using CAALHP.Events.Types;


namespace Plugins.UserService.Managers
{
    public class UserManager
    {
        #region fields

        private IServiceHostCAALHPContract _host;
        private string _assemblyPath;
        private string _userFileName;

        #endregion
        
        #region constructor

        public UserManager(IServiceHostCAALHPContract host)
        {
            //Debugger.Launch();
            _host = host;
            _userFileName = "Users.xml";
            _assemblyPath = AssemblyDirectory + "\\";

            var dataDirectory = Path.Combine(_assemblyPath, "Data\\");
            CopyDefaultUsersXmlToPath(dataDirectory);
        }

        public UserManager()
        {

        }

        #endregion
        
        #region public methods

        public string UserFilePath
        {
            get { return Path.Combine(_assemblyPath, "Data\\", _userFileName); }
        }

        public string ImgDirectoryPath
        {
            get { return Path.Combine(_assemblyPath, "Images\\"); }
        }

        //get list of users in the xml file
        public List<UserProfile> getUserList()
        {
            //UserList usrlst = new UserList();
            XmlSerializer deserializer = new XmlSerializer(typeof (List<UserProfile>));
            TextReader textReader = new StreamReader(UserFilePath, true);
            var usrlst = deserializer.Deserialize(textReader) as List<UserProfile>;
            textReader.Close();

            CheckForImg(usrlst);

            return usrlst;
        }

        #endregion
        
        #region private methods

        private void CopyDefaultUsersXmlToPath(string outPutDirectory)
        {
            var defaultFilePath = Path.Combine(_assemblyPath, _userFileName);
            var outPutFilePath = Path.Combine(outPutDirectory, _userFileName);

            if (File.Exists(outPutFilePath))
                return;

            if (!Directory.Exists(outPutDirectory))
                Directory.CreateDirectory(outPutDirectory);

            if (!File.Exists(defaultFilePath))
                CreateDefaultXmlFile(defaultFilePath);

            File.Copy(defaultFilePath, outPutFilePath);
        }

        private void CreateDefaultXmlFile(string path)
        {
            var userList = new List<UserProfile>();
            var defaultUser = new UserProfile();

            userList.Add(defaultUser);

            XmlSerializer serializer = new XmlSerializer(typeof (List<UserProfile>));

            FileStream fs = new FileStream(path, FileMode.Create);
            TextWriter writer = new StreamWriter(fs, new UTF8Encoding());
            // Serialize using the XmlTextWriter.
            serializer.Serialize(writer, userList);
            writer.Close();
        }



        //Checks if the image exsists in local folder, or sets photolink to null if it doest
        private void CheckForImg(List<UserProfile> users)
        {

            var sourcePath = _assemblyPath + "Images\\";

            if (!Directory.Exists(sourcePath))
            {
                Directory.CreateDirectory(sourcePath);
            }

            foreach (var user in users)
            {
                if (user.PhotoLink == null) continue;

                var current = AssemblyDirectory;
                if (current == null)
                {
                    user.HasPhoto = false;
                    continue;
                }

                var sourceImagePath = Path.Combine(sourcePath, user.PhotoLink);
                //if (!File.Exists(sourceImagePath))
                //    user.PhotoLink = null;
                user.HasPhoto = File.Exists(sourceImagePath);
            }

        }

        private static string AssemblyDirectory
        {
            get
            {
                var codeBase = Assembly.GetExecutingAssembly().CodeBase;
                var uri = new UriBuilder(codeBase);
                var path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        #endregion


    }
}