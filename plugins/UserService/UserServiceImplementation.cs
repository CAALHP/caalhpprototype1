﻿using System;
using System.Collections.Generic;
using System.Linq;
using CAALHP.Contracts;
using CAALHP.Events.Types;
using CAALHP.Events.UserServiceEvents;
//using FI.Events;
using FI.Events;
using FS.Events;
using Plugins.UserService.Managers;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;
using RequestTypes = FS.Events.RequestTypes;

namespace Plugins.UserService
{
    public class UserServiceImplementation : IServiceCAALHPContract
    {
        //caalhp classes
        private IServiceHostCAALHPContract _host;
        private int _processId;

        //user app classes
        private UserManager _userManager;

        private AuthenticationManager _authManager;
        private UserProfile _user;
        private object _userLock = new object();
        //private UserProfile up;


        public UserServiceImplementation()
        {
            _userManager = new UserManager(_host);
            _authManager = new AuthenticationManager(_host, _userManager);

        }

        public UserProfile User
        {
            get
            {
                UserProfile res;
                lock (_userLock)
                {
                    res = _user;
                }
                return res;
            }
            private set
            {
                lock (_userLock)
                {
                    _user = value;
                }
                
            }
        }
        
        //handle "authentication request" and "authentication response" events
        private void HandleEvent(AuthenticationRequestEvent userCredentials)
        {
            switch (userCredentials.ReqType)
            {
                case CAALHP.Events.Types.RequestTypes.CheckforScanner:
                    CheckForScannerRequest();
                    break;
                case CAALHP.Events.Types.RequestTypes.Enroll:
                    RequestEnroll(userCredentials.Username);
                    break;
                case CAALHP.Events.Types.RequestTypes.AuthByFinger:
                    AuthByFingerPrint(userCredentials.Username);
                    break;
                case CAALHP.Events.Types.RequestTypes.AuthByPincode:
                    AuthByPin(userCredentials);
                    break;
            }
        }
        
        //handle uselist event 
        private void HandleEvent(UserListRequestEvent userList)
        {

            var users = _userManager.getUserList();
            var photoRoot = _userManager.ImgDirectoryPath;

            var response = new UserListResponseEvent
                {
                    Users = users,
                    PhotoRoot = photoRoot
                };

            var serializedList = EventHelper.CreateEvent(SerializationType.Json, response);
            _host.Host.ReportEvent(serializedList);

        }

        private void HandleEvent(FSResponsEvent fsRespons)
        {
            var type = fsRespons.RequestType;
            var response = new AuthenticationResponseEvent
            {
                Username = fsRespons.UserId,
                Response = fsRespons.Result,
                Message = fsRespons.Message ?? ""
            };

            switch (type)
            {
                case RequestTypes.Auth:
                    response.ReqType = CAALHP.Events.Types.RequestTypes.AuthByFinger;
                    if (fsRespons.Result)
                        LoginUser(fsRespons.UserId);
                    break;
                case RequestTypes.Enroll:
                    response.ReqType = CAALHP.Events.Types.RequestTypes.Enroll;
                    break;
                case RequestTypes.CheckforScanner:
                    response.ReqType = CAALHP.Events.Types.RequestTypes.CheckforScanner;
                    break;
                case RequestTypes.DeleteUser:
                    throw new NotImplementedException();
                    break;
            }
            
            CreateAuthenticationResponsEvent(response);
        }

        private void HandleEvent(FacialAuthRequestEvent req)
        {
            var request = new IdentificationRequestEvent
            {
                CallerName = GetName(),
                CallerProcessId = _processId
            };

            var serializedResponse = EventHelper.CreateEvent(SerializationType.Json, request, "FI.Events");
            _host.Host.ReportEvent(serializedResponse);
        }

        private void HandleEvent(IdentificationResponsEvent res)
        {
            var userName = res.UserId;
            
            LoginUser(userName);
        }
        
        private void HandleEvent(IdentificationErrorResponsEvent res)
        {
            var response = new FacialErrorEvent()
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                Message = res.Error
            };

            var serializedResponse = EventHelper.CreateEvent(SerializationType.Json, response);
            _host.Host.ReportEvent(serializedResponse);
        }

        private void HandleEvent(IdentificationFailedResponsEvent res)
        {

            var response = new FacialAuthFailedResponsEvent()
            {
                CallerName = GetName(),
                CallerProcessId = _processId
            };

            var serializedResponse = EventHelper.CreateEvent(SerializationType.Json, response);
            _host.Host.ReportEvent(serializedResponse);

        }

        private void HandleEvent(IsUserLoggedInRequestEvent res)
        {
            var response = new IsUserLoggedInResponsEvent()
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                User = _user,
                PhotoRoot = _userManager.ImgDirectoryPath
            };

            var serializedResponse = EventHelper.CreateEvent(SerializationType.Json, response);
            _host.Host.ReportEvent(serializedResponse);
        }

        public void HandleEvent(UserLoggedOutEvent res)
        {
            LogoutUser();
        }

        //notify events
        public void Notify(KeyValuePair<string, string> notification)
        {
            //var serializer = EventHelper.GetSerializationTypeFromFullyQualifiedNameSpace(notification.Key);
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }


        #region CAALHP Conctact

        public string GetName()
        {
            return "UserService";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IServiceHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            _host.Host.SubscribeToEvents(
                EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof (AuthenticationRequestEvent)),
                _processId);
            _host.Host.SubscribeToEvents(
                EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof (UserListRequestEvent)),
                _processId);
            _host.Host.SubscribeToEvents(
                EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof (IsUserLoggedInRequestEvent)),
                _processId);
            _host.Host.SubscribeToEvents(
                EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof (FSResponsEvent), "FS.Events"),
                _processId);
            _host.Host.SubscribeToEvents(
                EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof (UserLoggedOutEvent)), _processId);
            _host.Host.SubscribeToEvents(
                EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof (FacialAuthRequestEvent)),
                _processId);
            _host.Host.SubscribeToEvents(
                EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof (IdentificationResponsEvent),
                                                       "FI.Events"), _processId);
            _host.Host.SubscribeToEvents(
                EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof (IdentificationFailedResponsEvent),
                                                       "FI.Events"), _processId);
            _host.Host.SubscribeToEvents(
                EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof (IdentificationErrorResponsEvent),
                                                       "FI.Events"), _processId);
            //_userManager = new UserManager(_host);
        }


        //copy files from user service location to output folder



        public void Start()
        {

        }

        public void Stop()
        {

        }

        #endregion

        private void LogoutUser()
        {
            User = null;
        }

        private void LoginUser(string userName)
        {
            
            try
            {
                User = _userManager.getUserList().Single(x => x.UserName == userName);
            }
            catch (Exception)
            {
                //todo lookinto raising propper event here.
                User = null;
                throw;
            }
            
            var response = new UserLoggedInEvent()
                {
                    CallerName = GetName(),
                    CallerProcessId = _processId,
                    User = User,
                    PhotoRoot = _userManager.ImgDirectoryPath
                };

            var serializedResponse = EventHelper.CreateEvent(SerializationType.Json, response);
            _host.Host.ReportEvent(serializedResponse);
        }

        private void RequestEnroll(string userId)
        {
            var request = new FSRequestEvent
            {
                RequestType = RequestTypes.Enroll,
                UserId = userId,
                CallerName = GetName(),
                CallerProcessId = _processId
            };

            CreateFingerScannerEvent(request);
        }

        private void CheckForScannerRequest()
        {
            var request = new FSRequestEvent
            {
                RequestType = RequestTypes.CheckforScanner,
                CallerName = GetName(),
                CallerProcessId = _processId
            };

            CreateFingerScannerEvent(request);
        }
        
        private void AuthByFingerPrint(string userId)
        {
            var request = new FSRequestEvent
            {
                RequestType = RequestTypes.Auth,
                UserId = userId,
                CallerName = GetName(),
                CallerProcessId = _processId
            };

            CreateFingerScannerEvent(request);
        }

        private void CreateFingerScannerEvent(FSRequestEvent request)
        {
            var serializedList = EventHelper.CreateEvent(SerializationType.Json, request, "FS.Events");
            _host.Host.ReportEvent(serializedList);
        }

        private void AuthByPin(AuthenticationRequestEvent userCredentials)
        {
            var validationResult = _authManager.authenticateUser(userCredentials.Username, userCredentials.Pincode);

            var response = new AuthenticationResponseEvent
            {
                Username = userCredentials.Username,
                Response = validationResult,
                ReqType = CAALHP.Events.Types.RequestTypes.AuthByPincode


            };

            if(validationResult)
                LoginUser(userCredentials.Username);

            CreateAuthenticationResponsEvent(response);
        }

        private void CreateAuthenticationResponsEvent(AuthenticationResponseEvent response)
        {
            var serializedResponse = EventHelper.CreateEvent(SerializationType.Json, response);
            _host.Host.ReportEvent(serializedResponse);
        }

    }
}