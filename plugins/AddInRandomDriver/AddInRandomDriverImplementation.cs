﻿using System.Collections.Generic;
using CAALHP.Contracts;

namespace Plugins.AddInRandomDriver
{
    public class AddInRandomDeviceDriverImplementation : IDeviceDriverCAALHPContract
    {
        private IDeviceDriverHostCAALHPContract _host;
        private int _processId;
        public double GetMeasurement()
        {
            var rnd = new System.Random();
            return rnd.NextDouble();
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            throw new System.NotImplementedException();
        }

        public string GetName()
        {
            return "AddInRandomDriver";
        }

        public void Initialize(IDeviceDriverHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
        }
    }
}