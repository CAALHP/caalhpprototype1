﻿using System.AddIn;
using Plugins.DriverPluginAdapters;

namespace Plugins.AddInRandomDriver
{
    // The AddInAttribute identifies this pipeline segment as an add-in.
    [AddIn("Random Driver Plugin", Version = "1.0.0.0")]
    public class AddInRandomDeviceDriver : DeviceDriverViewPluginAdapter
    {
        public AddInRandomDeviceDriver()
        {
            DeviceDriver = new AddInRandomDeviceDriverImplementation();
            
        }
    }
}