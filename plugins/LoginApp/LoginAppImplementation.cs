﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;
using CAALHP.Contracts;
using System.Threading;
using System.Windows.Threading;
using CAALHPUser;
using GalaSoft.MvvmLight.Messaging;

namespace Plugins.LoginApp
{
    public class LoginAppImplementation : IAppCAALHPContract
    {
        //caalhp classes
        private IAppHostCAALHPContract _host;
        private int _processId;
        
        public LoginAppImplementation()
        {
            //LoadLoginApp();
        }



        //Load login app notification event 

        private void HandleLoginAppLoadEvent(LoginAppLoadEvent appLoad)
        {

            Messenger.Default.Send(new GenericMessage<LoginAppLoadEvent>(this, typeof(LoginUI), appLoad));

        }


        //get authentication response from authenticationResponsEvent

        private void HandleEvent(AuthenticationResponseEvent response) 
        {
            Messenger.Default.Send(new GenericMessage<AuthenticationResponseEvent>(this, typeof(LoginUI), response));       
     
            
        }

        //handle uselist event 
        private void HandleEvent(UserListResponseEvent response)
        {
            Messenger.Default.Send(new GenericMessage<UserListResponseEvent>(this, typeof(LoginUI), response));       

        }

        private void HandleEvent(UserLoggedOutEvent obj)
        {
            Messenger.Default.Send(new GenericMessage<UserLoggedOutEvent>(this, typeof(LoginUI), obj));
        }

        private void HandleEvent(UserLoggedInEvent obj)
        {
            Messenger.Default.Send(new GenericMessage<UserLoggedInEvent>(this, typeof(LoginUI), obj));
        }
        

        //notify events
        public void Notify(KeyValuePair<string, string> notification)
        {
 
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }


        public string GetName()
        {
            return "LoginApp";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            //Environment.Exit(0);
            Application.Exit();
            //    Dispatcher.BeginInvokeShutdown(DispatcherPriority.Normal);
        }

        public void Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            //subscribtion to CAALHP events.
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(AuthenticationResponseEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserListResponseEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserLoggedOutEvent)), _processId);
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(UserLoggedInEvent)), _processId);

            //Subscribtion to mvvmlight events from login app for forwarding as caalhpevents.
            Messenger.Default.Register<GenericMessage<AuthenticationRequestEvent>>(this, ForwardAuthencationRequest);
            Messenger.Default.Register<GenericMessage<UserListRequestEvent>>(this, ForwardUserListRequest);
           // Messenger.Default.Register<GenericMessage<UserLoggedInEvent>>(this, ForwardUserLoggedInEvent);
            //Messenger.Default.Register<GenericMessage<UserLoggedInEvent>>(this, ForwardShowHomeApp);
            //_userManager = new UserManager(_host);
            LoadLoginApp();
        }

        private void ForwardUserListRequest(GenericMessage<UserListRequestEvent> obj)
        {
            var completeObj = obj.Content;
            completeObj.CallerName = GetName();
            completeObj.CallerProcessId = _processId;
            var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, completeObj);
            EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json,
                typeof (UserListRequestEvent));
            _host.Host.ReportEvent(serializedEvent);
        }

        private void ForwardAuthencationRequest(GenericMessage<AuthenticationRequestEvent> obj)
        {
            var completeObj = obj.Content;
            completeObj.CallerName = GetName();
            completeObj.CallerProcessId = _processId;
            var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, completeObj);
            EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json,
                typeof(AuthenticationRequestEvent));
            _host.Host.ReportEvent(serializedEvent);
        }

        //public void ForwardUserLoggedInEvent(GenericMessage<UserLoggedInEvent> obj)
        //{
        //    var completeObj = obj.Content;
        //    completeObj.CallerName = GetName();
        //    completeObj.CallerProcessId = _processId;
        //    var serializedEvent = EventHelper.CreateEvent(SerializationType.Json, completeObj);
        //    EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json,
        //        typeof(UserLoggedInEvent));
        //    _host.Host.ReportEvent(serializedEvent);

        //   // ShowHomeApp();
        //}

        private void ShowHomeApp()
        {
            var command = new ShowAppEvent
            {
                CallerName = "LoginApp",
                CallerProcessId = _processId,
                AppName = "HomeScreen",
            };

            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);

            _host.Host.ReportEvent(serializedCommand);

        }

        public void Start()
        {
            
        }

        public void Stop()
        {
            
        }

        //load LoginApp function
        public void LoadLoginApp() {

            //ref: http://reedcopsey.com/2011/11/28/launching-a-wpf-window-in-a-separate-thread-part-1/
            //Threading is used so we do not block the main thread.
            var thread = new Thread(() =>
            {
                // Create our context, and install it:
                SynchronizationContext.SetSynchronizationContext(
                    new DispatcherSynchronizationContext(
                        Dispatcher.CurrentDispatcher));

                var window = new LoginUI();
                window.Text = "LoginUI App";
                // When the window closes, shut down the dispatcher
                window.Closed += (s, e) =>
                   Dispatcher.CurrentDispatcher.BeginInvokeShutdown(DispatcherPriority.Background);
                window.Show();

                Dispatcher.Run();
            });

            //This is needed for GUI threads
            thread.SetApartmentState(ApartmentState.STA);
            // Make the thread a background thread
            thread.IsBackground = true;
            //Start
            thread.Start();

        }

        public void Show()
        {
            Application.DoEvents(); //DoEvents allows the message pump to exectue - thus allowing the application to show the form
            Messenger.Default.Send(new NotificationMessage(this, typeof(LoginUI), "Show"));
        }
    }
}