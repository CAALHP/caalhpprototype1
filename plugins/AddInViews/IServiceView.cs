﻿using System.AddIn.Pipeline;

namespace Plugins.AddInViews
{
    // The AddInBaseAttribute identifes this interface as the basis for 
    // the add-in view pipeline segment.
    [AddInBase()]
    public interface IServiceView : IPluginBaseView
    {
        void Initialize(IServiceHostView host, int processId);
        void Start();
        void Stop();
    }
}
