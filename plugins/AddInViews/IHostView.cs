﻿using System.AddIn.Pipeline;
using System.Collections.Generic;

namespace Plugins.AddInViews
{
    [AddInBase()]
    public interface IHostView
    {
        void ReportEvent(KeyValuePair<string, string> value);
        void SubscribeToEvents(string fullyQualifiedNameSpace, int processId); 
        void UnSubscribeToEvents(string fullyQualifiedNameSpace, int processId);
    }
}