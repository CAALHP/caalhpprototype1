﻿using System.AddIn.Pipeline;
using System.Collections.Generic;
using CAALHP.Contracts;

namespace Plugins.AddInViews
{
    [AddInBase()]
    public interface IAppHostView
    {
        IHostView Host { get; set; }
        void ShowApp(string appName);
        void CloseApp(string appName);
        IList<IPluginInfo> GetListOfInstalledApps();
    }
}