﻿using System.AddIn.Pipeline;
using System.Collections.Generic;

namespace Plugins.AddInViews
{
    [AddInBase()]
    public interface IPluginBaseView
    {
        string GetName();
        void Notify(KeyValuePair<string, string> notification);
        bool IsAlive();
        void ShutDown();
    }
}