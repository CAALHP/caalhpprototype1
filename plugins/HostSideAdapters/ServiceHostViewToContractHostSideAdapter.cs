﻿using System.AddIn.Pipeline;
using System.Collections.Generic;
using CAALHP.Contracts;
using Plugins.Contracts;
using Plugins.HostViewAdapters;

namespace Plugins.HostSideAdapters
{
    [HostAdapter()]
    public class ServiceHostViewToContractHostSideAdapter : ContractBase, IServiceHostContract
    {
        public IHostContract Host { get; set; }
        private readonly IServiceHostView _view;

        public ServiceHostViewToContractHostSideAdapter(IServiceHostView view)
            //: base(view)
        {
            _view = view;
            Host = new HostViewToContractHostSideAdapter(_view.Host);
        }
        
        public IList<IPluginInfo> GetListOfInstalledApps()
        {
            return _view.GetListOfInstalledApps();
        }

        public IList<IPluginInfo> GetListOfInstalledDeviceDrivers()
        {
            return _view.GetListOfInstalledDeviceDrivers();
        }

        public void CloseApp(string fileName)
        {
            _view.CloseApp(fileName);
        }

        public void ActivateDeviceDrivers()
        {
            _view.ActivateDeviceDrivers();
        }

        public IList<string> GetListOfEventTypes()
        {
            return _view.GetListOfEventTypes();
        }
    }
}