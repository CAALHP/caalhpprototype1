﻿using System.AddIn.Pipeline;
using System.Collections.Generic;
using Plugins.Contracts;
using Plugins.HostViewAdapters;

namespace Plugins.HostSideAdapters
{
    [HostAdapter()]
    public class HostContractToViewHostSideAdapter : IHostBaseView
    {
        //private readonly IHostObjectBaseView _view;
        private readonly IHostContract _contract;
        private ContractHandle _handle;

        public HostContractToViewHostSideAdapter(IHostContract contract)
        {
            _contract = contract;
            _handle = new ContractHandle(contract);
        }

        public void ReportEvent(KeyValuePair<string, string> value)
        {
            _contract.ReportEvent(value);
        }

        public void SubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            _contract.SubscribeToEvents(fullyQualifiedNameSpace, processId);
        }

        public void UnSubscribeToEvents(string fullyQualifiedNameSpace, int processId)
        {
            _contract.UnSubscribeToEvents(fullyQualifiedNameSpace, processId);
        }
    }
}