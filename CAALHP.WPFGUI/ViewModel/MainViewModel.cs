﻿using System.Collections.Generic;
using System.Windows.Input;
using CAALHP.Library.Hosts;
using GalaSoft.MvvmLight;
using CAALHP.WPFGUI.Model;
using GalaSoft.MvvmLight.Command;
using Plugins.Host;
using EventManager = CAALHP.Library.Managers.EventManager;

namespace CAALHP.WPFGUI.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private readonly IDataService _dataService;

        /// <summary>
        /// The <see cref="WelcomeTitle" /> property's name.
        /// </summary>
        public const string WelcomeTitlePropertyName = "WelcomeTitle";

        private string _welcomeTitle = string.Empty;
        private Library.CAALHP _caalhp;
        private AppHost _appHost;

        /// <summary>
        /// Gets the WelcomeTitle property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string WelcomeTitle
        {
            get
            {
                return _welcomeTitle;
            }

            set
            {
                if (_welcomeTitle == value)
                {
                    return;
                }

                _welcomeTitle = value;
                RaisePropertyChanged(WelcomeTitlePropertyName);
            }
        }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(IDataService dataService)
        {
            _dataService = dataService;
            _dataService.GetData(
                (item, error) =>
                {
                    if (error != null)
                    {
                        // Report error here
                        return;
                    }

                    WelcomeTitle = item.Title;
                });

            ShowCommand = new RelayCommand(ShowCommand_Execute);

            var eventManager = new EventManager();
            //start the servicehost first so we have loggingservice ready.
            var serviceHost = new ServiceHost(eventManager);
            var driverHost = new DeviceDriverHost(eventManager);
            _appHost = new AppHost(eventManager);
            
            var hostManager = new HostManager(_appHost, driverHost,
                serviceHost);
            _caalhp = new Library.CAALHP(eventManager, new List<IHostManager>(){hostManager});
            
            var apps = _appHost.GetListOfInstalledApps();
            var drivers = driverHost.GetListOfActiveDevices();
            
            WelcomeTitle = "";
            foreach (var driver in drivers)
            {
                WelcomeTitle += driver + ";";
            }
            
            StartSystemApps();
            //_caalhp.HostManager.AppHost.SwitchToApp(1);
            //caalhp.Run();
        }

        private void StartSystemApps()
        {
            var apps = _appHost.GetListOfInstalledApps();
            for (var i = 0; i < apps.Count; i++)
            {
                if (apps[i].Name.StartsWith("System"))
                {
                    _appHost.StartApp(i);
                }
            }
        }

        #region Command

        public ICommand ShowCommand { get; set; }

        private void ShowCommand_Execute()
        {
            _appHost.SwitchToApp(0);
        }
        #endregion
        ////public override void Cleanup()
        ////{
        ////    // Clean up if needed

        ////    base.Cleanup();
        ////}
    }
}