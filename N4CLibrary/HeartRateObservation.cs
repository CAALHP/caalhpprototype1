﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Net4Care.Observation;

namespace N4CLibrary
{
    [DataContract]
    public class HeartRateObservation : IObservationSpecifics
    {
        [DataMember(Name = "heartrate")]
        public ClinicalQuantity HeartRate { get; set; }

        public HeartRateObservation() { }

        public HeartRateObservation(double heartrate)
        {
            HeartRate = new ClinicalQuantity(heartrate, "1/min", "NPU21692", "Heart Rate");
        }

        public bool Equals(IObservationSpecifics other)
        {
            var compareTo = other as GlucoseObservation;
            if (compareTo == null) return false;
            return HeartRate.Equals(compareTo.Glucose);
        }

        public string ObservationAsHumanReadableText
        {
            get
            {
                return "Measured heart rate: " + HeartRate;
            }
        }

        public IList<ClinicalQuantity> Quantities
        {
            get { return new List<ClinicalQuantity>(new[] { HeartRate }); }
        }

    }
}
