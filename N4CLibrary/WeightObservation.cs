﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Net4Care.Observation;

namespace N4CLibrary
{
    [DataContract]
    public class WeightObservation: IObservationSpecifics
    {
        [DataMember(Name = "weight")]
        public ClinicalQuantity Weight { get; private set; }

        [DataMember(Name = "quality")]
        public ClinicalQuantity Quality { get; private set; }

        /// <summary>
		/// Default constructor for the benefit of the serializer
		/// </summary>
		public WeightObservation() { }

		public WeightObservation(double weightMeasurement, Net4CareContext context)
		{
            Quality = new ClinicalQuantity(0, "", "", "Weight Quality");
			Weight = new ClinicalQuantity(weightMeasurement, "kg", "NPU03804", "Weight", context.asContextCodeList());
		}

        public WeightObservation(double weightMeasurement, double quality, Net4CareContext context)
        {
            Weight = new ClinicalQuantity(weightMeasurement, "kg", "NPU03804", "Weight", context.asContextCodeList());
            Quality = new ClinicalQuantity(quality,"", "", "Weight Quality");
        }

        public string ObservationAsHumanReadableText
        {
            get
            {
                return "Measured weight: " + Weight;
            }
        }

        public IList<ClinicalQuantity> Quantities
        {
            get { return new List<ClinicalQuantity>(new[] { Weight, Quality }); }
        }

        public bool Equals(IObservationSpecifics other)
		{
			var compareTo = other as WeightObservation;
			if (compareTo == null) return false;
			return Weight.Equals(compareTo.Weight) && Quality.Equals(compareTo.Quality);
		}

    }
}
