﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAALHP.Events.UserServiceEvents
{
    public class FacialErrorEvent: Event
    {
        public String Message { get; set; }
    }
}
