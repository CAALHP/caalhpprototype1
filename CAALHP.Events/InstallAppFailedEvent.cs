﻿namespace CAALHP.Events
{
    public class InstallAppFailedEvent : Event
    {
        public string AppFileName { get; set; }
        public string Reason { get; set; }
    }
}