namespace CAALHP.Events
{
    public class NewEventTypeAddedEvent : Event
    {
        public string EventType { get; set; }
    }
}