﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAALHP.Events.Types;

namespace CAALHP.Events
{
    public class UserLoggedInEvent: Event
    {
        public UserProfile User { get; set; }
        public string PhotoRoot { get; set; }
    }
}
