﻿namespace CAALHP.Events
{
    public class ShowAppEvent : Event
    {
        public string AppName { get; set; }
    }
}
