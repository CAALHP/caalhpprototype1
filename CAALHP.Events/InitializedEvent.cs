﻿namespace CAALHP.Events
{
    public class InitializedEvent : Event
    {
        public string Name { get; set; }
    }
}
