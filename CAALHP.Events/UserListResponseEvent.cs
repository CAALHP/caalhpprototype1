﻿using CAALHP.Events.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CAALHP.Events
{
    public class UserListResponseEvent : Event
    {
        public string PhotoRoot { get; set; }
        public List<UserProfile> Users { get; set; }
    }
}
