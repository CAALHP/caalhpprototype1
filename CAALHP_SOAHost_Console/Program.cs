﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using CAALHP.Library;
using CAALHP.Library.Managers;
using CAALHP.Library.Strategies;
using CAALHP.WCFService;
using CareStoreServiceContracts;
using SOA.IceHost;

namespace CAALHP_SOAHost_Console
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Initializing CAALHP");
            var caalhp = new CAALHP.Library.CAALHP();
            caalhp.HostManager = new IceHost();

            var addDeviceManager = new DeviceManager();
            addDeviceManager.InstallStrategy = new InstallDriverPluginStrategy(caalhp.HostManager);

            //binding
            var binding = new BasicHttpBinding();
            //endpoint
            var endpoint = new EndpointAddress("http://carestoremarketplace.cloudapp.net/CareStoreMarketplaceService.svc");
            //channelfactory
            var channelFactory = new ChannelFactory<ICareStoreMarketplaceContract>(binding, endpoint);

            addDeviceManager.Market = channelFactory.CreateChannel();

            caalhp.DeviceManager = addDeviceManager;

            var baseAddress = new Uri("http://localhost:8880/caalhp");
            var wcfservice = new WCFService(baseAddress) { DeviceManager = addDeviceManager };
            
            caalhp.Service = wcfservice;

            Console.WriteLine("CAALHP started");
            caalhp.Run();
        }
    }
}
