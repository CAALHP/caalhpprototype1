﻿using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Web.Http;
using System.Web.Http.Routing;
using System.Web.Http.SelfHost;
using AttributeRouting.Web.Http.SelfHost;
using AttributeRouting.Web.Http.SelfHost.Logging;
using CAALHP.Library.Managers;
using CAALHP.Library.Services;
using System;
using CareStoreServiceContracts;
using Microsoft.Practices.Unity;

namespace CAALHP.RESTService
{
    public class RESTService : ServiceHost, ICAALHPServiceContract, IService
    {
        private readonly Uri _baseAddress;
        private HttpSelfHostServer _server;
        private readonly HttpSelfHostConfiguration _config;
        //private CustomControllerActivator _customControllerActivator;

        private readonly DeviceManager _deviceManager;

        public RESTService(Uri address, DeviceManager deviceManager)
        {
            _baseAddress = address;
            _config = new HttpSelfHostConfiguration(_baseAddress);
            _deviceManager = deviceManager;
            //_config.Routes.MapHttpAttributeRoutes();
        }

        public void Start()
        {
            var unity = new UnityContainer();
            unity.RegisterInstance(_deviceManager);
            unity.RegisterType<DeviceController>(new InjectionConstructor(_deviceManager));
            unity.RegisterType<IDeviceManager, DeviceManager>(new PerResolveLifetimeManager());
            _config.DependencyResolver = new IoCContainer(unity);
            
            _config.Routes.MapHttpRoute(
                name: "Default",
                routeTemplate: "{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            _config.Routes.MapHttpAttributeRoutes(cfg => cfg.AddRoutesFromAssembly(Assembly.GetExecutingAssembly()));
            
            _server = new HttpSelfHostServer(_config);

            _server.OpenAsync().Wait();

            Console.WriteLine("Routes begin:");
            _config.Routes.Cast<HttpRoute>().ToArray().LogTo(Console.Out);
            Console.WriteLine("Routes end:");

            Console.WriteLine("The service is ready at {0}", _baseAddress);
        }
        public void Stop()
        {
            // Close the ServiceHost.
            _server.CloseAsync();
        }

        public void AddDevice(DeviceProfile profile)
        {
            _deviceManager.AddDevice(profile.ModelName);
        }
    }
}
