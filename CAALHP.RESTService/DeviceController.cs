﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AttributeRouting.Web.Http;
using CAALHP.Library.Managers;
using CareStoreServiceContracts;

namespace CAALHP.RESTService
{
    /*public class DeviceProfile
    {
        public Guid Id { get; set; }
        public string ModelName { get; set; }
        public string DeviceType { get; set; }
        public string ManufacturerName { get; set; }
    }*/

    //[DefaultHttpRouteConvention]
    public class DeviceController : ApiController
    {
        private readonly IDeviceManager _deviceManager;

        public DeviceController(IDeviceManager deviceManager)
        {
            _deviceManager = deviceManager;
        }

        [POST("device")]
        public HttpResponseMessage PostDevice(DeviceProfile profile)
        {
            //implementation
            //profile must be sent in x-www-form-urlencoded format.
            //thanks to http://stackoverflow.com/questions/14624306/web-api-parameter-always-null
            Console.WriteLine("Call to DeviceController.Post");
            _deviceManager.AddDevice(profile.ModelName);
            //_deviceManager.AddDevice("PI");
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            return response;
        }

        [GET("device")]
        public string Get()
        {
            Console.WriteLine("Call to DeviceController.Get");
            return "Hello from DeviceController";
        }
    }
}
