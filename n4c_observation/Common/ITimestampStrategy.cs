﻿/** Strategy for getting current time in milliseconds (UNIX format). 
 * Preferably use this instead of using the Java classes directly 
 * as it allows test stubs to be injected. 
 *  
 *  @author Henrik Baerbak Christensen and Michael Christensen, Aarhus University
 *  Henrik wrote the original Java code, Michael ported it to C#
 */

namespace Net4Care.Common
{
    public interface ITimestampStrategy
	{
        long getCurrentTimeInMillis();
    }
}
