﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Net4Care.Observation
{
	/// <summary>
	/// ContextCode is an immutable object that
	/// states one fact about the context of a
	/// single observation; typically whether
	/// an observation was made by a person himself or
	/// by a clinician, or similar.
	/// 
	/// It is heavily inspired by the methodCode attribute
	/// used in PHMR/CDA to define context for an observation.
	/// 
	/// In Net4Care we provide convenience methods
	/// in the Net4CareContext class to shield developers
	/// for some of the HL7 details.
	/// </summary>
    public class ContextCode
	{
		public string Code { get; private set; }
        
        public string DisplayName { get; private set; }
		
        public string CodeSystem { get; private set; }

		public ContextCode() { }

		/// <summary>
		/// Define a context code
		/// </summary>
		/// <param name="code">the code that identifies the context</param>
		/// <param name="codeSystem">the system which provides semantics to the code</param>
		/// <param name="displayName">the human readable name of the code</param>
		public ContextCode(string code, string codeSystem, string displayName)
		{
			Code = code;
			DisplayName = displayName;
			CodeSystem = codeSystem;
		}

		public override string ToString() { return "Contextcode (" + Code + "," + DisplayName + "," + CodeSystem + ")"; }

		public override bool Equals(object obj)
		{
			var compareTo = obj as ContextCode;
			if (compareTo == null) return false;
			return Code == compareTo.Code && DisplayName == compareTo.DisplayName && CodeSystem == compareTo.CodeSystem;
		}

		public override int GetHashCode()
		{
			var hash = 17;
			hash = hash * 23 + Code.GetHashCode();
			hash = hash * 23 + DisplayName.GetHashCode();
			hash = hash * 23 + CodeSystem.GetHashCode();
			return hash;
		}
	}
}
