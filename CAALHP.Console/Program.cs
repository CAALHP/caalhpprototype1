﻿using System;
using System.ServiceModel;
using CAALHP.Library.Managers;
using CAALHP.Library.Strategies;
using Plugins.Host;
using ServiceHost = Plugins.Host.ServiceHost;

namespace CAALHP.Console
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            System.Console.WriteLine("Initializing CAALHP");
            var eventManager = new EventManager();
            var hostManager = new HostManager(new AppHost(eventManager), new DeviceDriverHost(eventManager),
                new ServiceHost(eventManager));
            var caalhp = new CAALHP.Library.CAALHP(hostManager, eventManager);

            /*
            //var addDeviceManager = new DeviceManager();
            //addDeviceManager.InstallStrategy = new InstallDriverPluginStrategy(caalhp.HostManager.DeviceDriverHost);

            //binding
            var binding = new BasicHttpBinding();
            //endpoint
            var endpoint = new EndpointAddress("http://carestoremarketplace.cloudapp.net/CareStoreMarketplaceService.svc");
            //channelfactory
            var channelFactory = new ChannelFactory<ICareStoreMarketplaceContract>(binding, endpoint);

            //addDeviceManager.Market = channelFactory.CreateChannel();
            
            //caalhp.DeviceManager = addDeviceManager;

            //var baseAddress = new Uri("http://localhost:8880/caalhp");
            //var wcfservice = new WCFService.WCFService(baseAddress) {DeviceManager = addDeviceManager};
            //var restservice = new RESTService.RESTService(baseAddress, addDeviceManager);
            //caalhp.Service = wcfservice;
            //caalhp.Service = restservice;
            */

            var binding = new BasicHttpBinding();
            var otherBinding = new BasicHttpsBinding(BasicHttpsSecurityMode.TransportWithMessageCredential);
            System.Console.WriteLine("CAALHP started");
            caalhp.Run();
        }
    }
}
