﻿using System;
using CareStoreSoaIceHost;
using EventManager = CAALHP.Library.Managers.EventManager;
using CareStoreSOAICE.Host;

namespace CareStoreSOAHostConsole
{
    class ConsoleHostApp
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting SOAHost");

            var app = new ConsoleHostApp();    

            Console.WriteLine("Press Enter to end");
            Console.ReadLine();
        }

        public ConsoleHostApp() {

            Console.WriteLine("Starting CareStoreHostNamingServer ...");

            var eventManager = new EventManager();
            //start the servicehost first so we have loggingservice ready.
            var serviceHost = new ServiceHost(eventManager);
            var driverHost = new DeviceDriverHost(eventManager);
            var appHost = new AppHost(eventManager);
            
            var hostManager = new  CAALHP.Library.Hosts.HostManager(appHost, driverHost, serviceHost);

            var caalhp = new CAALHP.Library.CAALHP(hostManager, eventManager);
            
            var apps = appHost.GetListOfInstalledApps();
            var drivers = driverHost.GetListOfActiveDevices();
            
            //WelcomeTitle = "";
            //foreach (var driver in drivers)
            //{
            //    //WelcomeTitle += driver + ";";
            //}
            
            StartSystemApps();
            //_caalhp.HostManager.AppHost.SwitchToApp(1);
            //caalhp.Run();
        }

        private void StartSystemApps()
        {
            //var apps = _appHost.GetListOfInstalledApps();
            //for (var i = 0; i < apps.Count; i++)
            //{
            //    if (apps[i].Name.StartsWith("System"))
            //    {
            //        _appHost.StartApp(i);
            //    }
            //}
        }

      }
}
