﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

using log4net;

using Net4Care.Observation;
using System.Runtime.Serialization.Json;
using System.IO;

/** An implementation of Serializer that uses JSON.NET. 
 *  
 * @author Michael Christensen, Aarhus University
 * 
 */

namespace Net4Care.Serializer.JSON
{
	public class JSONSerializer : ISerializer
	{
		private const string ID_PATIENTCPR = "patientCPR";
		private const string ID_ORGANIZATIONUID = "organizationUID";
		private const string ID_TREATMENTID = "treatmentID";
		private const string ID_TIME = "time";
		private const string ID_CODESYSTEM = "codeSystem";
		private const string ID_DEVICEDESCRIPTION = "deviceDescription";
		private const string ID_OBSERVATIONSPECIFICS = "observationSpecifics";
		private const string ID_COMMENT = "comment";
		private const string ID_DEVICEDESCRIPTIONASHUMANREADABLETEXT = "deviceDescriptionAsHumanReadableText";

		private static readonly ILog logger = LogManager.GetLogger(typeof(JSONSerializer));

        public Type[] KnownTypes { get; set; }

		private JsonSerializerSettings GetJsonSettings()
		{
			//Serialize private members!
			//Notice could instead use explicit [JsonProperty] on members that needs to be
			//serialized/deserialized but this would require n4c_observation to reference
			//Newtonsoft.Json.
            DefaultContractResolver contractResolver = new DefaultContractResolver();
			contractResolver.DefaultMembersSearchFlags |= BindingFlags.NonPublic;
            JsonSerializerSettings settings = new JsonSerializerSettings { ContractResolver = contractResolver };
			//settings.TypeNameHandling = TypeNameHandling.Arrays;
			return settings;
		}

		private string MakeObservationSpecificsHumanReadableText(List<ClinicalQuantity> quantities)
		{
			string result = "";
			StringBuilder builder = new StringBuilder();
			bool first = true;
			foreach (ClinicalQuantity cq in quantities)
			{
				if (first) first = false;
				else builder.Append(" / ");
				builder.Append(cq);
			}
			if (builder.Length > 0)
				result = "Measured: " + builder;
			return result;
		}

        public string Serialize(StandardTeleObservation sto)
        {
            var js = new DataContractJsonSerializer(sto.GetType(), KnownTypes);
            using (var ms = new MemoryStream())
            {
                try
                {
                    js.WriteObject(ms, sto);
                    var json = Encoding.UTF8.GetString(ms.ToArray());
                    return json;
                }
                catch (Exception e)
                {
                    throw;
                }
            }
        }

		public string SerializeList(List<String> stoList)
		{
			return SerializeObject(stoList);
		}

		private string SerializeObject(Object o)
		{
			string result = null;

			try
			{
				result = JsonConvert.SerializeObject(o, GetJsonSettings());
			}
			catch (JsonException e)
			{
				// Rethrow
				throw new Exception("Exception while serializing object: " + o.ToString(), e); ;
			}

			return result;
		}

        public StandardTeleObservation Deserialize(String json)
        {
            var js = new DataContractJsonSerializer(typeof(StandardTeleObservation), KnownTypes);
            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(json)))
            {
                try
                {
                    var sto = js.ReadObject(ms) as StandardTeleObservation;
                    return sto;
                }
                catch (Exception e)
                {
                    // throw;
                    return null;
                }
            }
        }

		Type GetTypeByName(string typeName)
		{
			foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
				foreach (Type type in assembly.GetTypes())
					if (type.FullName == typeName)
						return type;
			return null;
		}

		public List<StandardTeleObservation> DeserializeList(String listOfSTOasString)
		{
			List<StandardTeleObservation> result = new List<StandardTeleObservation>();

			try
			{
				List<String> stringList = DeserializeStringList(listOfSTOasString);
				foreach (String str in stringList)
					result.Add(Deserialize(str));
			}
			catch (JsonException e)
			{
				// Rethrow
				throw new Exception("Exception while deserializing list of StandardTeleObservation from: " + listOfSTOasString, e);
			}
			return result;
		}

		public List<String> DeserializeStringList(String listOfString)
		{
			List<String> stringList = null;

			try
			{
				stringList = JsonConvert.DeserializeObject<List<String>>(listOfString, GetJsonSettings());
			}
			catch (JsonException e)
			{
				// Rethrow
				throw new Exception("Exception while deserializing list of strings from: " + listOfString, e);
			}

			return stringList;
		}
	}
}
