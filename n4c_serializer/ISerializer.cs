﻿using System;
using System.Collections.Generic;
using Net4Care.Observation;

/** The serializer is responsible for marshalling and unmarshalling
 * StandardTeleObservations (and lists of them) 
 * to and from the on-the-wire format that
 * is used between a home device and the N4C server side.
 * 
 * You should use the implementation(s) in subpackage delegate.
 * 
 * The format is required to be a lightweight, compact format,
 * see design decision XXX in Net4Care technical report 3.
 * 
 *  @author Henrik Baerbak Christensen and Michael Christensen, Aarhus University
 *  Henrik wrote the original Java code, Michael ported it to C#
 *
 */

namespace Net4Care.Serializer
{
    public interface ISerializer
    {
        string Serialize(StandardTeleObservation sto);
        string SerializeList(List<String> stoList);

        StandardTeleObservation Deserialize(String messagePayload);
        List<StandardTeleObservation> DeserializeList(String listOfSTOasString);
        /** Deserialize a JSON message that just contains a list of strings.
         * Used for PHMR documents.
         * @param listOfStringsAsSingleString
         * @return
         */
        List<String> DeserializeStringList(String listOfStringsAsSingleString);
    }
}
